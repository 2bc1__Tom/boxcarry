﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using Newtonsoft.Json;
using Sing;

namespace UIFramework
{
	using DataTable = Dictionary<string, object>;

	public class WWWHelper : Singleton<WWWHelper>
	{
		public delegate IEnumerator PacketCallBack(UnityWebRequest www, FunctionDelegate funcCallback, object param);
		public delegate void FunctionDelegate(object param); // UI 처리

		//	이전 통신 요청 해시로 저장
		private int mLastUrlHash = int.MinValue, mLastParamHash = int.MinValue;
		private DateTime mLastPostTimestamp = DateTime.MinValue;
		private const float kDuplicatedRequestBlockTimer = 0.8f;

		//public UISlider mProgressBar;
		//public UILabel mLabel_persent;
		//public GameObject mIndicator;

		const int mTimeOut = 5; // 타임아웃 5초
	

		public string EncodeJsonData(string jsonString)
		{
			return jsonString;
			//return AES256Cipher.AES_encrypt(jsonString);
		}

		public string DecodeJsonData(string jsonString)
		{
			return jsonString;
			//return AES256Cipher.AES_decrypt(jsonString);
		}
	
		public static T JsonDeserializeObject<T>(string jsonStr)
		{
			return JsonConvert.DeserializeObject<T>(jsonStr, Instance.GetDateTimeSerializerSettings());
		}

		public JsonSerializerSettings GetDateTimeSerializerSettings()
		{
			var settings = new JsonSerializerSettings
			{
				NullValueHandling = NullValueHandling.Ignore,
				DateParseHandling = DateParseHandling.None,
				Converters = new List<JsonConverter> { new DateTimeFixingConverter() },
			};
			return settings;
		}

		// Json 데이터 값중 DateTime 값의 컨버팅을 위해 사용
		public class DateTimeFixingConverter : JsonConverter
		{

			public override bool CanWrite
			{
				get { return false; }
			}

			public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer)
			{
				throw new NotImplementedException();
			}

			public override object ReadJson(JsonReader reader, Type objectType, object existingValue, JsonSerializer serializer)
			{
				if (objectType == typeof(DateTime?))
					return null;

				//double longDate = Convert.ToDouble(reader.Value);
				//DateTime date = UGUIUtil.ConvertFromUnixTimestamp(longDate);

				return null;
			}

			public override bool CanConvert(Type objectType)
			{
				return (objectType == typeof(DateTime) || objectType == typeof(DateTime?));
			}
		}
	}
}
