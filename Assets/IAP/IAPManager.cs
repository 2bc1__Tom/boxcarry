﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Purchasing;
using UnityEngine;
using System;
using Newtonsoft.Json;

public class IAPManager : MonoBehaviour , IStoreListener
{
    public static IAPManager m_instance;
    public static IAPManager Instance
    {
        get
        {
            if(m_instance) return m_instance;

            m_instance = FindObjectOfType<IAPManager>();
            
            if(m_instance == null) m_instance = new GameObject("IAP Manager").AddComponent<IAPManager>();
            return m_instance;
        }
    }

    private IStoreController storeController; // 구매 과정을 제어하는 함수를 제공
    private IExtensionProvider storeExtensionProvider; //여러 플랫폼을 위한 확정 처리를 제공
    public static event Action<ProductInfo> PurchaseSucceedEvent;
    const string BillingServerURL = "http://billing.becomead.com/"; // 상용
    public List<ProductInfo> ProductList = new List<ProductInfo>();
    private List<string> mRestoreID = new List<string>();
    private bool mRestore = false;
    private string PackageName;

    [SerializeField] List<string> IAP_KEY_AOS = new List<string>();
    [SerializeField] List<string> IAP_KEY_IOS = new List<string>();

    void Awake()
    {
        PackageName = Application.identifier;
        StartCoroutine(InitUnityIAP());
    }

    IEnumerator InitUnityIAP()
    {
        ///step1. 자체 서버로부터 인앱정보를 얻어온다. 
        yield return StartCoroutine (InitProductList());

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());

#if UNITY_ANDROID
        for(int i = 0; i < IAP_KEY_AOS.Count; i++)
        {
            builder.AddProduct(IAP_KEY_AOS[i] , ProductType.Consumable);
        }
#else
        for(int i = 0; i < IAP_KEY_IOS.Count; i++)
        {
            builder.AddProduct(IAP_KEY_IOS[i] , ProductType.Consumable);
        }
#endif
    
        UnityPurchasing.Initialize(this, builder);
    }

    public void OnInitialized(IStoreController contoller , IExtensionProvider extensions)
    {
        Debug.Log("인앱 IAP 초기화 성공");
        storeController = contoller;
        storeExtensionProvider = extensions;
    }

    public void OnInitializeFailed(InitializationFailureReason errer)
    {
        Debug.Log("인앱 IAP 초기화 실패 : " + errer);
    }

    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        Debug.Log("인앱 구매 성공 : " + args.purchasedProduct.definition.id);
        Debug.Log("인앱 영수증 : " + args.purchasedProduct.receipt);
     
        if(mRestore) //리스토어 일 시 상품 목록을 담는다.
        {
            if(!mRestoreID.Contains(args.purchasedProduct.definition.id))
            {
                mRestoreID.Add(args.purchasedProduct.definition.id);
            }
        }

        #if UNITY_EDITOR
        Debug.Log("인앱 EDITOR");

        string productId = args.purchasedProduct.definition.id;
        PurchaseSucceed(productId);
        return PurchaseProcessingResult.Complete;

        #elif UNITY_ANDROID

        Debug.Log("인앱 ANDROID");
        string json = GetInAppDataAOS(args.purchasedProduct.receipt);
        StartCoroutine(PurchaseVerification(json , null , args.purchasedProduct.definition.id , args.purchasedProduct));
        return PurchaseProcessingResult.Pending;

        #else

        Debug.Log("인앱 IOS");
        InAppData inAppData = GetInAppDataIOS(args.purchasedProduct.receipt);
        string receipt = inAppData.Payload;
        string transcationId = inAppData.TransactionID;

        StartCoroutine(PurchaseVerification(receipt , transcationId , args.purchasedProduct.definition.id , args.purchasedProduct));
        return PurchaseProcessingResult.Pending;

        #endif

    }

    public void OnPurchaseFailed(Product prouct , PurchaseFailureReason reason)
    {
        Debug.Log("인앱 구매 실패 : " + prouct.definition.id + " / " + reason);
    }

    public void Purchase(string productId)
    {  
        Debug.Log("구매 아이디 : " + productId);

        if(storeController == null) return;

        var product = storeController.products.WithID(productId);

        if(productId != null && product.availableToPurchase)
        {
            Debug.Log("구매 시도 : " + product.definition.id);
            storeController.InitiatePurchase(product);

        }else
        {
            Debug.Log("구매 시도 불가");
        }
    }

    public void RestorePurchase()
    {
        if(storeExtensionProvider == null) return;

        #if UNITY_IOS

        mRestore = true;
        mRestoreID.Clear();

        var appleExt = storeExtensionProvider.GetExtension<IAppleExtensions>();

        appleExt.RestoreTransactions(callback:Result=>{

            Debug.Log("리스토어 결과 : " + Result);

            if(Result)
            {
                for(int i = 0; i < mRestoreID.Count; i++)
                {
                    if(!Prefs.GetBool(mRestoreID[i] , false))
                    {
                        PurchaseSucceed(mRestoreID[i]);
                    }
                }
            }

            mRestore = false;
        });

        #endif
    }

    ///결제정보를 서버를 통해 검증 한후 보상한다.
	IEnumerator PurchaseVerification(string receipt, string transcationId , string productId, UnityEngine.Purchasing.Product mProduct) {

		int retryCount = 3;
		
		while (retryCount > 0) {

			WWWForm form = new WWWForm();

            #if UNITY_ANDROID
			form.AddField("json", receipt);
			form.AddField("code", "KOR");
			form.AddField("user_id", ""); // user_id 가 없는 게임의 경우 "" 를 넣으면 과금로그에는 ADID 값으로 들어간다.
			form.AddField("udid", "NULL_ADID_ANDROID_USER"); // ADID 값
			form.AddField("serverType", "0"); // 게임서버 존재 유무 (보상 지급을 클라이언트로 받을지 게임서버로 보낼지 결정) > 1 : 게임서버 사용, 0 : 게임서버 사용안함
			form.AddField("os", "1"); // OS 구분값 > 1 : 안드로이드 , 2 : 아이폰
			#elif UNITY_IPHONE
			form.AddField("receipt", receipt);
			form.AddField("productId", productId);
			form.AddField("transactionId", transcationId);
			form.AddField("code", "KOR");
			form.AddField("bid", Application.identifier);
			form.AddField("user_id", "");
			form.AddField("udid", "NULL_ADID_IOS_USER");
			form.AddField("serverType", "0"); // 게임서버 존재 유무 (보상 지급을 클라이언트로 받을지 게임서버로 보낼지 결정) > 1 : 게임서버 사용, 0 : 게임서버 사용안함
			form.AddField("os", "2");
			#endif
	
			WWW www = new WWW(BillingServerURL + "v3/PurchaseVerification", form);
			yield return www;
			
			if (www.error != null) {

				Debug.Log("인앱 Error : " + www.error);
				retryCount--;

				yield return new WaitForSeconds(1.0f);
				continue;
			}
			
			string packet = System.Text.RegularExpressions.Regex.Unescape(www.text);

			JSONObject json = new JSONObject(packet);

			if (json == null) {
				Debug.LogError("Failed Result:" + packet);
				retryCount--;

				yield return new WaitForSeconds(1.0f);
				continue;
			}

			int msgCode = int.Parse(GetJsonField(json, "msgcode"));

            if (msgCode == (int)MSGCODE.successed) {

                PurchaseSucceed(productId , mProduct); ///보상을 지급한다.
                break;

            }else
            {
                Debug.LogError ("인앱 실패 코드 : " + msgCode);

                if(msgCode == (int)MSGCODE.FAIL_BE_ORDER_ID) //중복 지급일 때
				{
                    if(PlayerPrefs.GetString(productId , "").Length == 0)
                    {
                        PurchaseSucceed(productId , mProduct);
                    }

					yield break;
				}

                break;
            }
		}
	}

    public void PurchaseSucceed(string productId , UnityEngine.Purchasing.Product mProduct = null)
    {
        //if(PurchaseSucceedEvent != null)
        //{
            ///보상을 지급한다.
            foreach (ProductInfo list in ProductList) {
				if (list.product_id.Equals(productId)) {
                    
					int giveCash = 0;

					if (list.product_bonus > 0)
					{
						giveCash = list.product_give_cash + list.product_bonus;
					}
					else
					{
						giveCash = list.product_give_cash;
					}

					Debug.Log("결제 성공 : " + giveCash);
					Prefs.setPrefsJewel(giveCash);

					if (Game_script.Instance != null) Game_script.Instance.Gold_text();

                    //PurchaseSucceedEvent(list);
                    PlayerPrefs.SetString(productId , productId); //구매 여부 저장
				}
			//}

            ///보상지급 후 컨슘을 날린다.
            if(mProduct != null)
                storeController.ConfirmPendingPurchase(mProduct);
        }
    }

    ///Json 파싱
	string GetJsonField(JSONObject json, string field) {
		string result = string.Empty;
		
		if ((json == null) || (json["Item"] == null)) {
			Debug.LogError("Faild Format Json:" + field);
			return string.Empty;
		}
		
		JSONObject root = json["Item"][0];
		if (root != null) {
			JSONObject node = root.GetField(field);
			if (node != null) {
				result = node.str;
			}
		}
		
		if (string.IsNullOrEmpty(result) == true) {
			result = "0";
		}
		
		return result;
	}

    ///자체 서버로부터 인앱정보를 얻어온다. 
	IEnumerator InitProductList() {
		int retryCount = 3;
		while (retryCount > 0) {
			yield return new WaitForSeconds (1.0f);

			WWWForm form = new WWWForm ();
			form.AddField ("packageName", PackageName);
			#if UNITY_IPHONE
			form.AddField("os", "2");
			#endif
			WWW www = new WWW (BillingServerURL + "ProductList", form);
			yield return www;
			
			if (www.error != null) {
				Debug.Log ("www connection error : " + www.error);

				retryCount--;
				continue;
			}
			
			string packet = System.Text.RegularExpressions.Regex.Unescape (www.text);
			JSONObject json = new JSONObject (packet);
			if (json == null) {
				Debug.LogError ("Failed Result:" + packet);

				retryCount--;
				continue;
			}
			
			JSONObject jsonFirst = json ["Item"] [0];
			int msgCode = int.Parse (GetJsonField (json, "msgcode"));
			
			if (msgCode != (int)MSGCODE.successed) {
				Debug.LogError ("msgCode : " + msgCode);

				retryCount--;
				continue;
			}
			
			JSONObject itemJsonData = jsonFirst ["productlist"];
			
			ProductList.Clear ();
			for (int i = 0; i < itemJsonData.Count; i++) {
				ProductInfo items = new ProductInfo ();
				items.product_idx = (int)itemJsonData [i].GetField ("product_idx").n;
				items.product_id = itemJsonData [i].GetField ("product_id").str;
				items.product_type = (int)itemJsonData [i].GetField ("product_type").n;
				items.product_cost = (int)itemJsonData [i].GetField ("product_cost").n;
				items.product_give_cash = (int)itemJsonData [i].GetField ("product_give_cash").n;
				items.product_bonus = (int)itemJsonData [i].GetField ("product_bonus").n;
				
				ProductList.Add (items);
			}

			break;
		}
	}

    public string GetInAppDataAOS(string data)
    {
        InAppData inappData = UIFramework.WWWHelper.JsonDeserializeObject<InAppData>(data.ToString());
        InAppData.InAppJSon jsonData = UIFramework.WWWHelper.JsonDeserializeObject<InAppData.InAppJSon>(inappData.Payload);
        return jsonData.json;
    }

    public InAppData GetInAppDataIOS(string data)
    {
        InAppData inappData = UIFramework.WWWHelper.JsonDeserializeObject<InAppData>(data.ToString());
        return inappData;
    }

    public class InAppData 
    {
        string mPayload = "";
        string mTransactionID = "";

        [JsonProperty("Payload")]
        public string Payload
        {
            get { return mPayload; }
            set { mPayload = value; }
        }

        [JsonProperty("TransactionID")]
        public string TransactionID
        {
            get { return mTransactionID; }
            set { mTransactionID = value; }
        }

        public class InAppJSon
        {
            string mJson = "";

            [JsonProperty("json")]
            public string json
            {
                get { return mJson; }
                set { mJson = value; }
            }
        }
    }

    /// 서버로 부터 오는 데이터 클레스
	public class ProductInfo {
		private string mProduct_idx;		///인덱스
		private string mProduct_id;			///아이디 SKU
		private string mProduct_type;		///타입
		private string mProduct_cost;		///가격
		private string mProduct_give_cash;	///보상 화폐
		private string mProduct_bonus;		///보상 화폐 보너스
		
		public int product_idx {
			get { return int.Parse(mProduct_idx); }
			set { mProduct_idx = value.ToString(); }
		}
		
		public string product_id {
			get { return mProduct_id; }
			set { mProduct_id = value.ToString(); }
		}

		public int product_type {
			get { return int.Parse(mProduct_type); }
			set { mProduct_type = value.ToString(); }
		}
		
		public int product_cost {
			get { return int.Parse(mProduct_cost); }
			set { mProduct_cost = value.ToString(); }
		}
		
		public int product_give_cash {
			get { return int.Parse(mProduct_give_cash); }
			set { mProduct_give_cash = value.ToString(); }
		}
		
		public int product_bonus {
			get { return int.Parse(mProduct_bonus); }
			set { mProduct_bonus = value.ToString(); }
		}
		
	}

    /// 서버로부터 오는 응답코드
	public enum MSGCODE {
		successed 				= 10000,    // 성공
		exception_api 			= 20000,    // 서버 API 호출실패
		disconnect_db 			= 30001,    // DB서버에 연결안됨
		
		overlap_nickname 		= 10001,
		non_user 				= 10002,
		overlap_udid 			= 10003,
		non_user_no 			= 10004,
		non_rank_plan 			= 10005,
		non_rank_data 			= 10006,
		FAIL_BE_ORDER_CANCEL	= 40007,
		FAIL_BE_ORDER_REFUND	= 40008,
		FAIL_BE_ORDER_ID		= 40019
	}
}
 