﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

namespace TnkAd {
	public enum AdListType {
		ALL = -1,
		PPI = 1,
		CPS = 4
	}

	public enum TemplateStyle {
		DEFAULT = 0,
		BLUE_01 = 1,
		BLUE_02 = 2,
		BLUE_03 = 3,
		BLUE_04 = 4,
		BLUE_05 = 5,
		BLUE_06 = 6,
		BLUE_07 = 7,
		BLUE_08 = 8,
		RED_01 = 9,
		RED_02 = 10,
		RED_03 = 11,
		RED_04 = 12,
		RED_05 = 13,
		RED_06 = 14,
		RED_07 = 15,
		RED_08 = 16,

		TAG_TEXT_SMALL_BLUE_01 = 17,

		BLUE_TAB_01 = 101,
		BLUE_TAB_02 = 102,
		BLUE_TAB_03 = 103,
		BLUE_TAB_04 = 104,
		BLUE_TAB_05 = 105,
		BLUE_TAB_06 = 106,
		BLUE_TAB_07 = 107,
		BLUE_TAB_08 = 108,
		RED_TAB_01 = 109,
		RED_TAB_02 = 110,
		RED_TAB_03 = 111,
		RED_TAB_04 = 112,
		RED_TAB_05 = 113,
		RED_TAB_06 = 114,
		RED_TAB_07 = 115,
		RED_TAB_08 = 116
		
	}

	public class Plugin {
		private static Plugin _instance;
		
		public static Plugin Instance {
			get {
				if(_instance == null) {
					_instance = new Plugin();
				}
				return _instance;
			}
		}

		private AndroidJavaClass pluginClass;
		
		public Plugin() {
			pluginClass = new AndroidJavaClass("com.tnkfactory.ad.unity.TnkUnityPlugin");
		}

		public void initInstance() {
			pluginClass.CallStatic ("initInstance");
		}

		public void initInstance(string appId) {
			pluginClass.CallStatic ("initInstance", appId);
		}

        public void applicationStarted() {
            pluginClass.CallStatic("applicationStarted");
        }

        public void setUserName(string userName) {
            pluginClass.CallStatic("setUserName", userName);
        }

        public void setUserAge(int age) {
            pluginClass.CallStatic("setUserAge", age);
        }

        // 0 for male, 1 for female
        public void setUserGender(int gender) {
            pluginClass.CallStatic("setUserGender", gender);
        }

        public void setCOPPA(bool isCOPPA) {
            pluginClass.CallStatic("setCOPPA", isCOPPA);
        }

        public void enableCurrencyFormat(bool useFormat) {
            pluginClass.CallStatic("enableCurrencyFormat", useFormat);
        }

		public void onBackPressed() {
			pluginClass.CallStatic("onBackPressed");
		}

		public void showAdList() {
			pluginClass.CallStatic("showAdList", null, (int)AdListType.ALL, (int)TemplateStyle.DEFAULT);
		}

		public void showAdList(AdListType type) {
			pluginClass.CallStatic("showAdList", null, (int)type, (int)TemplateStyle.DEFAULT);
		}

		public void showAdList(string title) {
			pluginClass.CallStatic("showAdList", title, (int)AdListType.ALL, (int)TemplateStyle.DEFAULT);
		}

		public void showAdList(string title, AdListType type) {
			pluginClass.CallStatic("showAdList", title, (int)type, (int)TemplateStyle.DEFAULT);
		}

		public void showAdList(string title, TemplateStyle style) {
			pluginClass.CallStatic("showAdList", title, (int)AdListType.ALL, (int)style);
		}

		public void showAdList(string title, AdListType type, TemplateStyle style) {
			pluginClass.CallStatic("showAdList", title, (int)type, (int)style);
		}

		public void showAdListTab()
		{
			pluginClass.CallStatic("showAdListTab", null, (int)TemplateStyle.BLUE_TAB_01);
		}

		public void showAdListTab(string title)
		{
			pluginClass.CallStatic("showAdListTab", title, (int)TemplateStyle.BLUE_TAB_01);
		}

		public void showAdListTab(TemplateStyle style)
		{
			pluginClass.CallStatic("showAdListTab", null, (int)style);
		}

		public void showAdListTab(string title, TemplateStyle style)
		{
			pluginClass.CallStatic("showAdListTab", title, (int)style);
		}

		public void popupAdList() {
			pluginClass.CallStatic("popupAdList", null, (int)AdListType.ALL, null, (int)TemplateStyle.DEFAULT);
		}

		public void popupAdList(AdListType type) {
			pluginClass.CallStatic("popupAdList", null, (int)type, null, (int)TemplateStyle.DEFAULT);
		}

		public void popupAdList(string title) {
			pluginClass.CallStatic("popupAdList", title, (int)AdListType.ALL, null, (int)TemplateStyle.DEFAULT);
		}

		public void popupAdList(string title, AdListType type) {
			pluginClass.CallStatic("popupAdList", title, (int)type, null, (int)TemplateStyle.DEFAULT);
		}

		public void popupAdList(string title, string handlerName) {
			pluginClass.CallStatic("popupAdList", title, (int)AdListType.ALL, handlerName, (int)TemplateStyle.DEFAULT);
		}

		public void popupAdList(string title, AdListType type, string handlerName) {
			pluginClass.CallStatic("popupAdList", title, (int)type, handlerName, (int)TemplateStyle.DEFAULT);
		}

		public void popupAdList(string title, AdListType type, TemplateStyle style)
		{
			pluginClass.CallStatic("popupAdList", title, (int)type, null, (int)style);
		}

		public void popupAdList(string title, AdListType type, string handlerName, TemplateStyle style) {
			pluginClass.CallStatic("popupAdList", title, (int)type, handlerName, (int)style);
		}

        public void prepareInterstitialAdForPPI() {
            pluginClass.CallStatic("prepareInterstitialAdForPPI");
        }

        public void showInterstitialAdForPPI() {
			pluginClass.CallStatic("showInterstitialAdForPPI");
        }

		public void prepareInterstitialAdForPPI(string handlerName) {
            pluginClass.CallStatic("prepareInterstitialAdForPPI", handlerName);
        }

        public void showInterstitialAdForPPI(string handlerName) {
            pluginClass.CallStatic("showInterstitialAdForPPI", handlerName);
        }

		public void closeButtonAlignRightForInterstital(bool alignRight){
			if (alignRight)
			{
				pluginClass.CallStatic("closeButtonAlignRight");
			}
			else
			{
				pluginClass.CallStatic("closeButtonAlignLeft");
			}
		}

		public bool isAdViewVisible() {
			return pluginClass.CallStatic<bool>("isAdViewVisible");
		}

        public void actionCompleted() {
			pluginClass.CallStatic("actionCompleted");
		}

		public void actionCompleted(string actionName) {
			pluginClass.CallStatic("actionCompleted", actionName);
		}

		public void buyCompleted(string itemName) {
			pluginClass.CallStatic("buyCompleted", itemName);
		}

		public void queryPoint(string handlerName) {
			pluginClass.CallStatic("queryPoint", handlerName);
		}

		public void withdrawPoints(string desc, string handlerName) {
            pluginClass.CallStatic("withdrawPoints", desc, handlerName);
		}
		                                            
		public void purchaseItem(int cost, string itemName, string handlerName) {
			pluginClass.CallStatic("purchaseItem", cost, itemName, handlerName);
		}

		public void queryPublishState(string handlerName) {
			pluginClass.CallStatic("queryPublishState", handlerName);
		}
	}
}
