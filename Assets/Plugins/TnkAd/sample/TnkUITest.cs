﻿using UnityEngine;
using System.Collections;

public class TnkUITest : MonoBehaviour {

	void Start ()
	{
        TnkAd.Plugin.Instance.initInstance();
        TnkAd.Plugin.Instance.applicationStarted();
        TnkAd.Plugin.Instance.setUserName("test_name"); // unique id for a user in your game like login-id
	}
	
	void Update ()
	{
		if (Application.platform == RuntimePlatform.Android) {
			if (Input.GetKeyUp(KeyCode.Escape)) {
				Application.Quit();

//				if (TnkAd.Plugin.Instance.isAdViewVisible()) {
//					TnkAd.Plugin.Instance.onBackPressed();
//				}
//				else {
//					// put your logic for back-key pressed
//					Application.Quit();
//				}
			}
		}
	}
	
	void OnGUI ()
	{
		// be sure that put handler object named 'testhandler' in your scene. (It should be named in Unity Inspector)

		string title = "Test Title";

		if (GUI.Button(new Rect(100, 100, 200, 80), "Show Offerwall (Activity)"))
		{
			Debug.Log("Offerwall Ad - Android");

			//string title = "Test Title";

			TnkAd.Plugin.Instance.showAdList();
			//TnkAd.Plugin.Instance.showAdList(TnkAd.AdListType.PPI);
			//TnkAd.Plugin.Instance.showAdList(title);
			//TnkAd.Plugin.Instance.showAdList(title, TnkAd.AdListType.ALL);
			//TnkAd.Plugin.Instance.showAdList(title, TnkAd.TemplateStyle.BLUE_01);
			//TnkAd.Plugin.Instance.showAdList(title, TnkAd.AdListType.ALL, TnkAd.TemplateStyle.BLUE_01);
		}

		if (GUI.Button(new Rect(100, 200, 200, 80), "Show Offerwall (Popup)"))
		{
			Debug.Log("Offerwall Ad");

			//TnkAd.Plugin.Instance.popupAdList();
			//TnkAd.Plugin.Instance.popupAdList(TnkAd.AdListType.CPS);
			//TnkAd.Plugin.Instance.popupAdList(title);
			//TnkAd.Plugin.Instance.popupAdList(title, TnkAd.AdListType.CPS);
			//TnkAd.Plugin.Instance.popupAdList(title, "testhandler");
			//TnkAd.Plugin.Instance.popupAdList(title, TnkAd.AdListType.CPS, "testhandler");
			//TnkAd.Plugin.Instance.popupAdList(title, TnkAd.AdListType.ALL, TnkAd.TemplateStyle.RED_01);
			TnkAd.Plugin.Instance.popupAdList(title, TnkAd.AdListType.ALL, "testhandler", TnkAd.TemplateStyle.RED_01);
		}

		if (GUI.Button(new Rect(100, 300, 200, 80), "Show Tab Offerwall (Activity)"))
		{
			Debug.Log("Tab Offerwall Ad");

			//TnkAd.Plugin.Instance.showAdListTab();
			//TnkAd.Plugin.Instance.showAdListTab(title);
			//TnkAd.Plugin.Instance.showAdListTab(TnkAd.TemplateStyle.BLUE_TAB_01);
			TnkAd.Plugin.Instance.showAdListTab(title, TnkAd.TemplateStyle.BLUE_TAB_01);
		}

		if (GUI.Button(new Rect(100, 400, 200, 80), "Query point"))
		{
			Debug.Log("Query point");

			TnkAd.Plugin.Instance.queryPoint("testhandler");
		}

		if (GUI.Button(new Rect(100, 500, 200, 80), "Purchase Item"))
		{
			Debug.Log("Purchase Item");

			TnkAd.Plugin.Instance.purchaseItem(100, "item01", "testhandler");
		}

		if (GUI.Button(new Rect(100, 600, 200, 80), "ETC Test"))
		{
			Debug.Log("ETC Test 2");

			TnkAd.Plugin.Instance.queryPublishState("testhandler");
		}

		if (GUI.Button(new Rect(100, 700, 200, 80), "PPI Interstital"))
		{
			Debug.Log("Interstitial AD (PPI)");

			// No Handler
			//TnkAd.Plugin.Instance.prepareInterstitialAdForPPI();
			//TnkAd.Plugin.Instance.showInterstitialAdForPPI();

			// Handler
			TnkAd.Plugin.Instance.prepareInterstitialAdForPPI("testhandler");
			TnkAd.Plugin.Instance.showInterstitialAdForPPI("testhandler");
		}
	}
}