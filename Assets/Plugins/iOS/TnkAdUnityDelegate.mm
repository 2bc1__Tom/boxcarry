//
//  TnkAdCocos2dxDelegate.m
//  spaceship
//
//  Created by Hyeongdo Kim on 2014. 2. 28..
//
//

#import "TnkAdUnityDelegate.h"

#ifdef UNITY_4_2_0
    #import "UnityAppController.h"
#else
    #import "AppController.h"
#endif

static NSMutableDictionary *sharedInstance = nil;

@implementation TnkAdUnityDelegate

+ (TnkAdUnityDelegate *) sharedInstance:(NSString *)hname {
    TnkAdUnityDelegate *delegate = nil;
    
    @synchronized(self) {
        if (sharedInstance == nil) {
            sharedInstance = [[NSMutableDictionary alloc] init];
        }
        
        delegate = [sharedInstance objectForKey:hname];
        
        if (delegate == nil) {
            delegate = [[TnkAdUnityDelegate alloc] initWithHandleName:hname];
            
            if (delegate != nil) {
                [sharedInstance setObject:delegate forKey:hname];
            }
        }
    }
    
    return delegate;
}

- (id) initWithHandleName:(NSString *)hname {
    self = [super init];
    
    if (self) {
        self.handleName = hname;
    }
    
    return self;
}

#pragma mark - TnkAdViewDelegate

- (void)adViewDidClose:(int)type {
    NSLog(@"TnkAdUnity adViewDidClose handler=%@ error=%d", self.handleName, type);
    UnityPause(false);
    if (![self.handleName isEqualToString:@"__tnk_default__"]) {
        NSString *param = [NSString stringWithFormat:@"%d", type];
        UnitySendMessage(self.handleName.UTF8String, "onCloseBinding", param.UTF8String);
    }
}

- (void)adViewDidFail:(int)errCode {
    NSLog(@"TnkAdUnity adViewDidFail handler=%@ error=%d", self.handleName, errCode);
    if (![self.handleName isEqualToString:@"__tnk_default__"]) {
        NSString *param = [NSString stringWithFormat:@"%d", errCode];
        UnitySendMessage(self.handleName.UTF8String, "onFailureBinding", param.UTF8String);
    }
}

- (void)adViewDidShow {
    NSLog(@"TnkAdUnity adViewDidShow handler=%@", self.handleName);
    UnityPause(true);
    if (![self.handleName isEqualToString:@"__tnk_default__"]) {
        UnitySendMessage(self.handleName.UTF8String, "onShowBinding", "0");
    }
}

- (void)adViewDidLoad {
    NSLog(@"TnkAdUnity adViewDidLoad handler=%@", self.handleName);
    UnityPause(false);
    if (![self.handleName isEqualToString:@"__tnk_default__"]) {
        UnitySendMessage(self.handleName.UTF8String, "onLoadBinding", "0");
    }
}

- (void)adListViewClosed {
    NSLog(@"TnkAdUnity adListViewClosed handler=%@", self.handleName);
    UnityPause(false);
    if (![self.handleName isEqualToString:@"__tnk_default__"]) {
        UnitySendMessage(self.handleName.UTF8String, "onCloseBinding", "0");
    }
}

#pragma mark - TnkVideoDelegate

- (void)didVideoClose:(NSString *)logicName close:(int)type {
    NSLog(@"TnkAdUnity didVideoClose handler=%@ logic=%@ type=%d", self.handleName, logicName, type);
    if (![self.handleName isEqualToString:@"__tnk_default__"]) {
        NSString *param = [NSString stringWithFormat:@"%d", type];
        UnitySendMessage(self.handleName.UTF8String, "onCloseBinding", param.UTF8String);
    }
}

- (void)didVideoShow:(NSString *)logicName {
    NSLog(@"TnkAdUnity didVideoShow handler=%@ logic=%@", self.handleName, logicName);
    if (![self.handleName isEqualToString:@"__tnk_default__"]) {
        UnitySendMessage(self.handleName.UTF8String, "onShowBinding", "0");
    }
}

- (void)didVideoLoad:(NSString *)logicName {
    NSLog(@"TnkAdUnity didVideoLoad handler=%@ logic=%@", self.handleName, logicName);
    if (![self.handleName isEqualToString:@"__tnk_default__"]) {
        UnitySendMessage(self.handleName.UTF8String, "onLoadBinding", "0");
    }
}

- (void)didVideoCompleted:(NSString *)logicName skip:(BOOL)skipped {
    NSLog(@"TnkAdUnity didVideoCompleted handler=%@ logic=%@", self.handleName, logicName);
    if (![self.handleName isEqualToString:@"__tnk_default__"]) {
        NSString *param = [NSString stringWithFormat:@"%d", skipped?1:0];
        UnitySendMessage(self.handleName.UTF8String, "onVideoCompletedBinding", param.UTF8String);
    }
}

// 2016.07.05 추가
- (void)didVideoFail:(NSString *)logicName error:(int)errCode {
    NSLog(@"TnkAdUnity didVideoFail handler=%@ logic=%@ error=%d", self.handleName, logicName, errCode);
    if (![self.handleName isEqualToString:@"__tnk_default__"]) {
        NSString *param = [NSString stringWithFormat:@"%d", errCode];
        UnitySendMessage(self.handleName.UTF8String, "onFailureBinding", param.UTF8String);
    }
}

#pragma mark - ServiceCallback

- (void)onReturnQueryPoint:(NSNumber *)point {
    NSLog(@"TnkAdUnity onReturnQueryPoint handler=%@ point=%d", self.handleName, [point integerValue]);
    UnitySendMessage(self.handleName.UTF8String, "onReturnQueryPointBinding", [point stringValue].UTF8String);
}

- (void)onReturnWithdrawPoints:(NSNumber *)point {
    NSLog(@"TnkAdUnity onReturnWithdrawPoints handler=%@ point=%d", self.handleName, [point integerValue]);
    UnitySendMessage(self.handleName.UTF8String, "onReturnWithdrawPointsBinding", [point stringValue].UTF8String);
}

- (void)onReturnPurchaseItem:(NSNumber *)point seqId:(NSNumber *)seqId {
    NSLog(@"TnkAdUnity onReturnPurchaseItem handler=%@ point=%d seq=%d", self.handleName, [point integerValue], [seqId integerValue]);
    NSString *param = [NSString stringWithFormat:@"%ld,%ld", [point longValue], [seqId longValue]];
    UnitySendMessage(self.handleName.UTF8String, "onReturnPurchaseItemBinding", param.UTF8String);
}

- (void)onReturnQueryPublishState:(NSNumber *)state {
    NSLog(@"TnkAdUnity onReturnQueryPublishState handler=%@ state=%d", self.handleName, [state integerValue]);
    UnitySendMessage(self.handleName.UTF8String, "onReturnQueryPublishStateBinding", [state stringValue].UTF8String);
}

@end
