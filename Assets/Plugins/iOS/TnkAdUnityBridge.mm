//
//  TnkAdUnityBridge.mm
//  Unity-iPhone
//
//  Created by Hyeongdo Kim on 2014. 8. 7..
//
//

#import "tnksdk.h"
#import "TnkAdUnityDelegate.h"

#ifdef UNITY_4_2_0
    #import "UnityAppController.h"
#else
    #import "AppController.h"
#endif

extern "C" {

    void applicationStarted_TnkUnityBridge() {
        NSLog(@"TnkAdUnity applicationStarted_UnityBridge");
        [[TnkSession sharedInstance] applicationStarted];
    }
    
    void actionCompleted_TnkUnityBridge(const char* actionName) {
        NSLog(@"TnkAdUnity actionCompleted_UnityBridge %s", actionName);
        NSString *actionString = nil;
        if (actionName != NULL) {
            actionString = [NSString stringWithUTF8String:actionName];
        }
        
        [[TnkSession sharedInstance] actionCompleted:actionString];
    }
    
    void buyCompleted_TnkUnityBridge(const char* itemName) {
        NSLog(@"TnkAdUnity buyCompleted_UnityBridge %s", itemName);
        
        NSString *itemString = [NSString stringWithUTF8String:itemName];
        
        [[TnkSession sharedInstance] buyCompleted:itemString];
    }
    
    // interstitial ad
    void prepareInterstitialAd_TnkUnityBridge(const char* logicName, const char* hname) {
        NSLog(@"TnkAdUnity prepareInterstitialAd_UnityBridge %s %s", logicName, hname);
        
        NSString *logicNameString = [NSString stringWithUTF8String:logicName];
        
        TnkAdUnityDelegate *delegate = nil;
        if (hname != NULL) {
            delegate = [TnkAdUnityDelegate sharedInstance:[NSString stringWithUTF8String:hname]];
        }
        else {
            // set default delegate
            delegate = [TnkAdUnityDelegate sharedInstance:@"__tnk_default__"];
        }
        
        [[TnkSession sharedInstance] prepareInterstitialAd:logicNameString delegate:delegate];
    }
    
    void showInterstitialAd_TnkUnityBridge() {
        NSLog(@"TnkAdUnity showInterstitialAd_UnityBridge");
        [[TnkSession sharedInstance] showInterstitialAd];
    }
    
    BOOL isInterstitialAdVisible_TnkUnityBridge() {
        return [[TnkSession sharedInstance] isInterstitialAdVisible];
    }
    
    // video ad
    void prepareVideoAd_TnkUnityBridge(const char* logicName, const char* hname) {
        NSLog(@"TnkAdUnity prepareVideoAd_UnityBridge : %s %s", logicName, hname);

        NSString *logicNameString = [NSString stringWithUTF8String:logicName];
        
        TnkAdUnityDelegate *delegate = nil;
        if (hname != NULL) {
            delegate = [TnkAdUnityDelegate sharedInstance:[NSString stringWithUTF8String:hname]];
        }
        else {
            // set default delegate
            delegate = [TnkAdUnityDelegate sharedInstance:@"__tnk_default__"];
        }
        
        [[TnkSession sharedInstance] prepareVideoAd:logicNameString delegate:delegate];
    }
    
    // video ad
    void prepareVideoAdOnce_TnkUnityBridge(const char* logicName, const char* hname) {
        NSLog(@"TnkAdUnity prepareVideoAdOnce_UnityBridge : %s %s", logicName, hname);
        
        NSString *logicNameString = [NSString stringWithUTF8String:logicName];
        
        TnkAdUnityDelegate *delegate = nil;
        if (hname != NULL) {
            delegate = [TnkAdUnityDelegate sharedInstance:[NSString stringWithUTF8String:hname]];
        }
        else {
            // set default delegate
            delegate = [TnkAdUnityDelegate sharedInstance:@"__tnk_default__"];
        }
        
        [[TnkSession sharedInstance] prepareVideoAd:logicNameString delegate:delegate repeat:NO];
    }
    
    void showVideoAd_TnkUnityBridge(const char* logicName) {
        NSLog(@"TnkAdUnity showVideoAd_UnityBridge : %s", logicName);
        
        if (logicName != NULL) {
            NSString *logicNameString = [NSString stringWithUTF8String:logicName];
            
            [[TnkSession sharedInstance] showVideoAd:logicNameString on:nil];
        }
        else {
            [[TnkSession sharedInstance] showVideoAd:nil on:nil];
        }
    }
    
    BOOL hasVideoAd_TnkUnityBridge(const char* logicName) {
        NSString *logicNameString = nil;
        if (logicName != NULL) {
            logicNameString = [NSString stringWithUTF8String:logicName];
        }
        
        return [[TnkSession sharedInstance] hasVideoAd:logicNameString];
    }
    
    void hideVideoClose_TnkUnityBridge() {
        [[TnkSession sharedInstance] setNoVideoClose:YES];
    }
    
    void showVideoClose_TnkUnityBridge() {
        [[TnkSession sharedInstance] setNoVideoClose:NO];
    }
    
    // offerwall
    void showAdList_TnkUnityBridge(const char* title, const char* hname) {
        NSLog(@"TnkAdUnity showAdList_UnityBridge %s %s", title, hname);
        
        NSString *titleString = nil;
        if (title != NULL) {
            titleString = [NSString stringWithUTF8String:title];
        }
        
        TnkAdUnityDelegate *delegate = nil;
        if (hname != NULL) {
            delegate = [TnkAdUnityDelegate sharedInstance:[NSString stringWithUTF8String:hname]];
        }
        else {
            // set default delegate
            delegate = [TnkAdUnityDelegate sharedInstance:@"__tnk_default__"];
        }
        
        UIWindow *window = [[UIApplication sharedApplication] keyWindow];
        [[TnkSession sharedInstance] showAdListAsModal:window.rootViewController title:titleString delegate:delegate];
        
        UnityPause(true);
    }
    
    void queryPoint_TnkUnityBridge(const char* hname) {
        NSLog(@"TnkAdUnity queryPoints_UnityBridge %s", hname);
        NSString *handlerName = [NSString stringWithUTF8String:hname];
        TnkAdUnityDelegate *delegate = [TnkAdUnityDelegate sharedInstance:handlerName];
        
        [[TnkSession sharedInstance] queryPoint:delegate action:@selector(onReturnQueryPoint:)];
    }
    
    void withdrawPoints_TnkUnityBridge(const char* desc, const char* hname) {
        NSLog(@"TnkAdUnity withdrawPoints_UnityBridge %s %s", desc, hname);

        NSString *handlerName = [NSString stringWithUTF8String:hname];
        NSString *descStr = [NSString stringWithUTF8String:desc];
        
        TnkAdUnityDelegate *delegate = [TnkAdUnityDelegate sharedInstance:handlerName];
        
        [[TnkSession sharedInstance] withdrawPoints:descStr target:delegate action:@selector(onReturnQueryPoint:)];
    }
    
    void purchaseItem_TnkUnityBridge(int cost, const char* itemName, const char* hname) {
        NSLog(@"TnkAdUnity purchaseItem_UnityBridge %d %s %s", cost, itemName, hname);
        
        NSString *itemNameString = [NSString stringWithUTF8String:itemName];
        NSString *handlerName = [NSString stringWithUTF8String:hname];
        TnkAdUnityDelegate *delegate = [TnkAdUnityDelegate sharedInstance:handlerName];
        
        [[TnkSession sharedInstance] purchaseItem:itemNameString cost:cost target:delegate action:@selector(onReturnPurchaseItem:seqId:)];
        
    }
    
    void queryPublishState_TnkUnityBridge(const char* hname) {
        NSLog(@"TnkAdUnity queryPublishState_UnityBridge %s", hname);

        NSString *handlerName = [NSString stringWithUTF8String:hname];
        TnkAdUnityDelegate *delegate = [TnkAdUnityDelegate sharedInstance:handlerName];
        
        [[TnkSession sharedInstance] queryPublishState:delegate action:@selector(onReturnQueryPublishState:)];
    }
    
    void setUserName_TnkUnityBridge(const char* userName) {
        if (userName != NULL) {
            NSString *userNameString = [NSString stringWithUTF8String:userName];
            [[TnkSession sharedInstance] setUserName:userNameString];
        }
    }

    void setUserAge_TnkUnityBridge(int age) {
        [[TnkSession sharedInstance] setUserAge:age];
    }
    
    void setUserGender_TnkUnityBridge(int gender) {
        if (gender == 0) {
            [[TnkSession sharedInstance] setUserGender:@"M"];
        }
        else if (gender == 1) {
            [[TnkSession sharedInstance] setUserGender:@"F"];
        }
        
    }
    
    void setCloseButtonAlignLeft_TnkUnityBridge() {
        [TnkSession sharedInstance].interstitialCloseButtonAlignRight = NO;
    }
    
    void setCloseButtonAlignRight_TnkUnityBridge() {
        [TnkSession sharedInstance].interstitialCloseButtonAlignRight = YES;
    }
    
    void setLeftButtonLabel_TnkUnityBridge(const char* label) {
        if (label != NULL) {
            [TnkSession sharedInstance].interstitialLeftButtonLabel = [NSString stringWithUTF8String:label];
        }
        else {
            [TnkSession sharedInstance].interstitialLeftButtonLabel = nil;
        }
    }
    
    void setRightButtonLabel_TnkUnityBridge(const char* label) {
        if (label != NULL) {
            [TnkSession sharedInstance].interstitialRightButtonLabel = [NSString stringWithUTF8String:label];
        }
        else {
            [TnkSession sharedInstance].interstitialRightButtonLabel = nil;
        }
    }
}
