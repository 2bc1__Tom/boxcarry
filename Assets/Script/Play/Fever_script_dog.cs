﻿using UnityEngine;
using System.Collections;

public class Fever_script_dog : MonoSingleton<Fever_script_dog>
{

    public GameObject ob_FeverTime;

    public GameObject GoldPrefab; //골드 프리팹

    public GameObject ob_bg; //뒤에 깔리는 기본 배경 오브젝트(구름,반투명 빌딩)
    public GameObject ob_Fever_bg; //피버 배경 오브젝트
    public GameObject ob_Sun; //태양 오브젝트

    public GameObject ob_center; //가운데 춤추는 캐릭터

    public GameObject ob_character_left_building; //왼쪽 건물 옥상 캐릭터 오브젝트
    public GameObject ob_character_right_building; //오른쪽 건물 옥상 캐릭터 오브젝트

    public GameObject sp_dance_time; //댄스타임 타이틀 이미지 
    public GameObject sp_mirror_ball; //미러볼 이미지
    public GameObject sp_lighting_left; //상단 왼쪽 라이트
    public GameObject sp_lighting_right; //상단 오른쪽 라이트
    public GameObject sp_circle_lighting_left; //하단 왼쪽 둥근 라이트
    public GameObject sp_circle_lighting_right; //하단 오른쪽 둥근 라이트
    public GameObject sp_building_left; //왼쪽 빌딩
    public GameObject sp_building_right; //오른쪽 빌딩

    public GameObject sp_building_light_left; //왼쪽 빌딩 조명
    public GameObject sp_building_light_right; //오른쪽 빌딩 조명

    private float mSun_rote = 0; //태양 이미지 회전 각도.

    private float mRun_frameIndex = 1; //달리기 프레임 인덱스

    //중간 캐릭터 댄스 프레임 인덱스
    private int mDance_frame_Index_middle_1 = 1;
    private int mDance_frame_Index_middle_2 = 1;
    private int mDance_frame_Index_middle_3 = 1;

    private int mDance_frame_Index_long = 1; //키큰 캐릭터 프레임 인덱스
    private int mDance_frame_Index_girl = 1; //여자 캐릭터 프레임 인덱스

    private int mCircle_Lighting_index = 1; //하단 둥근 조명 프레임 인덱스
    private int mLighting_index = 1; //하단 둥근 조명 프레임 인덱스

    private bool mFever_Time = true; //피버타임인지 아닌지 구분
    private bool mRun_Frame = true; //달려가는 프레임인지 아닌지 구분
    private bool mDance_Frame = false; //댄스 프렘인지 아닌지 구분

    // Use this for initialization
    void Start()
    {
    }

    public void FeverTime_Start()
    {
        mFever_Time = true;

        //중간 캐릭터 댄스 프레임 인덱스
        mDance_frame_Index_middle_1 = 1;
        mDance_frame_Index_middle_2 = 1;
        mDance_frame_Index_middle_3 = 1;

        mDance_frame_Index_long = 1; //키큰 캐릭터 프레임 인덱스
        mDance_frame_Index_girl = 1; //여자 캐릭터 프레임 인덱스

        mCircle_Lighting_index = 1; //하단 둥근 조명 프레임 인덱스
        mLighting_index = 1; //하단 둥근 조명 프레임 인덱스

        ob_bg.SetActive(false); //구름,반투명 빌딩 배경 OFF
        TweenScript.Instance.setTweenRotation(ob_Sun, 0f, 2f, true); //태양 이미지 회전으로 다른 태양 이미지로 교체

        ob_Fever_bg.SetActive(true);

        sp_building_left.SetActive(true);
        sp_building_right.SetActive(true);

        TweenScript.Instance.setTweenAlpha(ob_Fever_bg, 0f, 1.5f, 0, 1); //피버타임 배경 이미지 알파 애니메이션 ON
        TweenScript.Instance.setTweenAlpha(ob_bg, 0f, 1.5f, 1, 0); //구름, 반투명 빌딩 알파 애니메이션 OFF

        TweenScript.Instance.setTweenAlpha(sp_building_left, 0f, 1.5f, 0, 1); //왼쪽 피버 건물 알파 애니메이션
        TweenScript.Instance.setTweenAlpha(sp_building_right, 0f, 1.5f, 0, 1); //왼쪽 피버 건물 알파 애니메이션

        TweenScript.Instance.setTweenPosition(sp_mirror_ball, 2f, 1f, new Vector3(0f, 336f, 0f)); //미러볼 등장 애니메이션

        //캐릭터 등장 애니메이션
        TweenPosition tweenPosition_center = TweenScript.Instance.setTweenPosition(ob_center, 2f, 2.5f, new Vector3(0f, -323.9f, 0f)); //가운데서 여자 캐릭터 등장

        TweenScript.Instance.setTweenPosition(ob_character_left_building, 1f, 1f, new Vector3(-27f, -6f, 0f)); //왼쪽 옥상에서 캐릭터 등장
        TweenScript.Instance.setTweenPosition(ob_character_right_building, 1f, 1f, new Vector3(-27f, -6f, 0f)); //오른쪽 옥상에서 캐릭터 등장

        //가운데 여자 캐릭터 등장 애니 종료 후 이벤트 발생
        EventDelegate eventAni_centet = TweenScript.Instance.setEventDelegate(ob_center, gameObject.GetComponent<Fever_script_dog>(), "animationEnd");
        EventDelegate.Add(tweenPosition_center.onFinished, eventAni_centet); // 이벤트 추가 
    }

    public void FeverTime_End()
    {

        mFever_Time = false;

        mDance_Frame = false; //댄스 OFF
        mRun_Frame = true;

        ob_bg.SetActive(true); //구름,반투명 빌딩 배경 OFF
        sp_dance_time.SetActive(false); //댄스타임 타이틀 이미지 OFF

        sp_lighting_left.SetActive(false);
        sp_lighting_right.SetActive(false);
        sp_circle_lighting_left.SetActive(false);
        sp_circle_lighting_right.SetActive(false);

        TweenScript.Instance.setTweenRotation(ob_Sun, 0f, 2f, false); //태양 이미지 회전으로 다른 태양 이미지로 교체

        TweenScript.Instance.setTweenAlpha(ob_Fever_bg, 0f, 1.5f, 1, 0); //피버타임 배경 이미지 알파 애니메이션 OFF
        TweenScript.Instance.setTweenAlpha(ob_bg, 0f, 1.5f, 0, 1); //구름, 반투명 빌딩 알파 애니메이션 ON

        TweenScript.Instance.setTweenAlpha(sp_building_left, 0f, 1.5f, 1, 0); //왼쪽 피버 건물 알파 애니메이션 OFF
        TweenScript.Instance.setTweenAlpha(sp_building_right, 0f, 1.5f, 1, 0); //왼쪽 피버 건물 알파 애니메이션 OFF

        TweenScript.Instance.setTweenPosition(sp_mirror_ball, 0f, 1f, new Vector3(0f, 521f, 0f)); //미러볼 퇴장 애니메이션

        //캐릭터 퇴장 애니메이션
        TweenPosition tweenPosition_center = TweenScript.Instance.setTweenPosition(ob_center, 0f, 1f, new Vector3(0f, -580f, 0f)); //가운데서 여자 캐릭터 퇴장

        TweenScript.Instance.setTweenPosition(ob_character_left_building, 0f, 1f, new Vector3(-27f, -124f, 0f)); //왼쪽 옥상에서 캐릭터 퇴장
        TweenScript.Instance.setTweenPosition(ob_character_right_building, 0f, 1f, new Vector3(-27f, -153f, 0f)); //오른쪽 옥상에서 캐릭터 퇴장

        //옥상 캐릭터가 빌딩 보다 뒤로 가게
        for (int i = 1; i < 4; i++)
        {
            GameObject charaacter_bulling_left = ob_character_left_building.transform.Find("sp_character_" + i).gameObject;
            GameObject charaacter_bulling_right = ob_character_right_building.transform.Find("sp_character_" + i).gameObject;

            charaacter_bulling_left.transform.GetComponent<UISprite>().depth = 2;
            charaacter_bulling_right.transform.GetComponent<UISprite>().depth = 2;
        }
    }

    public void animationEnd(GameObject _gameObject)
    {

        if (!mFever_Time) return; //피버타임이 아닐 땐 막아둠

        if (_gameObject.name.Contains("ob_center")) //가운데 여자 캐릭터 등장 애니 끝난 뒤
        {
            sp_dance_time.SetActive(true);

            //상단 양쪽 조명 이미지 ON
            sp_lighting_left.SetActive(true);
            sp_lighting_right.SetActive(true);

            sp_circle_lighting_left.SetActive(true);
            sp_circle_lighting_right.SetActive(true);

            //옥상 캐릭터 조명 보다 위로 올라 올 수 있게 댑스 설정
            for (int i = 1; i < 4; i++)
            {
                GameObject charaacter_bulling_left = ob_character_left_building.transform.Find("sp_character_" + i).gameObject;
                GameObject charaacter_bulling_right = ob_character_right_building.transform.Find("sp_character_" + i).gameObject;

                charaacter_bulling_left.transform.GetComponent<UISprite>().depth = 15;
                charaacter_bulling_right.transform.GetComponent<UISprite>().depth = 15;
            }

            //댄스타임 타이틀 애니메이셔 이 후 시작되기 위해 잠시 딜레이를 줌
            StartCoroutine(Dance_Delay());

        }
    }

    //댄스 애니 시작 전 잠시 들레이
    IEnumerator Dance_Delay()
    {

        yield return new WaitForSeconds(2.5f);

        mDance_Frame = true;

        StartCoroutine(Gold_Plus()); //골드+ 애니메이션 실행
        StartCoroutine(Dance_Frame()); //댄스 애니메이션 실행
        StartCoroutine(Dance_dog_Frame());
    }

    //골드+ 애니메이션
    IEnumerator Gold_Plus()
    {

        while (mDance_Frame)
        {
            //============ 골드 생성 ==============

            int BoxLevel = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + 4); //상자 아이템의 레벨을 가져온다.
            int Gold_Buff = Prefs.getPrefsInt(Prefs.GOLD_BUFF); //골드 버프 값을 가져온다.

            long gold = (long)Strings.ITME_LEVEL_UP[4][BoxLevel] * Strings.GOLD_BUFF[Gold_Buff] * 2; // + 골드 2배

            //골드 생성
            GameObject sp_gold = Instantiate(GoldPrefab) as GameObject;
            sp_gold.transform.parent = ob_FeverTime.transform;
            sp_gold.transform.localScale = Vector3.one;

            sp_gold.name = "fever_gold";
            sp_gold.transform.GetComponent<UISprite>().depth = 20;
            sp_gold.transform.Find("Label_gold").GetComponent<UILabel>().text = "+" + gold;

            //골드 생성 위치 랜덤 하게 설정
            int randomX = UnityEngine.Random.Range(-339, 302);
            int randomY = UnityEngine.Random.Range(-366, 0);

            sp_gold.transform.localPosition = new Vector3(randomX, randomY, 0f); //골드 오브젝트 위치 지정

            TweenScript.Instance.setTweenPosition(sp_gold, 0, 0.6f, new Vector3(-248.35f, 366.82f, 0f)); //지정된 위치로 골드가 날아감.

            yield return new WaitForSeconds(0.05f);
        }
    }

    IEnumerator Dance_dog_Frame()
    {

        GameObject charaacter_bulling_left_1 = ob_character_left_building.transform.Find("sp_character_" + 1).gameObject;
        GameObject charaacter_bulling_left_3 = ob_character_left_building.transform.Find("sp_character_" + 3).gameObject;

        GameObject charaacter_bulling_right_1 = ob_character_right_building.transform.Find("sp_character_" + 1).gameObject;
        GameObject charaacter_bulling_right_3 = ob_character_right_building.transform.Find("sp_character_" + 3).gameObject;

        GameObject charaacter_1 = ob_center.transform.Find("sp_character_1").gameObject;
        GameObject charaacter_2 = ob_center.transform.Find("sp_character_2").gameObject;
        GameObject charaacter_3 = ob_center.transform.Find("sp_character_3").gameObject;
        GameObject charaacter_4 = ob_center.transform.Find("sp_character_4").gameObject;
        GameObject charaacter_5 = ob_center.transform.Find("sp_character_5").gameObject;

        while (mDance_Frame)
        {

            //------- 옥상 댄스 ----------

            if (mDance_frame_Index_middle_1 == 9)
            {
                mDance_frame_Index_middle_1 = 1;
            }

            charaacter_bulling_left_1.transform.GetComponent<UISprite>().spriteName = ImgDatas.CHARACTER_DANCE_DOG[2] + mDance_frame_Index_middle_1;
            charaacter_bulling_right_1.transform.GetComponent<UISprite>().spriteName = ImgDatas.CHARACTER_DANCE_DOG[1] + mDance_frame_Index_middle_1;

            charaacter_bulling_left_3.transform.GetComponent<UISprite>().spriteName = ImgDatas.CHARACTER_DANCE_DOG[1] + mDance_frame_Index_middle_1;
            charaacter_bulling_right_3.transform.GetComponent<UISprite>().spriteName = ImgDatas.CHARACTER_DANCE_DOG[0] + mDance_frame_Index_middle_1;

            charaacter_1.transform.GetComponent<UISprite>().spriteName = ImgDatas.CHARACTER_DANCE_DOG[0] + mDance_frame_Index_middle_1;
            charaacter_2.transform.GetComponent<UISprite>().spriteName = ImgDatas.CHARACTER_DANCE_DOG[2] + mDance_frame_Index_middle_1;
            charaacter_3.transform.GetComponent<UISprite>().spriteName = ImgDatas.CHARACTER_DANCE_DOG[1] + mDance_frame_Index_middle_1;
            charaacter_4.transform.GetComponent<UISprite>().spriteName = ImgDatas.CHARACTER_DANCE_DOG[2] + mDance_frame_Index_middle_1;
            charaacter_5.transform.GetComponent<UISprite>().spriteName = ImgDatas.CHARACTER_DANCE_DOG[0] + mDance_frame_Index_middle_1;

            mDance_frame_Index_middle_1++;

            yield return new WaitForSeconds(0.05f);
        }
    }

    //댄스 프레임 애니메이션
    IEnumerator Dance_Frame()
    {

        GameObject charaacter_bulling_left_2 = ob_character_left_building.transform.Find("sp_character_" + 2).gameObject;
        GameObject charaacter_bulling_right_2 = ob_character_right_building.transform.Find("sp_character_" + 2).gameObject;

        while (mDance_Frame)
        {

            //------- 왼쪽 옥상 졸라맨 댄스2 ----------

            if (mDance_frame_Index_middle_2 == 4)
            {
                mDance_frame_Index_middle_2 = 1;
            }

            charaacter_bulling_left_2.transform.GetComponent<UISprite>().spriteName = ImgDatas.CHARACTER_DANCE[0][1] + mDance_frame_Index_middle_2;
            mDance_frame_Index_middle_2++;


            //-------- 오른쪽 옥상 졸라맨 댄스2  ------------

            if (mDance_frame_Index_girl == 5)
            {
                mDance_frame_Index_girl = 1;
            }

            charaacter_bulling_right_2.transform.GetComponent<UISprite>().spriteName = ImgDatas.CHARACTER_DANCE[2][0] + mDance_frame_Index_girl;
            mDance_frame_Index_girl++;

            //-------- 하단 둥근 조명 프레임 ---------

            if (mCircle_Lighting_index == 4)
            {
                mCircle_Lighting_index = 1;
            }

            sp_circle_lighting_left.transform.GetComponent<UISprite>().spriteName = ImgDatas.LIGHTING_IMG + mCircle_Lighting_index;
            sp_circle_lighting_right.transform.GetComponent<UISprite>().spriteName = ImgDatas.LIGHTING_IMG + mCircle_Lighting_index;

            mCircle_Lighting_index++;

            //-------- 조명 프레임 ----------

            if (mLighting_index == 3)
            {
                mLighting_index = 1;
            }

            if (mLighting_index == 1)
            {

                sp_lighting_left.SetActive(true);
                sp_lighting_right.SetActive(false);

            }
            else
            {

                sp_lighting_left.SetActive(false);
                sp_lighting_right.SetActive(true);
            }

            //빌딩 불빛 프레임
            sp_building_light_left.transform.GetComponent<UISprite>().spriteName = ImgDatas.BUILDING_LIGHTING_IMG + (mLighting_index + 1);
            sp_building_light_right.transform.GetComponent<UISprite>().spriteName = ImgDatas.BUILDING_LIGHTING_IMG + (mLighting_index + 1);

            mLighting_index++;

            yield return new WaitForSeconds(0.1f);
        }
    }
}
