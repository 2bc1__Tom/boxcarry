﻿using UnityEngine;
using System.Collections;

public class Main_script : MonoSingleton<Main_script>
{
   
    public GameObject mFadeInOut;
    
    float FADE_DURATION = 0.5f;

    void Start()
    {

        //Tapjoy.ShowOffers();

        Screen.sleepTimeout = SleepTimeout.NeverSleep; //화면 꺼짐 방지

        SceneManager.Instance.onFadeIn = fadeIn;
        SceneManager.Instance.onFadeOut = fadeOut;
        SceneManager.Instance.onExit = exit;
        SceneManager.Instance.onSceneChanged = sceneChanged;

        SceneManager.Instance.pushScene("Intro_Scene"); //인트로 씬 호출

        //ES에 저장되어 있는 데이터를 Prefs로 옮긴다.
        if (!Prefs.getPrefsBoolean(Prefs.DATA_CHANGE))
        {
            long gold = 0;

            if (ES2.Exists(Prefs.PREFS_GOLD) == true)
            {
                gold = ES2.Load<long>(Prefs.PREFS_GOLD);
            }

            Prefs.Gold_Save(gold); //골드 저장
            Prefs.setPrefsBoolean(Prefs.DATA_CHANGE , true); //데이터 백업 저장
        }

        //Prefs.Gold_Save(0);

        //처음 최초 1회 사운드 비지엠,효과음 설정을 ON 상태로 저장해 둠.
        if (!Prefs.getPrefsBoolean(Prefs.SOUND))
        {
            Prefs.setPrefsBoolean(Prefs.SOUND_BGM, true);
            Prefs.setPrefsBoolean(Prefs.SOUND_EFFECT, true);

            Prefs.setPrefsBoolean(Prefs.SOUND, true);
        }

        Prefs.setPrefsBoolean(Prefs.SPECIES_BUY + 0 , true); //인간 종족 상태 저장

        string language = Application.systemLanguage.ToString();

        if (language.Contains("Korean")) //한국어
        {
            Strings.LANGUAGE_TYPE = 0;
        }
        else if (language.Contains("Japanese")) //일본어
        {
            Strings.LANGUAGE_TYPE = 3;
        }
        else if (language.Contains("Chinese")) //중국어
        {
            Strings.LANGUAGE_TYPE =2;
        }
        else //영어
        {
            Strings.LANGUAGE_TYPE = 1;
        }

        SoundPlay(0, "mainBGM");
    }

    //사운드 재생
    public void SoundPlay(int type, string SoundName)
    {

        if (type == 0) //비지엠
        {

            if (Prefs.getPrefsBoolean(Prefs.SOUND_BGM)) //비지엠이 ON 일 때만
            {
                AudioController.PlayMusic(SoundName);
            }
        }
        else //효과음
        {
            if (Prefs.getPrefsBoolean(Prefs.SOUND_EFFECT)) //효과음이 ON 일 때만
            {
                AudioController.Play(SoundName);
            }
        }
    }

    void exit() {
        
        //Become21Plugin.BackPressed();
    }

    IEnumerator fadeIn()
    {
        //FadeIn 효과
        mFadeInOut.SetActive(true);
        TweenAlpha.Begin(mFadeInOut, FADE_DURATION, 1f);
        yield return new WaitForSeconds(FADE_DURATION);
    }

    IEnumerator fadeOut()
    {
        //FadeOut 효과
        TweenAlpha.Begin(mFadeInOut, FADE_DURATION, 0f);
        yield return new WaitForSeconds(FADE_DURATION);

        mFadeInOut.SetActive(false);
    }

    void sceneChanged(string newSceneName)
    {
        //if (mIntro != null)
        //{
        //    Destroy(mIntro);
        //    mIntro = null;
        //}

        //AudioManager.StopMusic();

        //switch (newSceneName)
        //{
        //    case Strings.SCENE_MAIN:
        //        {
        //            Become21Plugin.SetBottomAdVisible(true);
        //            Become21Plugin.setBannerAdVisible(true);
        //            //			AudioManager.PlayMusic("b_night_basic");
        //            break;
        //        }
        //}
    }

}
