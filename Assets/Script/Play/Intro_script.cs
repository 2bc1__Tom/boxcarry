﻿using UnityEngine;
using System.Collections;

public class Intro_script : MonoBehaviour
{

    private bool mScene_change = false; //씬 체인지

    // Use this for initialization
    void Start()
    {
        if (!Social.localUser.authenticated) //로그인 상태가 아니라면
        {
            Google_play.Instance.LogIn(); //구글 플레이 로그인
        }
    }

    //버튼 클릭 함수
    public void clicked_button(GameObject _button)
    {
        if (mScene_change) return; //장면 전환 될 때 버튼 안 눌리게 막음

        if (_button.name.Equals("btn_start")) //시작 버튼
        {
            SceneManager.Instance.pushScene("Game_Scene");
        }
        else if (_button.name.Equals("btn_ranking")) //랭킹 버튼
        {
            Google_play.Instance.SendBoardScore();
            Google_play.Instance.SpecialShowLeaderBoard();
        }
    }

    //씬 전환 
    public void onScenc_Ani_End(GameObject gameObject)
    {
        Application.LoadLevel(1); //메인 씬으로 전환
    }

    //이름을 입력하면 해당 값에 대한 정보를 받아온다.
    //public void getMessage()
    //{

    //    UIInput input = input_name_Box.GetComponent<UIInput>();
    //    string name = input.label.text;

    //    Prefs.setPrefsString(Prefs.NICK_NAME, name); //유저 닉네임 저장
    //}
}
