﻿using UnityEngine;
using System.Collections;

public class tutorial_script : MonoBehaviour {

    public GameObject ob_tutorial;
    public GameObject ob_tutorial_grid;

    public GameObject sp_tutorial_1;
    public GameObject sp_tutorial_2;
    public GameObject sp_tutorial_3;
    public GameObject sp_tutorial_4;

    private bool tutorial_click = false;
    private int tutorial_index = 0;

    private float[] tutorial_x = { -725 , -1450 , -2175 , -2900 };

	// Use this for initialization
	void Start () {

        //튜토리얼이 실행되지 않았다면 튜토리얼 이미지를 보여줌.
        if (!Prefs.getPrefsBoolean(Prefs.TUTORIAL))
        {
            StartCoroutine(Btn_on());
            ob_tutorial.SetActive(true);
        }
	}

    public void clicked_button(GameObject button)
    {

        if (!tutorial_click) return;

        if (button.name.Equals("sp_tutorial_1"))
        {
            Tutorial_Ani(0);
        }
        else if (button.name.Equals("sp_tutorial_2"))
        {
            Tutorial_Ani(1);
        }
        else if (button.name.Equals("sp_tutorial_3"))
        {
            Tutorial_Ani(2);
        }
        else if (button.name.Equals("sp_tutorial_4"))
        {
            Tutorial_Ani(3);
        }
    }

    public void Tutorial_Ani(int type)
    {

        tutorial_click = false; //버튼 눌리지 않게 막음.

        tutorial_index = type;

        //페이지 이동 애니메이션 실행
        TweenScript.Instance.setTweenPosition(ob_tutorial_grid, 0, 1, new Vector3(tutorial_x[type] , 0 , 0));

        if (tutorial_index == 3) //튜토리얼이 끝나면 게임을 시작한다.
        {
            Prefs.setPrefsBoolean(Prefs.TUTORIAL, true); //튜토리얼 상태 저장
            Game_script.Instance.GameStart(); //게임 시작
        }

        StartCoroutine(Btn_on());
    }

    //버튼이 연속으로 눌리지 않게 터치한 후 1초 텀을 준다.
    IEnumerator Btn_on()
    {
        yield return new WaitForSeconds(1.5f);

        if (tutorial_index == 3)
        {
            Destroy(ob_tutorial); //튜토리얼 오브젝트 삭제
        }

        tutorial_click = true;
    }
}
