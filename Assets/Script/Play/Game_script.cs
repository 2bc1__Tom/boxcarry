﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Game_script : MonoSingleton<Game_script>
{

    public Camera mCamera;

    public GameObject LauncherPrefab;
    public GameObject rauncherPrefab; //캐릭터와 상자를 관리하는 런처 프리팹

    public GameObject[] characterPrefab; //캐릭터 프리팹
    public GameObject[] boxPrefab; //박스 프리팹

    public GameObject[] characterPrefab_dog; //강아지 캐릭터 프리팹
    public GameObject[] boxPrefab_dog; //강아지 박스 프리팹

    public GameObject[] characterPrefab_cat; //고양이 캐릭터 프리팹
    public GameObject[] boxPrefab_cat; //고양이 박스 프리팹

    public GameObject[] characterPrefab_Dragon; //용 캐릭터 프리팹
    public GameObject[] boxPrefab_Dragon; //용 박스 프리팹

    public GameObject[] characterPrefab_Food; //패스트푸드 캐릭터 프리팹
    public GameObject[] boxPrefab_Food; //패스트푸드 박스 프리팹

    public GameObject GoldPrefab; //골드 프리팹
    public GameObject superPrefab; //옥상에서 던지는 슈퍼 캐릭터를 관리하는 런처 프리팹

    public GameObject ob_Popup; //팝업이 담긴 오브젝트
    public GameObject ob_setting_popup; //설정 팝업
    public GameObject ob_review_popup; //리뷰 팝업
    public GameObject ob_iden_popup; //구매 확인 팝업
    public GameObject ob_shop_popup; //Shop 팝업

    public GameObject ob_reivew_popup; //리뷰 팝업
    public GameObject ob_video_popup; //비디오 광고 팝업

    public GameObject LevelUp_popup; //레벨 업 시 설명 팝업

    public GameObject ob_Game_Bg; //게임 배경
    public GameObject script_fever_time; //피버타임 스크립트가 걸려있는 오브젝트

    public GameObject ob_Gold; //골드를 담는 오브젝트

    public GameObject ob_item; //아이템 정보가 담겨 있는 프리팹 오브젝트
    public GameObject grid_item; //그리드뷰 오브젝트

    public GameObject grid_badge; //환생 뱃지를 담고 있는 그리드 오브젝트

    public GameObject ob_Reincarnation; //환생 버튼 오브젝트
    public GameObject btn_Reincarnation; //환생 버튼

    public GameObject ob_character_box; //캐릭터와 박스가 담긴 오브젝트
    public GameObject ob_fever_time; //피버타임 오브젝트

    public GameObject btn_gold_buff; //골드 버프 아이콘 버튼
    public GameObject btn_dance_buff; //피버 타임 버프 아이콘 버튼

    public GameObject sp_gold_buff_notice_box; //골드 버프 설명 박스
    public GameObject sp_sale_buff_notice_box; //할인 버프 설명 박스

    public GameObject ob_Event_icon; //이벤트 상자에서 나오는 골드or보석 아이콘을 담는 오브젝트

    public UILabel label_gold_buff_notice; //골드 버프 설명
    public UILabel label_sale_buff_notice; //할인 버프 설명

    public UILabel label_reincarnation_ment; //환생 멘트
    public UILabel label_Review_ment; //리뷰 멘트

    public GameObject ob_energy_sun; //태양 오브젝트
    public GameObject btn_energy_sun; //태양 버튼

    private GameObject sp_event = null;

    public UILabel label_gold; //골드 표시 텍스트
    public UILabel label_jewel; //보석 표시 텍스트

    public UIGrid grid_super; //옥상에서 던지는 슈퍼 캐릭터를 담는 그리드

    private GameObject op_drag_box; //드래그 되고 있는 박스 오브젝트
    private GameObject ob_Launcher; //캐릭터와 박스를 관리하는 런처 오브젝트

    private List<GameObject> arSunGold; //폭파되는 골드를 담는 오브젝트
    private List<GameObject> arItem_List; //아이템 리스트를 담는 오브젝트

    private List<GameObject> arGold_List; //골드 아이콘을 담는 리스트
    private List<GameObject> arLanuncher_List; //런처 오브젝트를 담는 리스트

    public ParticleSystem mParticle_sun; //태양폭파 파티클

    private Vector3 MousePos; //드래그에 대한 위치 값

    private IEnumerator IeNumer; //레벌 업 설명 팝업 3초 뒤에 없앨 때 사용

    private bool mScene_change = false; //씬 체인지

    private bool mDragBox = false; //박스 드래그 여부
    public bool mFeverTime = false; //피버타임인지 아닌지 구분
    private bool mBuff_Box = false; //버프 설명 박스가 나와 있는지에 대한 여부

    private bool mGame_play = true; //게임이 진행 중인지 아닌지 체크
    private bool mSun_Click = false; //태양이 100프로일 떄만 클릭할 수 있음.

    public bool mPopup_ON = false; //팝업이 띄어져 있는지 아닌지 판단.
    public bool mJewel_pop = false; //보석 구매 팝업 창이 띄어져 있는지 판단.

    public bool mEvent_Box = false; //이벤트 상자가 생성 되어 있는지 판단.

    public bool mFever_dance = false; //피버타임 댄스 일 때

    private int mVideo_check = 60; //60초에 한번 씩 비디오 광고 노출 확률 체크
    private int mFever_check = 30; // 30초에 한번 씩 피버 확률 체크
    private int mFever_end_check = 18; //피버타임 시간 18초 동안 유지

    private int mBuff_index = 0; //어떤 버프 아이콘을 눌렀는지

    private int mLanuncher_count = 0; //리스트에 담긴 캐릭터,박스 생성 런처 불러 올 떄 사용하는 인덱스 값
    private int mGold_count = 0; //리스트에 담긴 골드 오브젝트 불러올 때 사용하는 인덱스 값

    private int mSpecies_Type = 0; //어떤 종족인지 타입

    private float mSun_Energy = 0; //태양 에너지

    //골드와 보석 텍스트로 표시
    public void Gold_text()
    {
        long gold = Prefs.GetGold(); //현재 보유 골드
        int jewel = int.Parse(Prefs.getPrefsJewel()); //현재 보유 보석

        label_gold.text = string.Format("{0:n0}", gold); //돈 단위 콤마로 표시
        label_jewel.text = "" + jewel;
    }

    //현재 시간을 체크해서 시간에 따라 배경 이미지를 바꿔 줌.
    public void BG_change()
    {
        DateTime now_ = DateTime.Now;

        if (now_.Hour >= 6 && now_.Hour < 16) //오전 6시부터 오후 4시까지
        {
            ob_Game_Bg.transform.GetComponent<UISprite>().spriteName = "bg_p1_1"; //낮 배경
        }
        else if (now_.Hour >= 16 && now_.Hour < 19) //오후 4시부터 7시까지
        {
            ob_Game_Bg.transform.GetComponent<UISprite>().spriteName = "bg_p1_2"; //노을 배경
        }
        else //오후 7시부터 오전 6시까지
        {
            ob_Game_Bg.transform.GetComponent<UISprite>().spriteName = "bg_p1_3"; //밤 배경
        }
    }

    //5분에 한번 씩 시간 체크
    IEnumerator Time_Check()
    {
        while(true){
            BG_change(); //시간에 따라 배경 이미지를 바꿔 줌.
            yield return new WaitForSeconds(500f); //5분에 한번 씩 시간을 체크한다.
        }
    }

    //비지엠 Start
    public void BGM_start()
    {
        if (mFeverTime) //피버 중 일 때
        {
            Main_script.Instance.SoundPlay(0, "DanceTime"); //비지엠 실행
        }
        else
        {
            Main_script.Instance.SoundPlay(0, "mainBGM"); //비지엠 실행
        }
    }

    //비지엠 Stop
    public void BGM_stop()
    {
        AudioController.StopMusic();
    }

    // Use this for initialization
	void Start () {

        //Prefs.setPrefsInt(Prefs.CHARACTER_COLOR , 6);

        //PlayerPrefs.DeleteKey(Prefs.CHARACTER_COLOR);
        //PlayerPrefs.DeleteKey(Prefs.UP_SALE_BUFF);

        //DataReset();

        //Prefs.Gold_Save(13000000000);
        //Prefs.setPrefsJewel(3000);

        //Prefs.setPrefsInt(Prefs.SPECIES_TYPE, 0);

        //=============================================================

        //Become21Plugin.upperBannerAd();
        SceneManager.Instance.onGameExit = gameExit;
      
        arGold_List = new List<GameObject>(); //골드 아이콘을 담는 오브젝트
        arLanuncher_List = new List<GameObject>(); //런처 오브젝트를 담는 리스트

        GameStart(); //게임 시작
    }

    public void GameStart()
    {

        //튜토리얼을 본 상태라면 게임을 시작하게 함.
        if (Prefs.getPrefsBoolean(Prefs.TUTORIAL))
        {
            itemListSetting(); //아이템 리스트 셋팅

            CharacterBox_Make(); //캐릭터와 상자 생성
            Super_Character(true); //옥상에서 상자 던지는 슈퍼 캐릭터 생성

            Gold_text(); //골드와 수정 텍스트로 표시

            StartCoroutine(Play_Time()); //플레이 시간 체크
            StartCoroutine(Sun_Time()); //태양 에너지 체크

            StartCoroutine(Time_Check()); //현재 시간 체크

            badge_color(); //환생 단계 체크해서 뱃지 색상 변경
            Buff_Check(); //버프가 있는지 체크해서 있으면 버프 아이콘을 표시
        }
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetMouseButtonDown(1))
        {
            CharacterBox_Make(); //캐릭터와 상자 생성
        }

        //화면을 터치 했을 때
        if (Input.GetMouseButtonDown(0))
        {

            op_drag_box = GetClicked_Object(); //터치한 영역에 오브젝트가 있다면 받아온다.

            if (op_drag_box != null)
            {
                  if (op_drag_box.name.Contains("box_prefab")) //가져온 오브젝트가 박스라면
                  {
                      mDragBox = true; //드래그 될 수 있게 true로 바꿈
                      
                      GameObject ob_rauncher = op_drag_box.transform.parent.gameObject; //상자가 속에 있는 런처 오브젝트를 가져온다.

                      if (op_drag_box.name.Contains("box_prefab_h")) //이벤트 상자일 떄
                      {
                          ob_rauncher.GetComponent<Launcher_script>().EventCharacter_collider(); //캐릭터에 BoxCollider를 걸어줌.
                      }
                      else
                      {
                          ob_rauncher.GetComponent<Launcher_script>().BoxCarryEnd(); //상자가 움직이던 상태를 멈춤
                      }
                  }
            }
        }
        else if (Input.GetMouseButtonUp(0)) //터치가 끝났을 때
        {
            if (op_drag_box != null)
            {
                if (op_drag_box.name.Contains("box_prefab")) //가져온 오브젝트가 박스라면
                {

                    op_drag_box.AddComponent<Rigidbody2D>(); //물리엔진 추가

                    op_drag_box.GetComponent<Rigidbody2D>().velocity = new Vector3(0.02f, 0f, 0f); //떨어 질 떄 살짝 앞으로 떨어지게 하기 위함
                    op_drag_box.GetComponent<Rigidbody2D>().gravityScale = 0.05f; //중력 값을 준다.
                }
            }

            mDragBox = false; //드래그 멈춤
        }

        //박스 드래그
        if (mDragBox)
        {
            //터치한 영역의 좌표값을 가져온다.
            MousePos = mCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, -mCamera.transform.position.z));

            if (op_drag_box != null)
            {

                //박스가 깨져 있을 때 드래그를 멈춤
                if (op_drag_box.name.Contains("delete_box"))
                {
                    mDragBox = false;
                }

                if (MousePos.y > -0.3f) //드래그 y 좌표 값이 바닥 보다 위 일 때만 드래그 가능하도록
                {
                    op_drag_box.transform.position = new Vector3(MousePos.x, MousePos.y, op_drag_box.transform.position.z);
                
                }else //바닥 밑으로 내려가면 상자 위치를 바닥과 닫는 부분에 고정으로 맞춘다.
                {
                    op_drag_box.transform.position = new Vector3(MousePos.x, -0.21f, op_drag_box.transform.position.z);
                }
            }
        }
	}

    //캐릭터와 상자 생성
    public void CharacterBox_Make()
    {
        if (mFeverTime) return;

        int peopleLevel = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + 0) + 1; //캐릭터 최대 인원
        int peopelIndex = ob_character_box.transform.childCount-1; //오브젝트 갯수가 몇개인지 가져온다;

        if (peopelIndex < peopleLevel) //현재 생성되어 있는 캐릭터가 최대 생성 인원 보다 적으면
        {
            int PosesIdex = 2; //포즈 랜덤 범위

            int characterIndex = UnityEngine.Random.Range(0, 3);//어떤 캐릭터인지에 대한 인덱스 값

            //캐릭터가 여자면 포즈 범위가 하나 더 늘어남(수레끄는 거)
            if (characterIndex == 2)
            {
                PosesIdex = 3;
            }

            int characterPosesIndex = UnityEngine.Random.Range(0, PosesIdex); //캐릭터 포즈에 대한 인덱스 값
            int boxIndex = UnityEngine.Random.Range(Strings.BOX_RANDOM[characterIndex, characterPosesIndex, 0], Strings.BOX_RANDOM[characterIndex, characterPosesIndex, 1]); //어떤 상자인지에 대한 인덱스 값

            //여자 캐릭터가 수레를 끄는 모습 일 떄
            if (characterIndex == 2 && characterPosesIndex == 2)
            {
                boxIndex += 2;
            }

            bool event_box = false;

            ob_Launcher = Instantiate(rauncherPrefab) as GameObject;

            //이벤트 상자 캐릭터가 없을 떄
            if (mEvent_Box == false)
            {
                //이벤트 스쿠터 캐릭터 등장 확률 계산
                int event_index = UnityEngine.Random.Range(0, 1000);

                if (event_index < 2) //이벤트 캐릭터 등장
                {
                    mEvent_Box = true; //이벤트 상자가 진행 중.
                    event_box = true;
                
                    PosesIdex = 0;
                    characterIndex = 3;
                    boxIndex = 7;

                    ob_Launcher.name = "event_Launcher";
                }
            }

            ob_Launcher.transform.parent = ob_character_box.transform;
            ob_Launcher.transform.localScale = Vector3.one;
            ob_Launcher.transform.localPosition = new Vector3(-80f, 0f, 0f);

            Launcher_script Launcher_script = ob_Launcher.GetComponent<Launcher_script>();

            Launcher_script.mCamera = mCamera; //런처 스크립트에 카메라 오브젝트 값을 보낸다.
            Launcher_script.ob_Gold = ob_Gold; //골드 오브젝트 값

            if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 0) //인간 종족
            {
                Launcher_script.character_prefab = characterPrefab[characterIndex]; //어떤 캐릭터인지에 대한 프리팹
                Launcher_script.box_prefab = boxPrefab[boxIndex]; //어떤 상자인지에 대한 프리팹
            }
            else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 1) //강아지 종족
            {
                Launcher_script.character_prefab = characterPrefab_dog[characterIndex]; //어떤 캐릭터인지에 대한 프리팹
                Launcher_script.box_prefab = boxPrefab_dog[boxIndex]; //어떤 상자인지에 대한 프리팹
            }
            else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 2) //고양이 종족
            {
                Launcher_script.character_prefab = characterPrefab_cat[characterIndex]; //어떤 캐릭터인지에 대한 프리팹
                Launcher_script.box_prefab = boxPrefab_cat[boxIndex]; //어떤 상자인지에 대한 프리팹
            }
            else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 3) //드래곤 종족
            {
                Launcher_script.character_prefab = characterPrefab_Dragon[characterIndex]; //어떤 캐릭터인지에 대한 프리팹
                Launcher_script.box_prefab = boxPrefab_Dragon[boxIndex]; //어떤 상자인지에 대한 프리팹
            }
            else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 4) //패스트푸드 종족
            {
                Launcher_script.character_prefab = characterPrefab_Food[characterIndex]; //어떤 캐릭터인지에 대한 프리팹
                Launcher_script.box_prefab = boxPrefab_Food[boxIndex]; //어떤 상자인지에 대한 프리팹
            }

            Launcher_script.mCharacterIndex = characterIndex;  //어떤 캐릭터가 나올지에 대한 인덱스 값
            Launcher_script.mCharacterPosesIndex = characterPosesIndex; //어떤 프즈인지에 대한 인덱스 값(앞으로 나르기 ,뒤로 나르기 , 수레 끌기)
            Launcher_script.mBoxIndex = boxIndex; //어떤 상자인지에 대한 인덱스 값

            Launcher_script.mEvent_Box = event_box; //이벤트 캐릭터 등장 여부

            Launcher_script.Luncher_Start(); //런처 오브젝트 시작.
        }
    }

    //슈퍼 캐릭터 생성 방식 구분
    public void Super_Character(bool _start)
    {

        int superIndex = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + 7); //옥상에서 던지는 캐릭터가 몇명인지
        
        //캐릭터 간 간격 조정
        float[] arCellWidtg = {
            55,51.4f,40f,33f,27f,22f,19.5f,16.8f,15f
        };

        if (_start) //게임 처음 시작 할 때 슈퍼파워 캐릭터가 몇명인지 판단해서 그 수 만큼 생성한다.
        {
            for (int i = 0; i < superIndex; i++)
            {
                Super_Character_Make(arCellWidtg[i]);
            }

        } else //슈퍼파워 아이템을 구매 했을 떄 한명 씩 캐릭터를 추가 시키기 위함
        {
            Super_Character_Make(arCellWidtg[superIndex-1]);
        }
    }

    //옥상에서 상자 던지는 슈퍼 캐릭터 생성
    public void Super_Character_Make(float CellWidtg)
    {
        grid_super.cellWidth = CellWidtg; //캐릭터 간 간격 조정
        grid_super.repositionNow = true; //그리드를 재 정렬 시킴.

        GameObject super_Launcher = Instantiate(superPrefab) as GameObject;
        super_Launcher.transform.parent = grid_super.transform;
        super_Launcher.transform.localScale = Vector3.one;
        super_Launcher.transform.localPosition = Vector3.zero; //position 값을 0으로 맞춤

        super_script superScript = super_Launcher.transform.GetComponent<super_script>();

        if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 0) //인간 종족
        {
            superScript.characterPrefab = characterPrefab; //캐릭터 프리팹 배열
            superScript.boxPrefab = boxPrefab; //상자 프리팹 배열

        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 1) //강아지 종족
        {
            superScript.characterPrefab = characterPrefab_dog; //캐릭터 프리팹 배열
            superScript.boxPrefab = boxPrefab_dog; //상자 프리팹 배열
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 2) //고양이 종족
        {
            superScript.characterPrefab = characterPrefab_cat; //캐릭터 프리팹 배열
            superScript.boxPrefab = boxPrefab_cat; //상자 프리팹 배열
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 3) //드래곤 종족
        {
            superScript.characterPrefab = characterPrefab_Dragon; //캐릭터 프리팹 배열
            superScript.boxPrefab = boxPrefab_Dragon; //상자 프리팹 배열
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 4) //패스트푸드 종족
        {
            superScript.characterPrefab = characterPrefab_Food; //캐릭터 프리팹 배열
            superScript.boxPrefab = boxPrefab_Food; //상자 프리팹 배열
        }
    }

    //아이템 리스트뷰 셋팅
    public void itemListSetting()
    {
        arItem_List = new List<GameObject>();

        for (int i = 0; i < Strings.ITEM_NAME[0].Length; i++)
        {
            GameObject item_ = Instantiate(ob_item) as GameObject;

            item_.name = "" + i; //어떤 아이템을 클릭했는지 구분하기 위해 이름을 숫자로 변경
            item_.transform.parent = grid_item.transform; // item_ 오브젝트가 grid_item 오브젝트 자식 view로 들어감.
            item_.transform.localScale = Vector3.one; //scale 값을 1로 맞춤
            item_.transform.localPosition = Vector3.zero; //position 값을 0으로 맞춤

            item_.transform.GetComponent<item_script>().itemDataSetting(i);
            arItem_List.Add(item_);
        }

        grid_item.GetComponent<UIGrid>().repositionNow = true;

    }

    //화면을 터치 했을 때 터치한 영역에 상자가 있는지 없는지 판단
    private GameObject GetClicked_Object()
    {
        GameObject target = null;

        Vector2 touchPosition = mCamera.ScreenToWorldPoint(Input.mousePosition);
        Ray2D ray = new Ray2D(touchPosition, Vector2.zero);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);

        if (hit.collider != null)
        {
            target = hit.collider.gameObject;
        }

        return target;
    }

    //박스가 바닥과 충돌 했을 떄
    public void BoxFloor(GameObject ob_Box)
    {
        GameObject ob_rauncher = ob_Box.transform.parent.gameObject; //상자가 속해 있는 부모 오브젝트를 가져온다.

        if (ob_rauncher != null)
        {
            StartCoroutine(Launcher_delete_delay(ob_rauncher)); //런처 오브젝트가 비어 있으면 삭제 시켜준다.
        }
    }

    //상자가 오른쪽 건물과 충돌하면 돈이 올라감
    public void Gold_Plus(int type)
    {
        int BoxLevel = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + 4); //상자 아이템의 레벨을 가져온다.
        int Gold_Buff = Prefs.getPrefsInt(Prefs.GOLD_BUFF); //골드 버프 값을 가져온다.

        long gold = (long)Strings.ITME_LEVEL_UP[4][BoxLevel] * Strings.GOLD_BUFF[Gold_Buff]; // + 골드

        GameObject sp_gold = null;

        if (arGold_List.Count < 25)
        {
            //골드 생성
            sp_gold = Instantiate(GoldPrefab) as GameObject;
            sp_gold.transform.parent = ob_Gold.transform;
            sp_gold.transform.localScale = Vector3.one;

            arGold_List.Add(sp_gold);
        }
        else
        {
            if (mGold_count > 23) mGold_count = 0;

            sp_gold = arGold_List[mGold_count];
            mGold_count++;

            TweenScript.Instance.setTweenAlpha(sp_gold, 0, 0, 0, 1);
        }

        sp_gold.transform.Find("Label_gold").GetComponent<UILabel>().text = "+" + gold; // +되는 골드 표시

        if (type == 0) //밑에 상자가 오른쪽 건물과 충돌 했을 때
        {
            sp_gold.transform.localPosition = new Vector3(307f, -263.4f, 0f); //골드 오브젝트 위치 지정
            TweenScript.Instance.setTweenPosition(sp_gold, 0, 1f, new Vector3(307f, -210f, 0f));

            Prefs.Gold_Save(gold); //골드  + 시켜줌

        }else if(type == 1) //옥상에서 던진 상자가 오른쪽 건물과 충돌 했을 때
        {
            sp_gold.transform.localPosition = new Vector3(307f, -76f, 0f); //골드 오브젝트 위치 지정
            TweenScript.Instance.setTweenPosition(sp_gold, 0, 1f, new Vector3(307f, -20f, 0f));

            sp_gold.transform.Find("Label_gold").GetComponent<UILabel>().text = "+" + (gold * 10); // +되는 골드 표시
            
            Prefs.Gold_Save((gold * 10)); //골드 + 10배 시켜줌

        }else //나르던 상자를 던진 경우나 드래그로 충돌 했을 경우
        {
            sp_gold.transform.localPosition = new Vector3(217f, -156f, 0f); //골드 오브젝트 위치 지정
            TweenScript.Instance.setTweenPosition(sp_gold, 0, 1f, new Vector3(217f, -116f, 0f));

            Prefs.Gold_Save(gold); //골드  + 시켜줌
        }

        Main_script.Instance.SoundPlay(1, "coin"); //골드+ 효과음

        TweenScript.Instance.setTweenAlpha(sp_gold, 0.4f, 0.5f, 1, 0);
        Gold_text(); //골드와 보석 텍스트로 표시
    }

    // Launcher 가 삭제될 때 호출되는 함수.(Launcher 오브젝트에서 캐릭터와 상자가 삭제 될 떄)
    public void New_Launcher()
    {
        StartCoroutine(New_Launcher_Delay()); //캐릭터와 상자 런처 생성
    }

    //캐릭터와 상자 런처 생성 전에 잠시 딜레이를 줌.(오브젝트 개수 초기화를 위함)
    IEnumerator New_Launcher_Delay()
    {
        yield return new WaitForSeconds(0.2f);
        CharacterBox_Make();
    }

    //상자가 바닥에 충돌 후 바로 사라지지 않고 0.5초 뒤에 사라지기 때문에 잠시 텀을 준다.
    IEnumerator Launcher_delete_delay(GameObject ob_Launcher)
    {
        yield return new WaitForSeconds(0.6f); //상자가 바닥에 충돌 후 바로 사라지지 않고 0.5초 뒤에 사라지기 때문에 잠시 텀을 준다.
        
        if (ob_Launcher != null)
        {
            if (ob_Launcher.transform.childCount == 0) //오브젝트 안에 아무것도 없으면 삭제 시킴
            {
                Destroy(ob_Launcher); //런처 삭제
                New_Launcher(); //캐릭터와 상자 런처 생성
            }
        }
    }

    //피버 확률 계산
    public void Fever_Probability()
    {
        int Light_Buillding_Level = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + 5); //왼쪽 건물 레벨

        int fever_random = (int)Strings.ITME_LEVEL_UP[5][Light_Buillding_Level]; //피버 랜덤 확률
        int randomFever = UnityEngine.Random.Range(0, 10000); //랜덤 값을 구함

        //피버 확률에 걸리면 피버 시작
        if (randomFever <= fever_random)
        {
            FeverTime_Start();
        }
    }

    //피버타임 시작
    public void FeverTime_Start()
    {

        if (mPopup_ON) return; //중간,리뷰,비디오 광고 팝업이 띄어져 있을 떈 피버타임 시작되지 않게 막아둠.
        if (mEvent_Box) return; //이벤트 상자 캐릭터가 진행 중 일 땐 피버 타임 안되게 막음.

        Main_script.Instance.SoundPlay(0, "DanceTime"); //피버 음악 시작

        //환생 뱃지 사라지게 함.
        for (int i = 1; i < 8; i++)
        {
            GameObject ob_badge = grid_badge.transform.GetChild(i - 1).gameObject; //뱃지 오브젝트를 받아옴.
            TweenScript.Instance.setTweenAlpha(ob_badge, 0, 1f, 1, 0);
        }

        LevelUp_popup.SetActive(false); //레벨 업 설명 팝업 없앰.

        mFeverTime = true;
        mFever_dance = true;

        if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 0) //인간 종족 일 때
        {
            Fever_script.Instance.FeverTime_Start(); //피버타임 시작
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 1) //강아지 종족 일 때
        {
            Fever_script_dog.Instance.FeverTime_Start(); //피버타임 시작
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 2) //고양이 종족 일 때
        {
            Fever_script_cat.Instance.FeverTime_Start(); //피버타임 시작
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 3) //드래곤 종족 일 때
        {
            Fever_script_dragon.Instance.FeverTime_Start(); //피버타임 시작
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 4) //패스트푸드 종족 일 때
        {
            Fever_script_food.Instance.FeverTime_Start(); //피버타임 시작
        }

        ob_fever_time.SetActive(true); //피버타임 오브젝트 ON

        //아이템 버튼 안눌리게 설정
        for (int i = 0; i < grid_item.transform.childCount; i++)
        {
            GameObject game_item = grid_item.transform.GetChild(i).gameObject;
            game_item.GetComponent<item_script>().mFeverTime = true; //버튼 안 눌리게 설정
        }

        //나르고 있던 상자를 전부 집어 던지고 퇴장 한다.
        for (int i = 1; i < ob_character_box.transform.childCount; i++)
        {
            GameObject game_Launcher = ob_character_box.transform.GetChild(i).gameObject;

            game_Launcher.GetComponent<Launcher_script>().mFeverTime = true; //피버타임 상태 ON
            game_Launcher.GetComponent<Launcher_script>().Box_Throw();
            game_Launcher.GetComponent<Launcher_script>().Character_Back();
        }

        //슈퍼파워 캐릭터 숨김
        for (int i = 0; i < grid_super.transform.childCount; i++)
        {
            GameObject super_Launcher = grid_super.transform.GetChild(i).gameObject;
            super_Launcher.GetComponent<super_script>().FeverTime_Dapth(true); //캐릭터가 건물보다 뒤 쪽으로 가게 댑스 설정
        }

            TweenScript.Instance.setTweenPosition(grid_super.gameObject, 0, 1f, new Vector3(-331f, -100f, 0));
    }

    //피버타임 종료
    public void FeverTime_End()
    {

        mFeverTime = false; //피버타임 OFF
        mFever_dance = false;

        mFever_end_check = 17; //피버타임 시간 초기화

        if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 0) //인간 종족 일 때
        {
             Fever_script.Instance.FeverTime_End(); //피버타임 종료
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 1) //강아지 종족 일 때
        {
            Fever_script_dog.Instance.FeverTime_End(); //피버타임 종료
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 2) //고양이 종족 일 때
        {
            Fever_script_cat.Instance.FeverTime_End(); //피버타임 종료
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 3) //드래곤 종족 일 때
        {
            Fever_script_dragon.Instance.FeverTime_End(); //피버타임 종료
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 4) //패스트푸드 종족 일 때
        {
            Fever_script_food.Instance.FeverTime_End(); //피버타임 종료
        }

        CharacterBox_Make(); //캐릭터와 상자 생성

        //수퍼파워 캐릭터 등장 애니메이션 실행.
        TweenPosition tweenPosition = TweenScript.Instance.setTweenPosition(grid_super.gameObject, 0, 1f, new Vector3(-331f, 53f, 0));
        StartCoroutine(AnimationEnd());

        //환생 뱃지 다시 등장
        for (int i = 1; i < 9; i++)
        {
            GameObject ob_badge = grid_badge.transform.GetChild(i - 1).gameObject; //뱃지 오브젝트를 받아옴.
            TweenScript.Instance.setTweenAlpha(ob_badge, 0, 1f, 0, 1); 
        }

        Main_script.Instance.SoundPlay(0, "mainBGM"); //메인 비지엠

    }

    //피버 이후 슈퍼캐릭터 등장 애미메이션이 끝난 후 호출
    IEnumerator AnimationEnd()
    {
        yield return new WaitForSeconds(1.1f);

        //슈퍼파워 캐릭터 상자 던지기 기능 재 작동
        for (int i = 0; i < grid_super.transform.childCount; i++)
        {
            GameObject super_Launcher = grid_super.transform.GetChild(i).gameObject;
            super_Launcher.GetComponent<super_script>().FeverTime_Dapth(false); //캐릭터가 건물보다 뒤 쪽으로 가게 댑스 설정
        }

        //아이템 버튼 다시 눌리게 설정
        for (int i = 0; i < grid_item.transform.childCount; i++)
        {
            GameObject game_item = grid_item.transform.GetChild(i).gameObject;
            game_item.GetComponent<item_script>().mFeverTime = false; //아이템 버튼 다시 눌리게 설정
        }
    }

    //게임 플레임 시간을 체크한다.
    IEnumerator Play_Time()
    {
        while (mGame_play)
        {
            //------ 피버타임 체크 -----

            if (mFever_dance) //피버 타임 일 때
            {
                mFever_end_check--;

                //10초 뒤에 피버 타임 종료
                if (mFever_end_check == 0) FeverTime_End(); 
            
            } else //아닐 때
            {
                mFever_check--;

                if (mFever_check < 0) mFever_check = 30; //시간 초기화

                //1분에 한번 씩 피버 확률 계산
                if (mFever_check == 0) Fever_Probability();

                //====== 비디오 광고 시간 체크 ========

                mVideo_check--;

                if (mVideo_check < 0) mVideo_check = 60; //시간 초기화

                if (mVideo_check == 0)
                {
                    //이벤트 상자 캐릭터가 진행 중 일 땐 비디오 광고 띄우지 않음.
                    if (!mEvent_Box)
                    {
                        Video_Popup(true); //비디오 광고 노출
                    }
                } 
            }

            yield return new WaitForSeconds(1f);
        }
    }

    //태양 에너지 충전 시간 체크
    IEnumerator Sun_Time()
    {
        mSun_Energy = Prefs.getPrefsInt(Prefs.SUN_ENERGY); //저장된 태양 게이지를 불러옴

        while (mGame_play)
        {
            if (mSun_Energy <= 100)
            {
                if (mSun_Energy == 100) //태양 에너지가 100프로 이면 이미지를 바꿔주고 태양을 클릭할 수 있게 해줌
                {
                    mSun_Click = true;

                    btn_energy_sun.transform.GetComponent<UISprite>().spriteName = "sun_p1_3";
                    btn_energy_sun.transform.GetComponent<UIButton>().normalSprite = "sun_p1_3";
                }

                float sunEnergy = (mSun_Energy / 100);

                ob_energy_sun.transform.GetComponent<UIProgressBar>().value = sunEnergy; //에너지 표시
                Prefs.setPrefsInt(Prefs.SUN_ENERGY, (int)mSun_Energy);

                mSun_Energy += 1;
            }
            
            int Right_Buillding_Level = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + 6); //오른쪽 건물 레벨
            float sunEnergyTime = Strings.ITME_LEVEL_UP[6][Right_Buillding_Level]; //태양 에너지 충전 시간을 받아 온다.

            yield return new WaitForSeconds(sunEnergyTime / 100f);
        }
    }

    //레벨 업 시 설명 팝업(item_script에서 호출)
    public void ItemLevle_Up_Popup(int itemIndex)
    {
        if (itemIndex == 7) {
            LevelUp_popup.SetActive(false); //레벨 업 설명 팝업 없앰
            return; //슈퍼파월 캐릭터 구매는 설명 팝업을 띄우지 않음
        }

        int itemLevel = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + itemIndex); //아이템 레벨

        UILabel levelupEx = LevelUp_popup.transform.Find("Label_ex").GetComponent<UILabel>(); //능력치 설명 텍스트

        string levelEx = Strings.ITEM_LEVEL_UP_EX[Strings.LANGUAGE_TYPE][itemIndex].Replace("@", "" + (itemLevel + 1)); //현재 레벨 텍스트로 표시
        levelupEx.text = levelEx.Replace("#", Strings.LEVEL_UP_EX[itemIndex][itemLevel]); //레벨 업 했을 시 올라간 능력치를 텍스트로 보여줌.

        TweenScript.Instance.setTweenAlpha(LevelUp_popup, 0f, 0f, 1, 0);

        TweenScript.Instance.setTweenScaleUp(LevelUp_popup , 0f , 0.5f);
        TweenScript.Instance.setTweenAlpha(LevelUp_popup, 0f, 0.5f, 0, 1);

        LevelUp_popup.SetActive(true);

        //실행 되고 있는 게 있다면 멈춰 줌.
        if (IeNumer != null) StopCoroutine(IeNumer);

        IeNumer = ItemLevle_Up_Popup_Delete();
        StartCoroutine(IeNumer); //3초 뒤에 레벨 업 기능 설명 파업 자동으로 없앰.
    }

    //3초 뒤에 레벨 업 기능 설명 파업 자동으로 없앰.
    IEnumerator ItemLevle_Up_Popup_Delete()
    {
        yield return new WaitForSeconds(3f);
        TweenScript.Instance.setTweenAlpha(LevelUp_popup, 0f, 0.5f, 1, 0);
    }

    //환생 단계에 따라 환생 뱃지 색상 변경
    public void badge_color()
    {

        int badgeIndex = Prefs.getPrefsInt(Prefs.CHARACTER_COLOR) + 1; //현재 환생 단계

        for (int i = 1; i < badgeIndex; i++)
        {
             //컬러 색상 값
            float Color_R = Strings.CHARACTER_COLOR[i, 0] / 256f;
            float Color_G = Strings.CHARACTER_COLOR[i, 1] / 256f;
            float Color_B = Strings.CHARACTER_COLOR[i, 2] / 256f;

            GameObject ob_badge = grid_badge.transform.GetChild(i-1).gameObject; //뱃지 오브젝트를 받아옴.
            ob_badge.transform.GetComponent<UISprite>().color = new Color(Color_R, Color_G, Color_B); //뱃지 색상 변경
        }
    }

    //발업,힘업 시 스피드 업 시켜줌
    public void Character_Speed_Up(int type)
    {
        //캐릭터 스피드업
        for (int i = 1; i < ob_character_box.transform.childCount; i++)
        {
            GameObject game_Launcher = ob_character_box.transform.GetChild(i).gameObject;

            if (!game_Launcher.name.Contains("event_Launcher"))
            {
                if (type == 1) //발업 시 뒤로 되돌아 갈 때 스피드 향상
                {
                    game_Launcher.GetComponent<Launcher_script>().Character_Back();
                }
                else //힘업 시 상자를 나르는 스피드 향상
                {
                    game_Launcher.GetComponent<Launcher_script>().Character_Carry();
                }
            }
        }
    }

    //구매 확인 팝업 띄움(다른 스크립트에서 구매 했을 때 호출함)
    public void Buy_Popup(string _ment)
    {
        ob_Popup.SetActive(true);
        ob_iden_popup.SetActive(true);

        GameObject label_ex = ob_iden_popup.transform.Find("Label_ex").gameObject;
        label_ex.transform.GetComponent<UILabel>().text = _ment; //구매 확인 멘트

        TweenScript.Instance.setTweenScaleUp(ob_iden_popup, 0, 0.4f); //구매 확인 팝업 애니메이션
    }

    //버프가 있는지 체크해서 있으면 버프 아이콘을 표시
    public void Buff_Check()
    {
        int gold_buff_index = Prefs.getPrefsInt(Prefs.GOLD_BUFF); //골드 버프 몇 단계인지 값을 가져옴
        int sale_buff_index = Prefs.getPrefsInt(Prefs.UP_SALE_BUFF); //세일 버프 몇 단계인지 값을 가져옴

        //버프가 두개 다 걸려있는 경우
        if (gold_buff_index > 0 && sale_buff_index > 0)
        {
            btn_gold_buff.transform.localPosition = new Vector3(-325f, 224f, 0);
            btn_dance_buff.transform.localPosition = new Vector3(-265f, 224f, 0);

            btn_gold_buff.SetActive(true);
            btn_dance_buff.SetActive(true);
        }
        else if (gold_buff_index > 0) //골드 버프만 걸려 있을 때
        {
            btn_gold_buff.transform.localPosition = new Vector3(-325f, 224f, 0);
            btn_gold_buff.SetActive(true);
        }
        else if (sale_buff_index > 0) //할인 버프만 걸려있을 떄
        {
            btn_dance_buff.transform.localPosition = new Vector3(-325f, 224f, 0);
            btn_dance_buff.SetActive(true);
        }
    }

    //리뷰 팝업 여부
    public void Review_popup(bool type)
    {

        if (type) //리뷰 팝업 띄움
        {

            int peopleIndex = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + 0);

            if (!Prefs.getPrefsBoolean(Prefs.REVIEW)) //리뷰를 달지 않았다면
            {

                if (peopleIndex == 3 || peopleIndex == 5) //사람 레벨 4,6 일 때 리뷰 팝업 띄움
                  {
                      if (!Prefs.getPrefsBoolean(Prefs.REVIEW_TIMING)) //리뷰 팝업이 아직 뜨지 않은 상태면
                      {

                          mPopup_ON = true;

                          ob_Popup.SetActive(true);
                          ob_reivew_popup.SetActive(true);

                          label_Review_ment.text = Strings.REVIEW_MENT[Strings.LANGUAGE_TYPE];

                          TweenScript.Instance.setTweenScaleUp(ob_reivew_popup, 0, 0.5f);
                          Prefs.setPrefsBoolean(Prefs.REVIEW_TIMING, true); //리뷰 팝업을 떳다는 상태 값 저장(1번만 띄우기 위함)
                      }
                  }
                  else
                  {
                      Prefs.setPrefsBoolean(Prefs.REVIEW_TIMING, false);
                  }
            }
        }
        else //리뷰 팝업 없앰
        {
            mPopup_ON = false;

            ob_Popup.SetActive(false);
            ob_reivew_popup.SetActive(false);
        }
    }

    //리뷰 달기
    public void clicked_review(GameObject button)
    {
        if (button.name.Equals("btn_yes")) //리뷰 달기
        {
            //Prefs.setPrefsJewel(Strings.REVIEW_JEWEL); //리뷰 보상 보석 저장
            Prefs.setPrefsBoolean(Prefs.REVIEW , true); //리뷰 달았다는 상태 저장

            Application.OpenURL("https://play.google.com/store/apps/details?id=com.gggg.ggggbox2");

            Review_popup(false); //리뷰 팝업 없앰

            Gold_text();
        }
        else if (button.name.Equals("btn_no")) //리뷰 팝업 닫기
        {
            Review_popup(false); //리뷰 팝업 없앰
        }
    }

    //비디오 광고 버튼
    public void clicked_video(GameObject button)
    {
        if (button.name.Equals("btn_yes")) //비디오 광고 시청
        {
            //Become21Plugin.showUnityAd(); //비디오 광고 호출
            Video_Popup(false); //팝업 닫음
        }
        else if (button.name.Equals("btn_no")) //비디오 광고 팝업 닫기
        {
            Video_Popup(false);
        }
    }

    //버튼 클릭시 발생 함수
    public void clicked_button(GameObject button)
    {
        if (mScene_change) return; //장면 전환 할 때 버튼 안 눌리게 막음

        if (button.name.Equals("btn_back")) //골드 구매 팝업 버튼
        {
            onBack();
        }
        else if (button.name.Equals("btn_species")) //종족 구매 팝업 버튼
        {
            Popup_ON(0);
        }
        else if (button.name.Equals("btn_gold")) //골드 구매 팝업 버튼
        {
            Popup_ON(1);
        }
        else if (button.name.Equals("btn_jewel")) //보석 구매 팝업 버튼
        {
            if (mFeverTime) return;

            mJewel_pop = true;
            Popup_ON(2);
        }
        else if (button.name.Equals("btn_setting")) //설정 버튼
        {
            Popup_ON(3);
        }
        else if (button.name.Equals("sp_buff_gold")) //골드 버프 버튼
        {
            Buff_Notice(0);
        }
        else if (button.name.Equals("sp_buff_dance")) //피버타임 버프 버튼
        {
            Buff_Notice(1);
        }
        else if (button.name.Equals("btn_energy_sun")) //태양 버튼
        {

            if (!mSun_Click) return; //태양 에너지가 100프로가 아니면 클릭 안되게 막음.

            SunGold(); //태양 폭파
            return;
        }
        else if (button.name.Equals("sp_levleUp_popup")) //레벨업  설명 팝업 닫음.
        {
            //실행 되고 있는 게 있다면 멈춰 줌.
            if (IeNumer != null) StopCoroutine(IeNumer);

            TweenScript.Instance.setTweenAlpha(LevelUp_popup, 0f, 0.5f, 1, 0);
        }
        else if (button.name.Equals("btn_Reincarnation")) //환생하기
        {
            Character_Reincarnation();
        }
        else if (button.name.Equals("btn_yes")) //구매 확인 팝업 닫기
        {
            ob_Popup.SetActive(false);
            ob_iden_popup.SetActive(false);
        }

        Main_script.Instance.SoundPlay(1, "touch"); //버튼 효과음
    }

    //씬 전환
    public void onScenc_Ani_End(GameObject gameObject)
    {
        Application.LoadLevel(0); //메인 씬으로 전환
    }

    IEnumerator Reincarnation_Btn_ON_Delay()
    {
        yield return new WaitForSeconds(1f);

        grid_item.SetActive(false); //아이템 리스트 OFF
        ob_Reincarnation.SetActive(true); //환샹 버튼 ON

        label_reincarnation_ment.text = Strings.REINCARNATION_MENT[Strings.LANGUAGE_TYPE];
        btn_Reincarnation.transform.GetComponent<UISprite>().spriteName = ImgDatas.COUNTRY_IMG[Strings.LANGUAGE_TYPE][2];
    }

    //환생 버튼 노출(item_script에서 상자와 슈퍼파워를 제외한 아이템들이 모두 만렙이면 호출함)
    public void Reincarnation_Btn_ON()
    {
        StartCoroutine(Reincarnation_Btn_ON_Delay());
    }

    //캐릭터 환생
    public void Character_Reincarnation()
    {

        mFeverTime = true;

        int reincarnation_level = Prefs.getPrefsInt(Prefs.CHARACTER_COLOR); //환생 레벨

        //환생 보상 보석 지급
        Prefs.setPrefsJewel(Strings.REINCARNATION_JEWEL[reincarnation_level]);

        int Reincarnation = Prefs.getPrefsInt(Prefs.CHARACTER_COLOR); //현재 환생 레벨
        Prefs.setPrefsInt(Prefs.CHARACTER_COLOR, Reincarnation + 1); //환생 레벨 업

        DataReset(); //레벨 데이터 초기화

        ob_Reincarnation.SetActive(false); //환샹 버튼 OFF
        grid_item.SetActive(true); //아이템 리스트 ON

        //나르고 있던 상자를 전부 집어 던지고 퇴장 한다.
        for (int i = 1; i < ob_character_box.transform.childCount; i++)
        {
            GameObject game_Launcher = ob_character_box.transform.GetChild(i).gameObject;

            game_Launcher.GetComponent<Launcher_script>().mFeverTime = true; //피버타임 상태 ON
            game_Launcher.GetComponent<Launcher_script>().Box_Throw();
            game_Launcher.GetComponent<Launcher_script>().Character_Back();
        }

        badge_color(); //환생 단계 체크해서 뱃지 색상 변경
        Gold_text();

        string ment = Strings.REINCARNATION_POPUP_MENT[Strings.LANGUAGE_TYPE].Replace("#", "" + Strings.REINCARNATION_JEWEL[reincarnation_level]);
        Buy_Popup(ment); //보석 보상 확인 팝업.

        Main_script.Instance.SoundPlay(1, "Reincarnation"); //환생 효과음
        //IgaworksUnityPluginAOS.Adbrix.retention("환생", "Level_" + Prefs.getPrefsInt(Prefs.CHARACTER_COLOR));

        StartCoroutine(Reincarnation_Refresh());

        //마지막 환생 시 자동으로 패스트푸드 종족으로 변환 시켜줌.
        if (Prefs.getPrefsInt(Prefs.CHARACTER_COLOR) == 8)
        {
            Prefs.setPrefsBoolean(Prefs.SPECIES_BUY + 4, true); //푸드 종족 상태 저장
            Species_change(4);
        }
    }

    IEnumerator Reincarnation_Refresh()
    {
        yield return new WaitForSeconds(2.3f);

        mFeverTime = false;

        for (int i = 1; i < ob_character_box.transform.childCount; i++)
        {
            GameObject game_Launcher = ob_character_box.transform.GetChild(i).gameObject;
            Destroy(game_Launcher);
        }

        CharacterBox_Make(); //캐릭터와 상자 생성
        
        //환생 할 때 종족이 인간이 아니면 슈퍼 파워 캐릭터 리플레쉬 시켜줌.
        if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) != 0)
        {
            for (int i = 0; i < grid_super.transform.childCount; i++)
            {
                GameObject super_Launcher = grid_super.transform.GetChild(i).gameObject;
                Destroy(super_Launcher);
            }

            grid_super.transform.localPosition = new Vector3(-331f, 53f, 0); //슈퍼파워 캐릭터 위치 지정 
            Super_Character(true);
        }
    }

    //환생 시 데이터를 초기화
    public void DataReset()
    {
        for (int i = 0; i < 7; i++)
        {
            PlayerPrefs.DeleteKey(Prefs.ITEM_LEVEL + i); //아이템 레벨 초기화
        }
            ItemList_Refresh(); //아이템 리스트뷰 새로고침
    }

    //아이템 리스트뷰 새로고침
    public void ItemList_Refresh()
    {
        for (int i = 0; i < 7; i++)
        {
            arItem_List[i].transform.GetComponent<item_script>().itemDataSetting(i); //리스트뷰 재 셋팅
        }
    }

    //태양 에너지가 차면 태양을 클릭하여 골드를 얻을 수 있음
    public void SunGold()
    {
        if (mFeverTime) return; //피버타임일 떈 폭파 안되게 막음.

        Main_script.Instance.SoundPlay(1, "goldsunburst");

        arSunGold = new List<GameObject>();

        mParticle_sun.Play(); //태양폭파 파티클

        btn_energy_sun.transform.GetComponent<UISprite>().spriteName = "sun_p1_2";
        btn_energy_sun.transform.GetComponent<UIButton>().normalSprite = "sun_p1_2";

        mSun_Click = false;
        mSun_Energy = 0; //에너지 초기화

        StartCoroutine(sun_Gold_Plus());
        StartCoroutine(sun_Gold_delete());
    }

    //태양폭파 골드 획득 애니메이션
    IEnumerator sun_Gold_Plus()
    {

        for (int i = 0; i < 50; i++)
        {

            int BoxLevel = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + 4); //상자 아이템의 레벨을 가져온다.
            int Gold_Buff = Prefs.getPrefsInt(Prefs.GOLD_BUFF); //골드 버프 값을 가져온다.

            long gold = (long)Strings.ITME_LEVEL_UP[4][BoxLevel] * Strings.GOLD_BUFF[Gold_Buff]; // + 골드

            Prefs.Gold_Save(gold); //골드 +

            Gold_text(); //골드와 보석 텍스트로 표시

            //골드 생성
            GameObject sp_gold = Instantiate(GoldPrefab) as GameObject;
            sp_gold.transform.parent = ob_Gold.transform;
            sp_gold.transform.localScale = Vector3.one;

            sp_gold.transform.Find("Label_gold").GetComponent<UILabel>().text = "+" + gold; // +되는 골드 표시

            sp_gold.transform.localPosition = new Vector3(275f, 254f, 0f); //골드 오브젝트 위치 지정

            int RandomX = UnityEngine.Random.Range(-259, 120);
            int RandomY = UnityEngine.Random.Range(-100, 277);

            TweenScript.Instance.setTweenPosition(sp_gold, 0, 1f, new Vector3(RandomX, RandomY, 0));
            TweenScript.Instance.setTweenAlpha(sp_gold, 0.4f, 0.5f, 1, 0);

            arSunGold.Add(sp_gold); //골드 오브젝트를 담는다.
            
            yield return new WaitForSeconds(0.06f);

        }
    }

    //태양폭파 골드 삭제
    IEnumerator sun_Gold_delete()
    {
        yield return new WaitForSeconds(5f);

        for (int i = 0; i < 50; i++)
        {
            Destroy(arSunGold[i]); //태양폭파 골드 오브젝트 삭제
        }
    }

    //버프 아이콘 ON
    public void Buff_Icon_ON_OFF(bool type)
    {
        btn_gold_buff.SetActive(type);
        btn_dance_buff.SetActive(type);
    }

    //버프 설명 박스 애니메이션
    public void Buff_Notice(int type)
    {

        if (mBuff_Box) return;

        mBuff_Box = true;
        mBuff_index = type;

        if (type == 0)
        {
            int gold_buff_index = Prefs.getPrefsInt(Prefs.GOLD_BUFF) + 1;

            label_gold_buff_notice.text = Strings.GOLD_POPUP_MENT[Strings.LANGUAGE_TYPE][0].Replace("#", "" + gold_buff_index);
            sp_gold_buff_notice_box.SetActive(true);

            TweenScript.Instance.setTweenAlpha(sp_gold_buff_notice_box, 0, 1f, 0, 1);
        
        } else
        {
            int sale_buff_index = Prefs.getPrefsInt(Prefs.UP_SALE_BUFF) + 1;

            label_sale_buff_notice.text = Strings.GOLD_POPUP_MENT[Strings.LANGUAGE_TYPE][1].Replace("#", "" + sale_buff_index);
            sp_sale_buff_notice_box.SetActive(true);

            TweenScript.Instance.setTweenAlpha(sp_sale_buff_notice_box, 0, 1f, 0, 1);
        }

        StartCoroutine(buff_Box_End());
    }

    //버프 설명 박스 사라짐
    IEnumerator buff_Box_End()
    {
        yield return new WaitForSeconds(3f);

        mBuff_Box = false;

        if (mBuff_index == 0)
        {
            TweenScript.Instance.setTweenAlpha(sp_gold_buff_notice_box, 0, 1f, 1, 0);
        }
        else
        {
            TweenScript.Instance.setTweenAlpha(sp_sale_buff_notice_box, 0, 1f, 1, 0);
        }
    }
 
    //골드,보석,설정 팝업 띄움
    public void Popup_ON(int type)
    {
        if (mFeverTime) return;

        mPopup_ON = true;

        ob_Popup.SetActive(true);

        if (type == 3) //설정 팝업
        {
            ob_setting_popup.SetActive(true);
        }
        else //골드,보석,종족 구매 팝업
        {
            if (!mFeverTime) {

               ob_shop_popup.SetActive(true);
               ob_shop_popup.transform.GetComponent<shop_popup_script>().Popup_Tpye_setting(type);
            } 
        }
    }

    //중간광고
    public void Middle_AdView()
    {
        int peopleIndex = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + 0) + 1; //사람추가 레벨

        if (Prefs.getPrefsInt(Prefs.CHARACTER_COLOR) == 0) //환생 레벨이 0 일 때만
        {
            if (peopleIndex % 5 == 0) //레벨 5,10,15,20,25 일 때 마다 중간광고 띄움
            {
                if (peopleIndex < 30) //30렙 전까지만
                {
                    Prefs.setPrefsBoolean(Prefs.MIDDLE_ADVIEW, true); //중간광고 노출 상태 저장
                    //Become21Plugin.showMiddleAd(); //중간광고
                }
            }
            else
            {
                Prefs.setPrefsBoolean(Prefs.MIDDLE_ADVIEW , false);
            }
        }
    }

    //비디오 광고
    public void Video_Popup(bool type)
    {
        if (mFeverTime) return; //피버타임 일 땐 비디오 광고 띄우지 않음.
       
        // if (type) //비디오 광고 시청
        // {
        //     int randomVideo = UnityEngine.Random.Range(0, 10); //랜덤 값을 구함

        //     //비디오 광고 1분에 20% 확률로 노출
        //     if (randomVideo < 3)
        //     {
        //         if (mPopup_ON) return; //팝업이 띄어져 있을 막아둠.
                
        //         mPopup_ON = true;

        //         ob_Popup.SetActive(true);
        //         //ob_video_popup.SetActive(true);
        //         //ob_video_popup.transform.GetComponent<UISprite>().spriteName = ImgDatas.COUNTRY_IMG[Strings.LANGUAGE_TYPE][3];

        //     }
        // }
        // else //비디오 팝업 닫기
        // {
        //     mPopup_ON = false;

        //     ob_Popup.SetActive(false);
        //     ob_video_popup.SetActive(false);
        // }
    }

    void OnEnable()
    {
        //Become21Plugin.EventOnUnityAd += HandleOnUnityAd;
    }

    void OnDisable()
    {
        //Become21Plugin.EventOnUnityAd -= HandleOnUnityAd;
    }

    //비디오 광고를 시청하고 돌아오면 호출
    public void HandleOnUnityAd(string param)
    {
        Debug.Log("[B21Plugin] 유니티 애드 동영상광고 보고 나옴");
        FeverTime_Start(); //피버타임
    }

    //백버튼 눌렀을 때
    void gameExit()
    {
        if (mPopup_ON) //팝업이 띄어 져 있는 상태 일 때 팝업 닫기
        {
            mPopup_ON = false;
            
            ob_Popup.SetActive(false);
            //ob_gold_popup.SetActive(false); //골드 구매 팝업
            //ob_jewel_popup.SetActive(false); //보석 구매 팝업
            ob_setting_popup.SetActive(false); //설정 팝업
            ob_review_popup.SetActive(false); //리뷰 팝업
            ob_iden_popup.SetActive(false); //구매 확인 팝업
            ob_reivew_popup.SetActive(false); //리뷰 팝업
            ob_video_popup.SetActive(false); //비디오 광고 팝업
            ob_shop_popup.SetActive(false); //종족,골드,보석 구매 팝업

            if (mJewel_pop) //보석 구매 팝업 창이 띄어져 있는 상태
            {
                if (jewel_popup_script.Instance.mTNK_adView)
                {
                    //Become21Plugin.callFreeCharge1(Strings.CHARGING_STATION_TITLE + "1");
                }
            }
        }
        else
        {
            onBack();
        }
    }

    //백버튼 눌렀을 때
    public void onBack()
    {
        if (mFeverTime) //피버타임 중에 메인으로 돌아가는거면 비지엠 다시 바꿈
        {
            Main_script.Instance.SoundPlay(0, "mainBGM");
        }

        SceneManager.Instance.popScene(); //메인으로 돌아가기
    }

    //이벤트 보상 아이템 애니메이션 효과
    public void Event_Item_Ani(GameObject _objdect , int type)
    {

        Main_script.Instance.SoundPlay(1, "event"); //이벤트 효과음

        if (type == 0) //보석 일 때
        {
            sp_event = ob_Event_icon.transform.Find("sp_event_jewel_icon").gameObject;
            Prefs.setPrefsJewel(Strings.EVENT_JEWLE); //보석 1개 지급
        }
        else //골드 일 떄
        {
            sp_event = ob_Event_icon.transform.Find("sp_event_gold_icon").gameObject;
            
            int BoxLevel = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + 4); //상자 아이템의 레벨을 가져온다.
            int Gold_Buff = Prefs.getPrefsInt(Prefs.GOLD_BUFF); //골드 버프 값을 가져온다.

            long gold = (long)Strings.ITME_LEVEL_UP[4][BoxLevel] * Strings.GOLD_BUFF[Gold_Buff] * 100; // + 골드 100배
            
            Prefs.Gold_Save(gold); //골드 x100 지급
        }

        sp_event.SetActive(true);
        TweenScript.Instance.setTweenAlpha(sp_event, 0, 0, 0, 1);
       
        Vector3 vectorLauncher = _objdect.transform.parent.gameObject.transform.localPosition;
        
        Vector3 vectorPosition = _objdect.transform.localPosition;
        Vector3 vectorScale = _objdect.transform.localScale;

        ob_Event_icon.transform.localPosition = new Vector3(vectorLauncher.x, vectorLauncher.y, vectorLauncher.z);
        sp_event.transform.localPosition = new Vector3(vectorPosition.x, vectorPosition.y, vectorPosition.z); //아이콘 오브젝트 위치 지정

        sp_event.transform.localScale = new Vector3(0.2f, 0.2f, 0.4f); 

        TweenScript.Instance.setTweenScale(sp_event, 0, 1.2f, new Vector3(1f, 1f , 1f)); //아이콘이 점점 켜지도록
        TweenScript.Instance.setTweenPosition(sp_event, 0, 1.2f, new Vector3(-vectorLauncher.x, -40f, 0f)); //지정된 위치로 골드or보석이 날아감.

        StartCoroutine(event_icon_delet());
    }

    IEnumerator event_icon_delet()
    {
        yield return new WaitForSeconds(2f);
        TweenScript.Instance.setTweenAlpha(sp_event , 0, 1.2f , 1 , 0);
    }

    //종족 변경
    public void Species_change(int species_type)
    {
        mFeverTime = true;
        mSpecies_Type = species_type;

        //나르고 있던 상자를 전부 집어 던지고 퇴장 한다.
        for (int i = 1; i < ob_character_box.transform.childCount; i++)
        {
            GameObject game_Launcher = ob_character_box.transform.GetChild(i).gameObject;

            game_Launcher.GetComponent<Launcher_script>().mFeverTime = true; //피버타임 상태 ON
            game_Launcher.GetComponent<Launcher_script>().Box_Throw();
            game_Launcher.GetComponent<Launcher_script>().Character_Back();
        }

        //슈퍼파워 캐릭터 숨김
        for (int i = 0; i < grid_super.transform.childCount; i++)
        {
            GameObject super_Launcher = grid_super.transform.GetChild(i).gameObject;
            super_Launcher.GetComponent<super_script>().FeverTime_Dapth(true); //캐릭터가 건물보다 뒤 쪽으로 가게 댑스 설정
        }

        TweenScript.Instance.setTweenPosition(grid_super.gameObject, 0, 1f, new Vector3(-358f, -100f, 0));
        StartCoroutine(super_change_make());

        Gold_text();
    }

    //슈퍼캐릭터 삭제 후 종족 변환 된 캐릭터로 다시 생성
    IEnumerator super_change_make()
    {
        yield return new WaitForSeconds(2.3f);

        Prefs.setPrefsInt(Prefs.SPECIES_TYPE, mSpecies_Type); //현재 종족 상태 저장

        mFeverTime = false;

        for (int i = 0; i < grid_super.transform.childCount; i++)
        {
            GameObject super_Launcher = grid_super.transform.GetChild(i).gameObject;
            Destroy(super_Launcher);
        }

        CharacterBox_Make(); //캐릭터와 상자 생성

        grid_super.transform.localPosition = new Vector3(-331f, 53f, 0); //슈퍼파워 캐릭터 위치 지정 
        Super_Character(true);
    }
}
