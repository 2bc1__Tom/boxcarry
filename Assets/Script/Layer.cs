﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Scene안에 들어가는 Layer클레스. 추가적으로 뜨는 화면은 Layer를 상속받아 사용한다.
/// </summary>

public class Layer : MonoBehaviour {
	///자신의 부모 씬 소속이 아닌 전처리를 위한 레이어의 경우 Awake안에서 true처리를 해준다.
	public bool isStatic = false;

	void OnEnable() {
		///전역 레이어의 경우 마지막 씬에 추가해 주고, 아닐 경우 자신의 부모 씬에 넣어준다.
		if (isStatic) {
			SceneManager.Instance.lastScene ().addLayer (this);
		} else {
			GetComponentInParent<Scene> ().addLayer (this);
		}
	}

	void OnDisable() {
		///마찬가지로 씬을 뺀다.
		if (isStatic) {
			SceneManager.Instance.lastScene ().removeLayer (this);
		} else {
			GetComponentInParent<Scene> ().removeLayer (this);
		}
	}

	///레이어 종료 시 호출되는 함수, 상속 받는 하위 클레스는 해당 함수를 오버라이드하여 사용한다.
	public virtual void onEndLayer() {}
}
