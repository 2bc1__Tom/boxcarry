using UnityEngine;
using System.Collections;

public class ClickEventReceiver : MonoBehaviour {

    public GameObject Reciver = null;
    private string RootName = string.Empty;

    void _SendMessage (string messageName)
    {        
        if (Reciver == null) {
            Debug.LogWarning ("Not Find Reciver:" + this.gameObject.name);
            return;
        }

        Reciver.SendMessage (messageName, this.gameObject);
    }

    void Awake ()
    {

    }

    IEnumerator Start ()
    {
        /*
        if (string.IsNullOrEmpty (RootName) == true) {
            RootName  = iSceneMaster.FindRootSceneName (transform);

            if (string.IsNullOrEmpty (RootName) == true) {        
                Debug.LogError ("Not Find RootName");
                Debug.Break ();
            }
        }
         * */

        if (Reciver == null) {
           
            while (true) {
                
                Reciver = GameObject.Find("GUI@" + RootName) as GameObject;
                
                if (Reciver != null) {
                    break;
                }

                yield return new WaitForSeconds (1f);
            }
        }

        _SendMessage ("OnCreate");
    }

    void OnClick ()
    {
        _SendMessage ("OnClick");
    }

    void OnPress (bool isPressed)
    {
        if (enabled) {
            if (isPressed) {
                _SendMessage ("OnPress");
            } else {
                _SendMessage("OnRelease");
            }
        }
    }

    void OnActivate()
    {
        //_SendMessage ("OnActivate");
    }
}
