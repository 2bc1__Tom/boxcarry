﻿using UnityEngine;
using System.Collections;

public class setting_popup : MonoBehaviour {

    public GameObject ob_popup; //골드,보석,설정 파업이 담겨 있는 팝업 오브젝트
    public GameObject sp_setting_bg; //골드 팝업 오브젝트

    public GameObject btn_Bgm_sound; //배경음 ON_OFF 버튼 오브젝트
    public GameObject btn_effect_sound; //효과음 ON_OFF 버튼 오브젝트
    public GameObject btn_google; //구글플레이 로그아웃 버튼 오브젝트

    // Use this for initialization
    void Start()
    {
        Sound_check(0); //비지엠 버튼 체크
        Sound_check(1); //효과음 버튼 체크

        if (Social.localUser.authenticated) //로그인 상태
        {
            btn_google.transform.GetComponent<UISprite>().spriteName = "btn_p4_1";
            btn_google.transform.GetComponent<UIButton>().normalSprite = "btn_p4_1";
        }
        else //로그아웃 상태
        {
            btn_google.transform.GetComponent<UISprite>().spriteName = "btn_p4_1_2";
            btn_google.transform.GetComponent<UIButton>().normalSprite = "btn_p4_1_2";
        }
    }

    // Update is called once per frame
    void Update()
    {}

    //버튼 클릭시 발생 함수
    public void clicked_button(GameObject button)
    {

        if (button.name.Equals("btn_bg_sound")) //비지엠 ON_OFF
        {

            if (Prefs.getPrefsBoolean(Prefs.SOUND_BGM))
            {
                Prefs.setPrefsBoolean(Prefs.SOUND_BGM, false); //비지엠 OFF
                Game_script.Instance.BGM_stop(); //비지엠 멈춤
            }
            else
            {
                Prefs.setPrefsBoolean(Prefs.SOUND_BGM, true); //비지엠 ON
                Game_script.Instance.BGM_start();

                //if (Game_script.Instance.mFeverTime) //피버 중 일 때
                //{
                //    Main_script.Instance.SoundPlay(0, "DanceTime"); //비지엠 실행
                //}
                //else
                //{
                //    Main_script.Instance.SoundPlay(0, "mainBGM"); //비지엠 실행
                //}
            }

            Sound_check(0);
        }
        else if (button.name.Equals("btn_effect_sound")) //효과음 ON_OFF
        {
            if (Prefs.getPrefsBoolean(Prefs.SOUND_EFFECT))
            {
                Prefs.setPrefsBoolean(Prefs.SOUND_EFFECT, false); //효과음 OFF
            }
            else
            {
                Prefs.setPrefsBoolean(Prefs.SOUND_EFFECT, true); //효과음  ON
            }

            Sound_check(1);
        }
        else if (button.name.Equals("btn_google")) //구글 로그아웃
        {
            Google_Log_In_out();
        }
        else if (button.name.Equals("btn_url")) 
        {
            Application.OpenURL("http://admin.becomead.com/provision/provision.php?lang=kr");
        }
        else if (button.name.Equals("btn_x")) //팝업 닫기
        {
            ob_popup.SetActive(false);
            sp_setting_bg.SetActive(false);

            Game_script.Instance.mPopup_ON = false;
        }

        Main_script.Instance.SoundPlay(1, "touch"); //버튼 효과음
    }

    //사운드 버튼이 ON 상태인지 OFF 상태인지 체크
    public void Sound_check(int type)
    {

        if (type == 0) //비지엠
        {
            if (Prefs.getPrefsBoolean(Prefs.SOUND_BGM))
            {
                btn_Bgm_sound.transform.GetComponent<UISprite>().spriteName = "btn_p4_2"; //비지엠 ON
                btn_Bgm_sound.transform.GetComponent<UIButton>().normalSprite = "btn_p4_2";
            }
            else
            {
                btn_Bgm_sound.transform.GetComponent<UISprite>().spriteName = "btn_p4_3"; //비지엠 OFF
                btn_Bgm_sound.transform.GetComponent<UIButton>().normalSprite = "btn_p4_3";
            }
        }
        else //효과음
        {
            if (Prefs.getPrefsBoolean(Prefs.SOUND_EFFECT))
            {
                btn_effect_sound.transform.GetComponent<UISprite>().spriteName = "btn_p4_2"; //효과음 ON
                btn_effect_sound.transform.GetComponent<UIButton>().normalSprite = "btn_p4_2";
            }
            else
            {
                btn_effect_sound.transform.GetComponent<UISprite>().spriteName = "btn_p4_3"; //효과음 OFF
                btn_effect_sound.transform.GetComponent<UIButton>().normalSprite = "btn_p4_3";
            }
        }
    }

    //구글 플레이 로그인,로그아웃
    public void Google_Log_In_out()
    {
        if (Social.localUser.authenticated) //로그인 상태라면 로그아웃 시킴
        {
            btn_google.transform.GetComponent<UISprite>().spriteName = "btn_p4_1_2";
            btn_google.transform.GetComponent<UIButton>().normalSprite = "btn_p4_1_2";

            Google_play.Instance.SignOut(); //구글 플레이 로그아웃
        }
        else //로그아웃 상태라면 로그인 시킴
        {
            btn_google.transform.GetComponent<UISprite>().spriteName = "btn_p4_1";
            btn_google.transform.GetComponent<UIButton>().normalSprite = "btn_p4_1";

            Google_play.Instance.LogIn(); //구글 플레이 로그인
        }
    }
}
