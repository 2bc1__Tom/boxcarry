﻿using UnityEngine;
using System.Collections;

public class shop_popup_script : MonoBehaviour {

    public GameObject ob_popup;
    public GameObject ob_shop;

    public GameObject ob_btn_species; //종족 구매 탭 버튼
    public GameObject ob_btn_gold; //골드 구매 탭 버튼
    public GameObject ob_btn_jewel; //보석 구매 탭 버튼

    public GameObject ob_species; //종족 구매 팝업
    public GameObject ob_gold; //골드 구매 팝업
    public GameObject ob_jewel; //보석 구매 팝업

	// Use this for initialization
	void Start () {
       
	}

    public void Popup_Tpye_setting(int type_popup)
    {

        ob_species.SetActive(false);
        ob_gold.SetActive(false);
        ob_jewel.SetActive(false);

        //탭 OFF 이미지
        ob_btn_species.transform.GetComponent<UIButton>().normalSprite = "tap_p7_off";
        ob_btn_gold.transform.GetComponent<UIButton>().normalSprite = "tap_p7_off";
        ob_btn_jewel.transform.GetComponent<UIButton>().normalSprite = "tap_p7_off";

        if (type_popup == 0) //종족 구매 팝업
        {
            ob_species.SetActive(true);
            ob_species.transform.GetComponent<species_script>().Species_popup_Setting();
            ob_btn_species.transform.GetComponent<UIButton>().normalSprite = "tap_p7_sel"; //탭 ON 이미지

        }
        else if (type_popup == 1) //골드 구매 팝업
        {
            ob_gold.SetActive(true);
            ob_btn_gold.transform.GetComponent<UIButton>().normalSprite = "tap_p7_sel"; //탭 ON 이미지
        }
        else if (type_popup == 2) //보석 구매 버튼
        {
            ob_jewel.SetActive(true);
            ob_btn_jewel.transform.GetComponent<UIButton>().normalSprite = "tap_p7_sel"; //탭 ON 이미지
        }
    }

    //탭 버튼 클릭시 발생 함수
    public void clicked_button(GameObject button)
    {

        if (button.name.Equals("btn_species"))
        {
            Popup_Tpye_setting(0);
        }
        else if (button.name.Equals("btn_gold"))
        {
            Popup_Tpye_setting(1);
        }
        else if (button.name.Equals("btn_jewel"))
        {
            Popup_Tpye_setting(2);
        }
        else if (button.name.Equals("btn_x"))
        {
            ob_popup.SetActive(false);
            ob_shop.SetActive(false);

            Game_script.Instance.mPopup_ON = false;
        }
    }
}
