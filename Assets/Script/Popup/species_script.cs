﻿using UnityEngine;
using System.Collections;

public class species_script : MonoBehaviour {

    public GameObject ob_popup;
    public GameObject ob_shop;

    public UILabel label_buy_ment_dot;
    public UILabel label_buy_ment_cat;
    public UILabel label_buy_ment_draong;
    public UILabel label_buy_ment_food;

    public GameObject[] ob_Species;
    public int mType = 0;

	// Use this for initialization
	void Start () {
	}

    public void Species_popup_Setting()
    {
        for (int i = 0; i < 5; i++)
        {
            GameObject btn_gold_buy = ob_Species[i].transform.Find("btn_gold_buy_" + i).gameObject;
            GameObject btn_jewel_buy = ob_Species[i].transform.Find("btn_jewel_buy_" + i).gameObject;
            GameObject ob_btn_use = ob_Species[i].transform.Find("btn_use_" + i).gameObject;

            UIButton btn_use = ob_btn_use.transform.GetComponent<UIButton>();

            //종족이 구매 되어 있는 상태인지 아닌지  구분
            if (Prefs.getPrefsBoolean(Prefs.SPECIES_BUY + i)) //구매 된 상태
            {
                //현재 선택되어 있는 종족이 어떤건지 구분
                if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == i)
                {
                    btn_use.normalSprite = "btn_p7_1"; //사용 중 이미지
                
                    GameObject ob_use = ob_btn_use.transform.Find("Label").gameObject;
                    ob_use.transform.GetComponent<UILabel>().text = "사용 중";
                }
                else
                {
                    btn_use.normalSprite = "btn_p7_2"; //사용하기 이미지

                    GameObject ob_use = ob_btn_use.transform.Find("Label").gameObject;
                    ob_use.transform.GetComponent<UILabel>().text = "사용하기";
                }

                btn_gold_buy.SetActive(false); //골드로 구매 버튼 OFF
                btn_jewel_buy.SetActive(false); //보석으로 구매 버튼 OFF
                ob_btn_use.SetActive(true); //사용하기 버튼 ON
            }
            else // 구매 전
            {
                int level = Prefs.getPrefsInt(Prefs.CHARACTER_COLOR); //현재 환생 레벨
                
                //환생 레벨이 안 될 시 멘트
                if (Strings.SPECIES_BUY_REIN_LEVEL[i] > level)
                {

                    if (i == 1)
                    {
                        label_buy_ment_dot.text = "2번째 환생 대기";
                    }
                    else if (i == 2)
                    {
                        label_buy_ment_cat.text = "4번째 환생 대기";
                    }
                    else if (i == 3)
                    {
                        label_buy_ment_draong.text = "6번째 환생 대기";
                    }
                }
                else
                {
                    if (i == 1)
                    {
                        label_buy_ment_dot.text = "1,000,000,000";
                    }
                    else if (i == 2)
                    {
                        label_buy_ment_cat.text = "3,000,000,000";
                    }
                    else if (i == 3)
                    {
                        label_buy_ment_draong.text = "10,000,000,000";
                    }
                }
            }
        }
    }

    //버튼 클릭시 발생 함수
    public void clicked_button(GameObject button)
    {

        int buttonIndex = int.Parse(button.name.Substring(button.name.Length - 1, 1));

        if (button.name.Contains("btn_gold_buy"))
        {
            Species_Buy(buttonIndex , 0);
        }
        else if (button.name.Contains("btn_jewel_buy"))
        {
            Species_Buy(buttonIndex , 1);
        }
        else if (button.name.Contains("btn_use"))
        {

            //구매 된 상태가 아니라면 버튼 막음.
            if (!Prefs.getPrefsBoolean(Prefs.SPECIES_BUY + buttonIndex))
            {
                return;
            }

            //사용 중인 겨우 버튼 막음.
            if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == buttonIndex)
            {
                return;
            }

            Game_script.Instance.Species_change(buttonIndex); //종족 교체
            //Prefs.setPrefsInt(Prefs.SPECIES_TYPE , buttonIndex); //현재 종족 상태 저장

            ob_popup.SetActive(false);
            ob_shop.SetActive(false);

            Game_script.Instance.mPopup_ON = false;

            Species_popup_Setting();
        }
    }

    //종족 구매
    public void Species_Buy(int species_type ,  int type)
    {

        string[] species_title = {"졸라맨","강아지" , "고양이" , "드래곤" , "패스트푸드"};

        if (type == 0) //골드로 구매
        {
            int Reincarnation_Level = Prefs.getPrefsInt(Prefs.CHARACTER_COLOR); //현재 환생 레벨 값

            //구매 할 수 있는 환생 레벨 조건이 만족한다면 골드로 구매 가능
            if (Reincarnation_Level >= Strings.SPECIES_BUY_REIN_LEVEL[species_type])
            {
                long gold = Prefs.GetGold();

                //종족 구매
                if (gold > Strings.SPECIES_GOLD[species_type])
                {
                    //구매 상태 저장
                    Prefs.setPrefsBoolean(Prefs.SPECIES_BUY  + species_type , true);
                    Prefs.Gold_Minus(Strings.SPECIES_GOLD[species_type]); //골드 마이너스

                    Game_script.Instance.Gold_text();

                    Species_popup_Setting();

                    //애드브릭스 통계 저장
                    //IgaworksUnityPluginAOS.Adbrix.retention(species_title[species_type] , "골드 구매");
                }
            }
        }
        else //보석으로 구매
        {

            int jewel = int.Parse(Prefs.getPrefsJewel());

            //종족 구매
            if (jewel > Strings.SPECIES_JEWEL[species_type])
            {
                Prefs.setPrefsBoolean(Prefs.SPECIES_BUY + species_type, true); //구매 상태 저장
                Prefs.setPrefsMinus_Jewel(Strings.SPECIES_JEWEL[species_type]); //보석 마이너스

                Game_script.Instance.Gold_text();

                //애드브릭스 통계 저장
                //IgaworksUnityPluginAOS.Adbrix.retention(species_title[species_type], "보석 구매");

                Species_popup_Setting();
            }
        }
    }
}
