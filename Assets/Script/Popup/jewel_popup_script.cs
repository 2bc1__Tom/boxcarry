﻿using UnityEngine;
using System.Collections;
using TapjoyUnity;

public class jewel_popup_script : MonoSingleton<jewel_popup_script>
{

    public GameObject ob_popup; //골드,보석,설정 파업이 담겨 있는 팝업 오브젝트
    public GameObject sp_shop_bg; //팝업 오브젝트

    public UILabel label_gold_free_1; //무료충전소_1
    public UILabel label_gold_free_2; //무료충전소_2

    public UILabel label_jewel_buy_1; //100보석 구매
    public UILabel label_jewel_buy_2; //1000보석 구매

    public bool mTNK_adView = true; //광고를 보지 않고 팝업을 닫으면 무료충전소가 호출되게 하기 위함.

	// Use this for initialization
	void Start () {

        // label_gold_free_1.text = Strings.JEWEL_POPUP_MENT[Strings.LANGUAGE_TYPE][0];
        // label_gold_free_2.text = Strings.JEWEL_POPUP_MENT[Strings.LANGUAGE_TYPE][1];
        //label_jewel_buy_1.text = Strings.JEWEL_POPUP_MENT[Strings.LANGUAGE_TYPE][2];
        //label_jewel_buy_2.text = Strings.JEWEL_POPUP_MENT[Strings.LANGUAGE_TYPE][3];
	}
	
	// Update is called once per frame
	void Update () {
	}

    //버튼 클릭시 발생 함수
    public void clicked_button(GameObject button)
    {
        if (button.name.Equals("sp_gold_free_1")) //무료 충전소1
        {
            mTNK_adView = false;
            //Become21Plugin.callFreeCharge1(Strings.CHARGING_STATION_TITLE + "1");
            Day7OfferwallAds.Instance.ShowOfferwall1();
        }
        else if (button.name.Equals("sp_gold_free_2")) //무료 충전소2
        {
            mTNK_adView = false;
            //Become21Plugin.callFreeCharge2(Strings.CHARGING_STATION_TITLE + "2");
            Day7OfferwallAds.Instance.ShowOfferwall1();
        }
        else if (button.name.Equals("sp_gold_free_3")) //무료 충전소3
        {
            mTNK_adView = false;
            //BecomeOfferwallAds.Instance.ShowOfferwallTapjoy();
            Day7OfferwallAds.Instance.ShowOfferwall3();
        }
        else if (button.name.Equals("sp_jewel_buy_1")) //100보석 구매하기
        {
            mTNK_adView = false;
            //GoogleIAB.purchaseProduct(GoogleIABEventListener.skus[0]);
            //Day7InappManager.Instance.InappPurchase("gem_100");
            IAPManager.Instance.Purchase("gem_100");
        }
        else if (button.name.Equals("sp_jewel_buy_2")) //1000보석 구매하기
        {
            mTNK_adView = false;
            //GoogleIAB.purchaseProduct(GoogleIABEventListener.skus[1]);
            //Day7InappManager.Instance.InappPurchase("gem_1000");
            IAPManager.Instance.Purchase("gem_1000");
        }
        else if (button.name.Equals("btn_x")) //팝업 닫기
        {
            ob_popup.SetActive(false);
            sp_shop_bg.SetActive(false);

            Game_script.Instance.mJewel_pop = false;
            Game_script.Instance.mPopup_ON = false;

            if (mTNK_adView)
            {
                //BecomeOfferwallAds.Instance.ShowOfferwallTapjoy();
                Day7OfferwallAds.Instance.ShowOfferwall1();
            }
        }

        Main_script.Instance.SoundPlay(1, "touch"); //버튼 효과음
    }
}
