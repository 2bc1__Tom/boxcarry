﻿using UnityEngine;
using System.Collections;

public class Gold_popup_script : MonoBehaviour
{

    public GameObject ob_popup; //골드,보석,설정 파업이 담겨 있는 팝업 오브젝트
    public GameObject sp_shop_bg; //팝업 오브젝트
    //public GameObject sp_gold_bg; //골드 팝업 오브젝트

    public GameObject sp_gold_buff_buy; //골드 버프 버튼 오브젝트
    public GameObject sp_sale_buff_buy; //세일 버프 버튼 오브젝트

    public UILabel label_gold_jewel; //골드 버프 구매 보석 갯수
    public UILabel label_sale_jewel; //세일 버프 구매 보석 갯수

    public UILabel label_gold_Buy_1; //골드 구매 멘트
    public UILabel label_gold_Buy_2; //골드 구매 멘트

    public UISprite sp_bonus_plus;

	// Use this for initialization
	void Start () {

        GameObject label_ex_gold = sp_gold_buff_buy.transform.Find("Label_ex").gameObject;

        int gold_buff_index = Prefs.getPrefsInt(Prefs.GOLD_BUFF) + 2;

        if (gold_buff_index == 6) //골드 버프를 모두 구매 했을 때
        {
            sp_gold_buff_buy.transform.Find("sp_lock").gameObject.SetActive(true); //락 이미지 ON
            label_ex_gold.transform.GetComponent<UILabel>().text = Strings.PURCHASE_COMPLETE[Strings.LANGUAGE_TYPE];
            label_gold_jewel.text = "0"; //구매 보석 갯수 0으로 표시
        }
        else
        {
            //버프에 대한 설명
            label_ex_gold.transform.GetComponent<UILabel>().text = Strings.GOLD_POPUP_MENT[Strings.LANGUAGE_TYPE][0].Replace("#", "" + gold_buff_index);
            label_gold_jewel.text = "" + Strings.GOLD_BUFF_JEWEL[gold_buff_index-2]; //버프 구매 보석 갯수
        }

        //======================================================================

        GameObject label_ex_sale = sp_sale_buff_buy.transform.Find("Label_ex").gameObject;

        int sale_buff_index = Prefs.getPrefsInt(Prefs.UP_SALE_BUFF) + 2;

        if (sale_buff_index == 6) //골드 버프를 모두 구매 했을 때
        {
            sp_sale_buff_buy.transform.Find("sp_lock").gameObject.SetActive(true); //락 이미지 ON
            label_ex_sale.transform.GetComponent<UILabel>().text = Strings.PURCHASE_COMPLETE[Strings.LANGUAGE_TYPE];
            label_sale_jewel.text = "0"; //구매 보석 갯수 0으로 표시
        }
        else
        {
            //버프에 대한 설명
            label_ex_sale.transform.GetComponent<UILabel>().text = Strings.GOLD_POPUP_MENT[Strings.LANGUAGE_TYPE][1].Replace("#", "" + sale_buff_index);
            label_sale_jewel.text = "" + Strings.SALE_BUFF_JEWEL[sale_buff_index - 2]; //버프 구매 보석 갯수
        }

        label_gold_Buy_1.text = Strings.GOLD_POPUP_MENT[Strings.LANGUAGE_TYPE][2];
        label_gold_Buy_2.text = Strings.GOLD_POPUP_MENT[Strings.LANGUAGE_TYPE][3];

        sp_bonus_plus.spriteName = ImgDatas.COUNTRY_IMG[Strings.LANGUAGE_TYPE][0];

        Game_script.Instance.Gold_text(); //현재 골드와 보석 표시
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //버튼 클릭시 발생 함수
    public void clicked_button(GameObject button)
    {

        if (button.name.Equals("sp_gold_buff_buy")) //골드 x배 지급 버프
        {
            int gold_Buff = Prefs.getPrefsInt(Prefs.GOLD_BUFF); //현재 골드 버프가 몇 단계인지

            if (gold_Buff < 5)
            {
                Buff_Buy(0, Strings.GOLD_BUFF_JEWEL[gold_Buff]); 
            }
        }
        else if (button.name.Equals("sp_sale_buff_buy")) //레벨 업 비용 세일 버프
        {
            int sale_Buff = Prefs.getPrefsInt(Prefs.UP_SALE_BUFF); //현재 세일 버프가 몇 단계인지

            if (sale_Buff < 5)
            {
                Buff_Buy(1, Strings.SALE_BUFF_JEWEL[sale_Buff]);
            }
        }
        else if (button.name.Equals("sp_gold_buy_1")) //500,000골드 구매
        {
            Gold_Buy(0 , Strings.GOLD_1_JEWEL);
        }
        else if (button.name.Equals("sp_gold_buy_2")) //10,000,000골드 구매
        {
            Gold_Buy(1, Strings.GOLD_2_JEWEL);
        }
        else if (button.name.Equals("btn_x")) //팝업 닫기
        {
            ob_popup.SetActive(false);
            sp_shop_bg.SetActive(false);

            Game_script.Instance.mPopup_ON = false;
        }

        Main_script.Instance.SoundPlay(1, "touch"); //버튼 효과음
    }

    //골드,세일 버프 구매
    public void Buff_Buy(int type, int jewel_buy)
    {
        int jewel = Prefs.getPrefsInt(Prefs.PREFS_JEWEL); //현재 보석 갯수를 가져옴

        if (jewel >= jewel_buy)
        {
            if (type == 0) //골브 2배 버프 구매
            {
                int gold_Buff = Prefs.getPrefsInt(Prefs.GOLD_BUFF); //현재 골드 버프가 몇 단계인지
                Prefs.setPrefsInt(Prefs.GOLD_BUFF, (gold_Buff + 1)); //골드 버프 저장

                Game_script.Instance.Buy_Popup(Strings.BUY_MENT[Strings.LANGUAGE_TYPE][0].Replace("#", "" + (gold_Buff + 2))); //구매 확인 팝업 애니메이션
            }
            else //아이템 업 세일 버프 구매
            {
                int sale_Buff = Prefs.getPrefsInt(Prefs.UP_SALE_BUFF); //현재 세일 버프가 몇 단계인지
                Prefs.setPrefsInt(Prefs.UP_SALE_BUFF, (sale_Buff + 1)); //세일 버프 저장

                Game_script.Instance.ItemList_Refresh(); //아이템 리스트 데이터 갱신

                Game_script.Instance.Buy_Popup(Strings.BUY_MENT[Strings.LANGUAGE_TYPE][1].Replace("#", "" + (sale_Buff + 2))); //구매 확인 팝업 애니메이션
            }
           
            Prefs.setPrefsMinus_Jewel(jewel_buy); //보석 차감
            Game_script.Instance.Buff_Check(); //버프 아이콘 체크

            Start(); //버튼 상태 초기화
        }
        else
        {
            Game_script.Instance.Buy_Popup(Strings.JEWEL_LACK_MENT[Strings.LANGUAGE_TYPE]); //보석 부족 팝업.
        }

        sp_shop_bg.SetActive(false);
    }

    //골드 구매
    public void Gold_Buy(int type, int jewel_buy)
    {
        int jewel = Prefs.getPrefsInt(Prefs.PREFS_JEWEL); //현재 보석 갯수를 가져옴

        if (jewel >= jewel_buy)
        {
            Prefs.Gold_Save(Strings.JEWEL_BUY_GOLD[type]); //골드 지급 저장
            Prefs.setPrefsMinus_Jewel(jewel_buy); //보석 차감

            Game_script.Instance.Buy_Popup(Strings.BUY_MENT[Strings.LANGUAGE_TYPE][2].Replace("#", string.Format("{0:n0}", Strings.JEWEL_BUY_GOLD[type]))); //구매 확인 팝업 애니메이션

            Game_script.Instance.Gold_text(); //현재 골드와 보석 표시
        }
        else
        {
            Game_script.Instance.Buy_Popup(Strings.JEWEL_LACK_MENT[Strings.LANGUAGE_TYPE]); //보석 부족 팝업.
        }

        sp_shop_bg.SetActive(false);
    }
}
