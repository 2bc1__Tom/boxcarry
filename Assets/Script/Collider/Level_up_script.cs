﻿using UnityEngine;
using System.Collections;

public class Level_up_script : MonoBehaviour {

    //아이템 업 할 때 올라오는 LevelUp 이미지를 삭제 시키기 위한 스크립트

    public GameObject levelUp;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnTriggerEnter2D(Collider2D other)
    {
       
        if (other.gameObject.name.Contains("Label_title"))
        {
            StartCoroutine(LelevelUp_delete());
        }
    }

    //LevelUp 이미지 삭제
    IEnumerator LelevelUp_delete()
    {
        yield return new WaitForSeconds(1f);
        Destroy(levelUp);
    }
}
