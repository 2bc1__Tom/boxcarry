﻿using UnityEngine;
using System.Collections;

public class constructor_script : MonoBehaviour {

    /*
     
     왼쪽 건물 입구 쪽에 BoxCollider가 걸린 오브젝트를 만들어두고
     상자가 그 시점과 충돌하면 던지기 확률 시작과 다음 캐릭터를 생성 할 수 있게
     하기 위한 스크립트
    
     * **/

	// Use this for initialization
	void Start () {}
	
	// Update is called once per frame
	void Update () {}

    public void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.name.Contains("box_prefab"))
        {
            Game_script.Instance.CharacterBox_Make(); //캐릭터와 상자를 관리하는 런처 오브젝트 생성
        }
    }
}
