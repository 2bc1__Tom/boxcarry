﻿using UnityEngine;
using System.Collections;

public class right_buillding_script : MonoBehaviour {

    /*
     
     오른쪽 건물에 캐릭터가 충돌 했을 때 처리
    
     * **/

    // Use this for initialization
    void Start() { }

    // Update is called once per frame
    void Update() { }

    public void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.name.Contains("character_event")) //이벤트 캐릭터 일 떄
        {
            other.gameObject.name = "delete_event_character"; //삭제해야될 캐릭터를 구분하기 위해 이름을 변경해줌.

            GameObject ob_rauncher = other.gameObject.transform.parent.gameObject;
            ob_rauncher.GetComponent<Launcher_script>().ObjectDelete(other.gameObject); //캐릭터 삭제 시킴
        }
    }
}
