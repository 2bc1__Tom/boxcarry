﻿using UnityEngine;
using System.Collections;

public class Gold_delete_script : MonoBehaviour {

    /*
     
     *피버타임 시 골드 오브젝트가 충돌하면 삭제 시켜주기 위한 스크립트

    **/

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnTriggerEnter2D(Collider2D other)
    {

        if (other.gameObject.name.Contains("gold_plus_prefab")) //건물과 충돌 후 돈 올라가는 애니메이션이 끝나고 삭제 시킴
        {
            Destroy(other.gameObject); //골드 오브젝트를 삭제 시킴

        }else if (other.gameObject.name.Contains("fever_gold")) //피버 타임 일 때
        {
            Gold_Plus(other.gameObject , 2); //골드 2배 +

        }
    }

    public void Gold_Plus(GameObject other , int _plus)
    {
        Destroy(other.gameObject); //골드 오브젝트를 삭제 시킴

        int BoxLevel = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + 4); //상자 아이템의 레벨을 가져온다.
        int Gold_Buff = Prefs.getPrefsInt(Prefs.GOLD_BUFF); //골드 버프 값을 가져온다.

        long gold = (long)Strings.ITME_LEVEL_UP[4][BoxLevel] * Strings.GOLD_BUFF[Gold_Buff] * _plus; // + 골드 x배

        Prefs.Gold_Save(gold); //골드 + 시켜줌

        Game_script.Instance.Gold_text(); //골드 표시
        Main_script.Instance.SoundPlay(1, "coin"); //골드+ 효과음
    }
}
