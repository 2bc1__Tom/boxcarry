﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

/// <summary>
/// Scene의 추가, 삭제, 전환 등을 관리해준다. 싱글톤으로 구성되어있다.
/// 사용법 : Scene 클레스는 Scene을 상속 받아서 사용한다.
///	Scene의 루트는 1개로 구성되어 있어야 한다.
///	Scene의 이름은 각 Scene에 맞게 통일 되어 있어야 한다.
/// </summary>

public class SceneManager : Singleton<SceneManager> {
	///델리게이트 설정.
	public delegate IEnumerator OnNotificationCoroutine();
	public delegate void OnNotification();
	public delegate void OnSceneChanged(string newSceneName);
	
	///페이드 인아웃을 델리게이트로 설정하여 연출하도록...
	public OnNotificationCoroutine onFadeIn { get; set; }
	public OnNotificationCoroutine onFadeOut { get; set; }
	public OnNotification onExit { get; set; }

    public OnNotification onGameExit { get; set; }

	///Scene 전환 시 호출된다. BGM플레이 등 시 사용된다.
	public OnSceneChanged onSceneChanged { get; set; }

    //씬 전환시 기존씬을 처리하는 방법.
    const int TYPE_ACTIVE 	= 1;
    const int TYPE_INACTIVE = 2;
    const int TYPE_DESTROY 	= 3;

    ///씬 매니징을 위한 스택 객체. (씬 정보를 쌓아둠) 
    Stack<string> mSceneManager = new Stack<string>();

    ///INACTIVE된 씬을 관리하는 딕셔너리 객체. (씬 전환 시 기존 씬을 살려둬야할 경우 이 객체에 담아두고 INACTIVE시킨다. 되돌아올 시 ACTIVE.)
    Dictionary<string, GameObject> mSceneTemp = new Dictionary<string, GameObject>();

	bool mIsLoading = false;
	
    ///안드로이드의 경우 뒤로가기 버튼이 먹도록 설정.
    void Update() {
//        if (Application.platform == RuntimePlatform.Android) {
		if (Input.GetKeyDown(KeyCode.Escape) && !mIsLoading) {

            string objectName = mSceneManager.Peek();

            if (objectName.Equals("Game_Scene"))
            {
                if (onGameExit != null)
                {
                    onGameExit();
                }
            }
            else
            {
                 if (onExit != null)
                 {
                       onExit();
                 }
            }

            //if (mSceneManager.Count > 0) {
            //    ///백키에 대한 작업은 코르틴으로 진행한다.
            //    StartCoroutine(onBackPressed());
            //}
            //else {
            //    if (onExit != null)
            //    {
            //        onExit();
            //    }
            //}
    	}
//        }
    }

	///백키를 눌렀을때 레이어가 있다면 레이어를 먼저 제거하고 없다면 씬을 제거한다.
	IEnumerator onBackPressed() {

		///백키 여러번 누르는것을 방지한다.
		mIsLoading = true;

		//현재 씬 이름을 담을 변수 선언
		string objectName = mSceneManager.Peek();

        //if (objectName.Equals("Game_Scene"))
        //{
        //    yield break;
        //}

		//현재 씬 이름의 게임오브젝트를 찾아서 sceneObject에 담음
		GameObject sceneObject = GameObject.Find(objectName);
		sceneObject.GetComponent<Scene>().onBackPressed();
	
		///백키를 연속으로 눌러 나는 오류를 방지하기 위해 잠시 대기...
		yield return new WaitForSeconds (1.0f);

		mIsLoading = false;
	}

	///현재 씬의 이름을 반환.
	public string lastSceneName() {
		return mSceneManager.Peek ();
	}

	///현재 씬의 클레스를 반환.
	public Scene lastScene() {
		return GameObject.Find(mSceneManager.Peek()).GetComponent<Scene>();
	}

    //------------------------------------------------------------------------------------

    ///기존 씬을 삭제하고 새로운 씬을 올리는 메서드. (기존 씬 정보는 스택으로만 저장)
    public void pushScene(string sceneName) {
        //현재 씬과 지정한 씬 이름이 같을 경우 메서드를 빠져나간다.
		if (mSceneManager.Contains(sceneName)) {
            Debug.LogError("Scene name is the same as the current scene name .");
			return;
        }

        GameObject sceneObject = null;

        //스택에 저장되어 있는 씬이 있을 경우
        if (mSceneManager.Count > 0) {
            //저장되어 있는 씬을 sceneObject에 담음.
            sceneObject = GameObject.Find(mSceneManager.Peek());
        }

		//스택에 현재씬 이름을 쌓아둠.
		mSceneManager.Push(sceneName);

		//기존 씬은 제거하면서 새로운 씬을 붙여 주는 코루틴.
        StartCoroutine(pushSceneForCoroutine(sceneObject, sceneName, TYPE_DESTROY));
    }

	///씬을 바꾼다.
	public void replaceScene(string sceneName) {
		//현재 씬과 지정한 씬 이름이 같을 경우 메서드를 빠져나간다.
		if (mSceneManager.Contains(sceneName)) {
			Debug.LogError("Scene name is the same as the current scene name .");
			return;
		}

		GameObject sceneObject = null;
		
		//스택에 저장되어 있는 씬이 있을 경우
		if (mSceneManager.Count > 0) {
			//저장되어 있는 씬을 sceneObject에 담음.
			sceneObject = GameObject.Find(mSceneManager.Peek());
		}

		mSceneManager.Pop ();

		//스택에 현재씬 이름을 쌓아둠.
		mSceneManager.Push(sceneName);

		//기존 씬은 제거하면서 새로운 씬을 붙여 주는 코루틴.
		StartCoroutine(pushSceneForCoroutine(sceneObject, sceneName, TYPE_DESTROY));
	}

    ///기존 씬을 INACTIVE시키고 새로운 씬을 올리는 메서드. (기존 씬 정보는 스택과 딕셔너리에 저장)
    public void pushSceneForAdd(string sceneName) {
		//현재 씬과 지정한 씬 이름이 같을 경우 메서드를 빠져나간다.
		if (mSceneManager.Contains(sceneName)) {
			Debug.LogError("Scene name is the same as the current scene name .");
			return;
		}

        GameObject sceneObject = null;
        //스택에 저장되어 있는 씬이 있을 경우
        if (mSceneManager.Count > 0) {
            //되돌아갈 씬을 sceneObject에 담음.
            sceneObject = GameObject.Find(mSceneManager.Peek());

            if (sceneObject != null) {
                //되돌아갈 씬 정보를 딕셔너리에 담음. (INACTIVE 시켜줄 것이므로)
                mSceneTemp.Add(mSceneManager.Peek(), sceneObject);
            }
        }

		//스택에 현재씬 이름을 쌓아둠.
		mSceneManager.Push(sceneName);

		//기존 씬은 INACTIVE 시키면서 새로운 씬을 붙여 주는 코루틴.
        StartCoroutine(pushSceneForCoroutine(sceneObject, sceneName, TYPE_INACTIVE));
    }

    ///최상위 씬을 하나 삭제한다.
    public void popScene() {
        //저장되어 있는 씬이 있을 경우
        if (mSceneManager.Count > 1) {
            //현재 씬 이름을 담을 변수 선언
            string objectName = "";

			//현재 씬 이름을 ObjectName에 담음
			objectName = mSceneManager.Peek();

            //현재 씬 이름의 게임오브젝트를 찾아서 sceneObject에 담음
            GameObject sceneObject = GameObject.Find(objectName);
            //현재 씬 이름을 스택에서 제거
            mSceneManager.Pop();

            //저장되어 있는 씬이 있을 경우
            if (mSceneManager.Count > 0) {
				//이 전 씬 이름을 beforObejctName에 담음
				string beforeObjecName = mSceneManager.Peek();
                
                //이 전 씬이 INACTIVE 된 상태라면
                if (mSceneTemp.ContainsKey(beforeObjecName)) {
                    //현재 씬을 제거 하면서 이 전 씬을 ACTIVE 시켜 줌.
                    StartCoroutine(pushSceneForCoroutine(sceneObject, mSceneManager.Peek(), TYPE_ACTIVE));
                } else { //이 전 씬이 DESTROY 된 상태라면
                    //현재 씬을 제거 하면서 이 전 씬을 새로 붙여 줌.
                    StartCoroutine(pushSceneForCoroutine(sceneObject, mSceneManager.Peek(), TYPE_DESTROY));
                }
            } else { //되돌아갈 씬이 없는 경우
                //어플리케이션 종료
				if(onExit != null) {
					onExit();
				}
            }
        } else { //되돌아갈 씬이 없는 경우
            //어플리케이션 종료
			if(onExit != null) {
				onExit();
			}
        }
    }

    ///저장되어 있는 씬을 찾아서 보여준다. 
    public void popSceneForName(string sceneName) {
        //저장되어 있는 씬이 아닌 경우
        if (!mSceneManager.Contains(sceneName)) {
            //에러 메세지를 출력하면서 메서드를 빠져나간다.
            Debug.LogError("Check your sceneName");
            return;
        }

        //지정한 씬까지 뒤로가기 하고 화면에 띄워줌.
        StartCoroutine(popSceneForNameCoroutine(sceneName));
    }

    ///씬 전환을 자연스럽게 처리하는 코루틴
    IEnumerator pushSceneForCoroutine(GameObject gameObject, string sceneName, int type) {
		mIsLoading = true;

        //이 전 씬을 ACTIVE 시키는 경우
        if (type == TYPE_ACTIVE) {
            //저장되어 있는 씬이 아니라면 메서드를 빠져나간다.
            if (!mSceneTemp.ContainsKey(sceneName)) {
                Debug.LogError("Check your sceneName");
                yield break;
            }
        }

		//FadeInOut 효과
		if (onFadeIn != null) {
			yield return StartCoroutine(onFadeIn()); 
		}

        //각 타입에 맞게 기존 씬을 처리한다.
        switch (type) {
            //INACTIVE 타입이라면
            case TYPE_INACTIVE: {
                    //기존 씬을 비활성화 시켜준다.
                    if (gameObject != null) {
                        gameObject.SetActive(false);
                    }
                    break;
            }
            //ACTIVE 타입, DESTROY 타입일 경우    
            default: {
                    //기존 씬을 제거한다.
                    if (gameObject != null) {
                        Destroy(gameObject);
                    }
                    break;
            }
        }

		///씬이 전환될때 필요한 처리를 위해 호출.
		if (onSceneChanged != null) {
			onSceneChanged(sceneName);
		}

        //각 타입에 맞기 새로운 씬을 처리한다.
        switch (type) {
            //ACTIVE 타입이라면
            case TYPE_ACTIVE: {
                    //이 전 씬을 활성화 시켜준다.
                    mSceneTemp[sceneName].SetActive(true);
                    mSceneTemp.Remove(sceneName);
                    break;
            }
            //INACTIVE 타입, DESTROY 타입일 경우
            default: {
                    //씬을 새로 생성해서 붙여준다.
                    AsyncOperation operation = Application.LoadLevelAdditiveAsync(sceneName);

                    //씬 로드가 끝날 때까지 기다린다.
                    while (!operation.isDone) {
                        yield return new WaitForEndOfFrame();
                    }
                    break;
            }
        }

		//FadeOut 효과
		if (onFadeOut != null) {
			yield return StartCoroutine(onFadeOut()); 
		}

		mIsLoading = false;
    }

    ///저장되어 있는 씬을 찾아서 그씬으로 바로 이동한다.
    IEnumerator popSceneForNameCoroutine(string sceneName) {
		mIsLoading = true;

        //FadeIn 효과
		if (onFadeIn != null) {
			yield return StartCoroutine(onFadeIn()); 
		}

		///씬이 전환될때 필요한 처리를 위해 호출.
		if (onSceneChanged != null) {
			onSceneChanged(sceneName);
		}

        //저장되어 있는 씬이 있는 경우
        while (mSceneManager.Count > 0) {
            //스택에서 가장 최근 씬을 찾는다.
            string lastSceneName = mSceneManager.Peek();

            if (sceneName.Equals(lastSceneName)) {
                //씬이 InActive 되어 있다면
                if (mSceneTemp.ContainsKey(lastSceneName)) {
                    //지정한 씬을 활성화 시켜준다.
                    mSceneTemp[sceneName].SetActive(true);
                    //딕셔너리에서 해당 씬 정보를 제거
                    mSceneTemp.Remove(sceneName);
                } else {  //씬이 Destroy 되어 있다면
                        //지정한 씬을 새로 만들어 붙여준다.
                        AsyncOperation operation = Application.LoadLevelAdditiveAsync(sceneName);

                        //지정된 씬 로드가 끝날 때까지 기다린다.
                        while (!operation.isDone) {
                            yield return new WaitForEndOfFrame();
                        }
                }
                break;
            } else {
                //씬이 INACTIVE 된 상태라면
                if (mSceneTemp.ContainsKey(lastSceneName)) {
                    //씬 제거 및 딕셔너리에서 삭제한다.
                    Destroy(mSceneTemp[lastSceneName]);
                    mSceneTemp.Remove(lastSceneName);
                } else { //씬이 DESTROY 된 상태라면
                    //씬 오브젝트를 찾는다.
                    GameObject sceneObject = GameObject.Find(lastSceneName);

                    if (sceneObject != null) {
                        //DESTORY 된 씬이 혹시 살아 있다면 제거한다.
                        Destroy(sceneObject);
                    }
                }
                //씬을 스택에서 제거
                mSceneManager.Pop();
            }
		}

		//FadeOut 효과
		if (onFadeOut != null) {
			yield return StartCoroutine(onFadeOut()); 
		}

		mIsLoading = false;
    }
    //------------------------------------------------------------------------------------
}
