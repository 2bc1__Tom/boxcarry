﻿using UnityEngine;
using System.Collections;

public class TweenScript : MonoSingleton<TweenScript>
{

    //======================== Scale 애니메이션 ===========================

    public TweenScale setTweenScale(GameObject gameObject, float delay, float duration, Vector3 scaleTo)
    {

        TweenScale tweenScale = TweenScale.Begin(gameObject, duration, scaleTo); // Scale 애니메이션 실행
        tweenScale.delay = delay;

        return tweenScale;
    }

    //오브젝트 확대
    public TweenScale setTweenScaleUp(GameObject gameObject, float delay, float duration)
    {

        Vector3 scaleFrom = new Vector3(0, 0, 0);
        Vector3 scaleTo = new Vector3(1, 1, 1);
        
        TweenScale tweenScale = TweenScale.Begin(gameObject, duration, scaleTo); // Scale 애니메이션 실행
        
        tweenScale.from = scaleFrom;
        tweenScale.delay = delay;

        return tweenScale;
    }

    //오브젝트 축소
    public TweenScale setTweenScaleDown(GameObject gameObject, float delay, float duration)
    {

        Vector3 scaleFrom = new Vector3(1, 1, 1);
        Vector3 scaleTo = new Vector3(0, 0, 0);

        TweenScale tweenScale = TweenScale.Begin(gameObject, duration, scaleTo); // Scale 애니메이션 실행

        tweenScale.from = scaleFrom;
        tweenScale.delay = delay;

        return tweenScale;
    }

    //확대 반복
    public TweenScale setTweenScalePingPong(GameObject gameObject, float delay, float duration)
    {

        Vector3 scaleFrom = new Vector3(1, 1, 1);
        Vector3 scaleTo = new Vector3(1.1f, 1.1f, 1.1f);

        TweenScale tweenScale = TweenScale.Begin(gameObject, duration, scaleTo); // Scale 애니메이션 실행

        tweenScale.from = scaleFrom;
        tweenScale.delay = delay;

        //tweenScale.style = UITweener.Style.PingPong;

        return tweenScale;
    }

    //======================== Position 애니메이션 ===========================

    public TweenPosition setTweenPosition(GameObject gameObject, float delay, float duration, Vector3 positionFrom, Vector3 positionTo)
    {
        TweenPosition tweenPostion = TweenPosition.Begin(gameObject, duration, positionTo);

        tweenPostion.from = positionFrom;
        tweenPostion.delay = delay;

        return tweenPostion;
    }

    public TweenPosition setTweenPosition(GameObject gameObject, float delay, float duration, Vector3 positionTo)
    {
        TweenPosition tweenPostion = TweenPosition.Begin(gameObject, duration, positionTo);
        tweenPostion.delay = delay;

        return tweenPostion;
    }

    //======================== Rotation 애니메이션 ===========================

    public TweenRotation setTweenRotation(GameObject gameObject, float delay, float duration , bool type)
    {
        Vector3 RotationFrom;
        Vector3 RotationTo;

        if (type) //피버일 때
        {
            RotationFrom = new Vector3(0, 0, 0);
            RotationTo = new Vector3(0, 0, -179f);
        }
        else //아닐 때
        {
            RotationFrom = new Vector3(0, 0, -179f);
            RotationTo = new Vector3(0, 0, 0);
        }

        TweenRotation tweenRotation = TweenRotation.Begin(gameObject, duration, Quaternion.Euler(RotationTo));
        
        tweenRotation.from = RotationFrom;
        tweenRotation.delay = delay;

        return tweenRotation;
    }

    //======================== Alpha 애니메이션 ===========================

    public TweenAlpha setTweenAlpha(GameObject gameObject, float delay, float duration, float alphaFrom, float alphaTo)
    {
        TweenAlpha tweenAlpha = TweenAlpha.Begin(gameObject, duration, alphaTo);

        tweenAlpha.from = alphaFrom;
        tweenAlpha.delay = delay;

        return tweenAlpha;
    }

    //================ 애니메이션 이벤트 처리 함수 =============

    public EventDelegate setEventDelegate(GameObject _gameObject , MonoBehaviour _target, string _action)
    {

        EventDelegate eventDelegate = new EventDelegate(_target, _action);//gameObject.GetComponent<game_play_script>() , "");
        EventDelegate.Parameter param = new EventDelegate.Parameter(); // Parameter 생성후 

        param.obj = _gameObject.gameObject; // 오브젝트 집어넣고 
        param.expectedType = typeof(GameObject); //타입 변경
        eventDelegate.parameters[0] = param; // 이벤트 Parameter 삽입

        return eventDelegate;
    }
}
