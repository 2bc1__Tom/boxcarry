﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// 기본 씬은 해당 클레스를 상속받아서 사용한다. 
/// 씬 위에 뜨는 레이어에 대한 처리와 뒤로가기 시 처리가 되어있다.
/// </summary>

public class Scene : MonoBehaviour {
	///해당 씬에 포함된 레이어를 담아둔다.
	List<Layer> mLayerList = new List<Layer>();
	
	///레이어를 추가한다.
	public virtual void addLayer(Layer layer) {
		mLayerList.Add (layer);
	}
	
	///레이어를 삭제한다.
	public virtual void removeLayer(Layer layer) {
		mLayerList.Remove (layer);
	}

	///뒤로가기를 눌렀을 시 레이어가 있다면 먼저 삭제하고 없다면 씬을 종료한다.
	public virtual void onBackPressed() {
		if (mLayerList.Count > 0) {
			mLayerList[mLayerList.Count-1].onEndLayer();
		} else {
			onEndScene();
		}
	}

	///씬을 종료한다.
	public virtual void onEndScene() {
		SceneManager.Instance.popScene ();
	}
}
