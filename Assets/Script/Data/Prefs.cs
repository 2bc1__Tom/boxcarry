﻿using UnityEngine;
using System.Collections;

public class Prefs : MonoBehaviour
{
    //저장 파일 이름
    public static string PREFS_GOLD = "prefs_gold"; //골드 저장
    public static string PREFS_JEWEL = "prefs_jewel"; //보석 저장

    public static string SOUND = "sound"; //최초 처음 시작할 때 비지엠,효과음 ON으로 저장

    public static string SOUND_BGM = "sound_bgm"; //사운드 비지엠 여부
    public static string SOUND_EFFECT = "sound_effect"; //사운드 효과음 여부

    public static string NICK_NAME = "nick_name"; //이름 저장

    public static string GOLD_BUFF = "gold_buff"; //골드 버프
    public static string UP_SALE_BUFF = "up_sale_buff"; //업그레이드 비용 할인 버프

    public static string ITEM_LEVEL = "item_level"; //각 아이템 레벨 저장
    public static string ITEM_LEVEL_UP_GOLD = "level_up_gold"; //아이템 레벌 업 가격 저장

    public static string SUN_ENERGY = "sun_energy"; //태양 에너지 저장
    public static string CHARACTER_COLOR = "character_color"; //현재 환생 몇 단계인지 저장(캐릭터 색깔)

    public static string REVIEW_TIMING = "review_timing"; //리뷰를 팝업 타이밍 여부
    public static string REVIEW = "review"; //리뷰를 달았는지 여부

    public static string MIDDLE_ADVIEW = "middle_adview"; //중간광고 노출 타이밍 여부
    public static string VIDEO_ADVIEW = "video_adview"; //비디오 광고 노출 타이밍 여부

    public static string TUTORIAL = "tutorial_start"; //튜토리얼 여부 저장

    public static string DATA_CHANGE = "data_change"; //데이터 백업 여부 확인

    public static string SPECIES_TYPE = "species_type"; //현재 종족 상태 저장
    public static string SPECIES_BUY = "species_buy"; //종족 구매 상태 저장

    //=================== SET ===================

    //골드 저장
    public static void Gold_Save(long gold)
    {
        string saveGold = PlayerPrefs.GetString(PREFS_GOLD);

        long goldSum = 0;

        if (saveGold.Length != 0)
        {
            goldSum = long.Parse(saveGold) + gold;
        }
        else
        {
            goldSum = gold;
        }

        string strGold = "" + goldSum;
        PlayerPrefs.SetString(PREFS_GOLD, strGold);
    }

    //골드 마이너스
    public static void Gold_Minus(long gold)
    {

        string saveGold = PlayerPrefs.GetString(PREFS_GOLD);

        long GoldMinus = 0;

        if (saveGold.Length != 0)
        {
            GoldMinus = long.Parse(PlayerPrefs.GetString(PREFS_GOLD)) - gold;
        }

        if (GoldMinus < 0)
        {
            GoldMinus = 0;
        }

        string strGold = "" + GoldMinus;

        PlayerPrefs.SetString(PREFS_GOLD, strGold);
    }

    //골드 값을 가져옴
    public static long GetGold()
    {
        string saveGold = PlayerPrefs.GetString(PREFS_GOLD);

        if (saveGold.Length == 0)
        {
            return 0;
        }
        else
        {
            return long.Parse(PlayerPrefs.GetString(PREFS_GOLD));
        }
    }

    //보석 저장
    public static void setPrefsJewel(int jewel)
    {
        int goldSum = PlayerPrefs.GetInt(PREFS_JEWEL) + jewel;
        PlayerPrefs.SetInt(PREFS_JEWEL, goldSum);
    }

    //보석 마이너스
    public static void setPrefsMinus_Jewel(int jewel)
    {
        int jewelMinus = PlayerPrefs.GetInt(PREFS_JEWEL) - jewel;

        if (jewelMinus < 0)
        {
            jewelMinus = 0;
        }

        PlayerPrefs.SetInt(PREFS_JEWEL, jewelMinus);
    }

    //보석 값을 가져옴
    public static string getPrefsJewel()
    {
        string strJewel = PlayerPrefs.GetInt(PREFS_JEWEL) + "";
        return strJewel;
    }

    //-------------------------------------------------------------------

    public static void setPrefsInt(string name , int num)
    {
        PlayerPrefs.SetInt(name, num);
    }

    public static void setPrefsString(string name, string data)
    {
        PlayerPrefs.SetString(name, data);
    }

    public static void setPrefsBoolean(string name, bool data)
    {
        int index;

        if(data){
            index = 1;
        }else {
            index = 0;
        }

        PlayerPrefs.SetInt(name, index);
    }

    public static void setPrefsFloat(string name, float num)
    {
        PlayerPrefs.SetFloat(name , num);
    }

    //=================== GET ===================
 

    public static int getPrefsInt(string name)
    {
        return PlayerPrefs.GetInt(name);
    }

    public static string getPrefsString(string name)
    {
        return PlayerPrefs.GetString(name);
    }

    public static bool getPrefsBoolean(string name)
    {
        bool data = false;

        if (PlayerPrefs.GetInt(name) == 0)
        {
            data = false;
        }else {
            data = true;
        }

        return data;
    }

    public static float getPrefsFloat(string name)
    {
        return PlayerPrefs.GetFloat(name);
    }
 
}
