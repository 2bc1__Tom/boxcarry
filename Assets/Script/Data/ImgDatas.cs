﻿using UnityEngine;
using System.Collections;

public class ImgDatas : MonoBehaviour {

    //아이템 아이콘 이미지
    public static string[] ITME_ICON = {
        "btn_p1_1_1","btn_p1_1_2","btn_p1_1_3","btn_p1_1_4","btn_p1_1_5","btn_p1_1_6","btn_p1_1_7","btn_p1_2_1"
    };

    //박스 깨지는 이미지
    public static string[][] IMG_BOX_BROKEN = {

        //인간 상자
        new string[] {
            "box1_2","box2_2","box3_2","box4_2","box5_2","box6_4_2","box6_3_2","box_man4_2"
        },

        //강아지 상자
        new string[] {
            "box_dog_1_2","box_dog_2_2","box_dog_3_2","box_dog_1_2","box_dog_2_2","box_dog_1_2","box_dog_3_2","box_man4_2"
        },

        //고양이 상자
        new string[] {
            "box_cat_1_2","box_cat_2_2","box_cat_3_2","box_cat_2_2","box_cat_3_2","box_cat_2_2","box_cat_3_2","box_man4_2"
        },

        //드래곤 상자
        new string[] {
            "box_dragon_1_2","box_dragon_2_2","box_dragon_3_2","box_dragon_4_2","box_dragon_1_2","box_dragon_1_2","box_dragon_2_2","box_man4_2"
        },

        //패스트푸드 상자
        new string[] {
            "box_food_1_2","box_food_2_2","box_food_3_2","box_food_1_2","box_food_2_2","box_food_1_2","box_food_2_2","box_man4_2"
        },
    };

    //캐릭터 이동 프렘 이미지
    public static string[][][] CHARACTER_FRAME = { 
              
        //인간 종족                               
        new string[][] {

            //중간 캐릭터
            new string[] {            
                "man_1_boxback_", //뒤로 나르기
                "man_1_boxfront_", //앞으로 나르기
                "man_1_throw_", //던지기
                "man_1_run_", //달리기
                "man_1_run_", //달리기
            },

            //키큰 캐릭터
            new string[] {            
                "man_2_boxback_", //뒤로 나르기
                "man_2_boxfront_", //앞으로 나르기
                "man_2_throw_", //던지기
                "man_2_run_", //달리기
                "man_2_run_", //달리기
            },

            //여자 캐릭터
            new string[] {            
                "man_3_boxback_", //뒤로 나르기
                "man_3_boxfront_", //앞으로 나르기
                "man_3_boxfront_", //수레 나르기
                "man_3_throw_", //던지기
                "man_3_run_" //달리기
            },
        },

        //강아지 종족
        new string[][] {
           
            new string[] {            
                "dog_1_boxfront_", //앞으로 나르기
                "dog_1_boxfront_", //앞으로 나르기
                "dog_1_throw_", //던지기
                "dog_1_run_", //달리기
                "dog_1_run_", //달리기
            },

             new string[] {            
                "dog_2_boxfront_", //앞으로 나르기
                "dog_2_boxfront_", //앞으로 나르기
                "dog_2_throw_", //던지기
                "dog_2_run_", //달리기
                "dog_2_run_", //달리기
            },

            new string[] {            
                "dog_3_boxfront_", //앞으로 나르기
                "dog_3_boxfront_", //앞으로 나르기
                "dog_3_boxfront_", //앞으로 나르기
                "dog_3_throw_", //던지기
                "dog_3_run_" //달리기
            },
        },

       //고양이 종족
       new string[][] {

            new string[] {            
                "cat_1_boxfront_", //앞으로 나르기
                "cat_1_boxfront_", //앞으로 나르기
                "cat_1_throw_", //던지기
                "cat_1_run_", //달리기
                "cat_1_run_", //달리기
            },

             new string[] {            
                "cat_2_boxfront_", //뒤로 나르기
                "cat_2_boxfront_", //뒤로 나르기
                "cat_2_throw_", //던지기
                "cat_2_run_", //달리기
                "cat_2_run_", //달리기
            },

            new string[] {            
                "cat_3_boxfront_", //앞으로 나르기
                "cat_3_boxfront_", //앞으로 나르기
                "cat_3_boxfront_", //앞으로 나르기
                "cat_3_throw_", //던지기
                "cat_3_run_" //달리기
            },
        },

       //드래곤 종족
       new string[][] {

            new string[] {            
                "dragon1_", //앞으로 나르기
                "dragon1_", //앞으로 나르기
                "dragon_1_throw_", //던지기
                "dragon1_run_", //달리기
                "dragon1_run_", //달리기
            },

             new string[] {            
                "dragon2_", //앞으로 나르기
                "dragon2_", //앞으로 나르기
                "dragon_2_throw_", //던지기
                "dragon2_run_", //달리기
                "dragon2_run_", //달리기
            },

            new string[] {            
                "dragon3_", //앞으로 나르기
                "dragon3_", //앞으로 나르기
                "dragon3_", //앞으로 나르기
                "dragon_3_throw_", //던지기
                "dragon3_run_" //달리기
            },
        },

       //푸드 종족
       new string[][] {

            new string[] {            
                "food1_", //앞으로 나르기
                "food1_", //앞으로 나르기
                "food_1_throw_", //던지기
                "food1_run_", //달리기
                "food1_run_", //달리기
            },

             new string[] {            
                "food2_", //앞으로 나르기
                "food2_", //앞으로 나르기
                "food_2_throw_", //던지기
                "food2_run_", //달리기
                "food2_run_", //달리기
            },

            new string[] {            
                "food3_", //앞으로 나르기
                "food3_", //앞으로 나르기
                "food3_", //앞으로 나르기
                "food_3_throw_", //던지기
                "food3_run_" //달리기
            },
        },
    };

    //캐릭터 춤추는 이미지
    public static string[][] CHARACTER_DANCE = { 

            //중간 캐릭터
            new string[] {
                "man_1_dance1_", //댄스1
                "man_1_dance2_", //댄스2
                "man_1_dance3_", //댄스3
            },

            //키큰 캐릭터
            new string[] {
                "man_2_dance_", //댄스1
            },

            //여자 캐릭터
            new string[] {
                "man_3_dance_", //댄스1
            },
    };

    public static string[] CHARACTER_DANCE_DOG = { "dog1_", "dog2_", "dog3_" }; //멍멍이 댄스 이미지 이름

    public static string[] CHARACTER_DANCE_CAT = { "cat1_", "cat2_", "cat3_", "cat4_", "cat5_", "cat6_"}; //고양이 댄스 이미지 이름

    public static string[] CHARACTER_DANCE_DRAGON = { "dragon1_dance_", "dragon2_dance_", "dragon3_dance_", "dragon4_dance_"}; //드래곤 댄스 이미지 이름

    public static string[] CHARACTER_DANCE_FOOD = { "food1_dance_", "food2_dance_", "food3_dance_", "food4_dance_", "food5_dance_"}; //푸드 댄스 이미지 이름

    //번역 이미지
    public static string[][] COUNTRY_IMG = {
    
        //한글
        new string[] {
           "bg_p3_1_txt2","btn_p1_1_lock_1","btn_p1_3","popup_video",
        },

        //영어                                        
        new string[] {
           "bg_p3_1_txt2_e","btn_p1_1_lock_1_e","btn_p1_3_e","popup_video_e",
        },

        //중국어
        new string[] {
           "bg_p3_1_txt2_c","btn_p1_1_lock_1_c","btn_p1_3_c","popup_video_c",
        },

        //일어
        new string[] {
           "bg_p3_1_txt2_j","btn_p1_1_lock_1_j","btn_p1_3_j","popup_video_j",
        },
   };

    //번역 튜로리얼 이미지
    public static string[][] TUTORIAL_IMG = { 

         //한글
        new string[] {
           "tutorial_1","tutorial_2","tutorial_3","tutorial_4"
        },

        //영어                                        
        new string[] {
           "sc_0_e","sc_1_e","sc_2_e","sc_3_e"
        },

        //중국어
        new string[] {
          "sc_0_c","sc_1_c","sc_2_c","sc_3_c"
        },

        //일어
        new string[] {
           "sc_0_j","sc_1_j","sc_2_j","sc_3_j"
        },                         
   };

    //둥근 조명 프레임 이미지
    public static string LIGHTING_IMG = "bg_p2_3_";

    //빌딩 조명 프레임 이미지
    public static string BUILDING_LIGHTING_IMG = "building_p2_";
}
