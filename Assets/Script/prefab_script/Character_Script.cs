﻿using UnityEngine;
using System.Collections;

public class Character_Script : MonoBehaviour {

    public GameObject ob_Launcher; //캐릭터와 상자를 관리하는 런쳐 프리팹 오브젝트
    public GameObject ob_character; //캐릭터 오브젝트

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {

	}

    public void OnTriggerEnter2D(Collider2D other)
    {

        if (other.name.Contains("sp_door_left")) //왼쪽 빌딩과 충돌 했을 때
        {
            ob_Launcher.GetComponent<Launcher_script>().ObjectDelete(ob_character); //캐릭터 삭제 시킴
        }
    }
}
