﻿using UnityEngine;
using System.Collections;

public class item_script : MonoBehaviour {

    public UILabel lb_item_name; //아이템 이름
    public UILabel lb_item_level; //아이템 레벨
    public UILabel lb_item_gold; //아이템 가격

    public UISprite sp_item_icon; //아이템 아이콘 이미지
    public UISprite sp_gold_icon; //골드 아이콘
    public GameObject Levle_Up_Prefab; //레벨업 이미지 프리팹

    public GameObject ob_Lock; //만렙 시 환생대기 락 이미지
    public UISprite sp_Lock;

    public bool mFeverTime = false; //피버 타임인지 아닌지 구분
    //private int itemLevelUpGold = 0; //아이템 가격 인상률

	// Use this for initialization
	void Start () {}
	
	// Update is called once per frame
	void Update () {}

    //아이템 리스트 정보 셋팅
    public void itemDataSetting(int itemIndex)
    {

        int itemLevel = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + itemIndex); //아이템 레벨

        long level_up_gold = 0; //레벨업 골드

        //마지막 단계에선 레벨업 골드 10배로 함.
        if (Prefs.getPrefsInt(Prefs.CHARACTER_COLOR) == 8)
        {
            level_up_gold = Strings.LEVEL_UP_GOLD[itemLevel] * 10;
        }
        else
        {
            level_up_gold = Strings.LEVEL_UP_GOLD[itemLevel];
        }

        lb_item_name.text = Strings.ITEM_NAME[Strings.LANGUAGE_TYPE][itemIndex]; //아이템 이름 셋팅
        lb_item_level.text = "Level " + (itemLevel + 1); //아이템 레벨

        int sale_buff = Prefs.getPrefsInt(Prefs.UP_SALE_BUFF); //레벨 업 세일 버프 값을 가져옴
        float saleGold = (float)(level_up_gold / 100f) * Strings.SALE_BUFF[sale_buff]; //할일 금액

        long levelUpGold = (long)(level_up_gold - saleGold); //원가에서 할인 금액을 빼준다.

        lb_item_gold.text = "" + string.Format("{0:n0}", levelUpGold); //아이템 가격 표시

        ob_Lock.SetActive(false);
        sp_gold_icon.spriteName = "coin_strok"; //골드 아이콘

        //아이템 레벨이 만렙 일 때
        if (itemLevel == 29)
        {
            //상자랑 슈퍼파월을 제외 한 나머지 아이템 일 떄 그리고 환생 레벨이 만렙이 아닐 떄
            if (itemIndex != 4 && itemIndex != 7 && Prefs.getPrefsInt(Prefs.CHARACTER_COLOR) != 8)
            {
                sp_Lock.spriteName = ImgDatas.COUNTRY_IMG[Strings.LANGUAGE_TYPE][1];
                ob_Lock.SetActive(true); //만렙일 떄 환생 대기 락 이미지
            }

            lb_item_gold.text = "Full";
        }

        //슈퍼파월 아이템만 골드 아이콘을 보석으로 바꿈
        if (itemIndex == 7)
        {
            sp_gold_icon.spriteName = "gem_strok";
            lb_item_gold.text = "" + Strings.SUPER_ITME_JEWEL[itemLevel]; //아이템 가격 표시

            //아이템 레벨이 만렙 일 때
            if (itemLevel == 9)
            {
                lb_item_gold.text = "Full";
            }
        }
        else if (itemIndex == 4 && Prefs.getPrefsInt(Prefs.CHARACTER_COLOR) == 0) //환생 레벨이 0일 때만 상자 중간중간 레벨 업을 보석으로 할 수 있게 함.
        {
            if ((itemLevel + 2) % 5 == 0) //렙 5때 마다 보석 아이콘으로 바꿔준다.
            {
                sp_gold_icon.spriteName = "gem_strok";
                lb_item_gold.text = "" + Strings.BOX_ITME_JEWEL[(itemLevel + 1) / 5]; //아이템 가격 표시
            }
            else
            {
                sp_gold_icon.spriteName = "coin_strok"; //골드 아이콘
            }
        }

        sp_item_icon.spriteName = ImgDatas.ITME_ICON[itemIndex];
        Game_script.Instance.Gold_text(); //현재 골드와 수정 텍스트로 표시

        //----- 환생 버튼 노출 체크 ---

        if (Prefs.getPrefsInt(Prefs.CHARACTER_COLOR) < 8)
        {
            int index = 0;

            for (int i = 0; i < 7; i++)
            {
                if(i == 4) continue; //상자는 건너 뜀

                int Level = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + i); //아이템 레벨
           
                if (Level == 29) index++; //만렙 아이템이 몇개인지 체크
            }
            
            //상자와 슈퍼파워를 제외한 아이템들이 전부 만렙이면 환생 버튼 노출
            if (index == 6)
            {
                Game_script.Instance.Reincarnation_Btn_ON();
            }
        }
    }

    //아이템 클릭 했을 때 호출
    public void clicked_button(GameObject _button)
    {
        if (mFeverTime) return; //피버타임 일 땐 버큰 안 눌리도록 막아둠.

        int itemIndex = int.Parse(_button.name);
        int itemLevel = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + itemIndex); //아이템 레벨

        if (itemIndex == 7) { //슈퍼파워 일 때

            //슈퍼파워 인원 최대 10명 까지만
            if (Prefs.getPrefsInt(Prefs.ITEM_LEVEL + itemIndex) >= (Strings.SUPER_MAX - 1))
            {
                return;
            }

            super_buy(itemIndex); //슈퍼파월 캐릭터 구매

            return;

        }else {

            //최대 아이템 레벨 30까지만
            if (Prefs.getPrefsInt(Prefs.ITEM_LEVEL + itemIndex) >= (Strings.ITEM_MAX - 1)) return;

            //환생 레벨이 0일 때만 상자 중간중간 레벨 업을 보석으로 할 수 있게 함.
            if (itemIndex == 4 && Prefs.getPrefsInt(Prefs.CHARACTER_COLOR) == 0) 
            {
                if ((itemLevel + 2) % 5 == 0) //렙 5때 마다 보석으로 레벨 업 할 수 있다.
                {
                    Box_jewel_buy(); //상자 레벨 업 보석으로 업그레이드
                    return;
                }
            }
        }

        long gold = Prefs.GetGold(); //현재 보유 골드를 가져온다.
        long level_up_gold = 0; //레벨업 골드

        //마지막 단계에선 레벨업 골드 10배로 함.
        if (Prefs.getPrefsInt(Prefs.CHARACTER_COLOR) == 8)
        {
            level_up_gold = Strings.LEVEL_UP_GOLD[itemLevel] * 10;
        }
        else
        {
            level_up_gold = Strings.LEVEL_UP_GOLD[itemLevel];
        }

        int sale_buff = Prefs.getPrefsInt(Prefs.UP_SALE_BUFF); //레벨 업 세일 버프 값을 가져옴
        float saleGold = (float)(level_up_gold / 100f) * Strings.SALE_BUFF[sale_buff]; //할일 금액

        long levelUpGold = (long)(level_up_gold - saleGold); //원가에서 할인 금액을 빼준다.

        if (gold >= levelUpGold)
        {
            Prefs.setPrefsInt(Prefs.ITEM_LEVEL + itemIndex, (itemLevel+1)); //아이템 레벨 업
            ES2.Save(levelUpGold, Prefs.ITEM_LEVEL_UP_GOLD + itemIndex); //아이템 업에 필요한 골드 저장

            Prefs.Gold_Minus(levelUpGold);
            itemDataSetting(itemIndex);
    
            if (itemIndex == 0) //사람 추가 했을 때
            {
                Game_script.Instance.Middle_AdView(); //중간광고
                Game_script.Instance.Review_popup(true); //리뷰 팝업

                Game_script.Instance.CharacterBox_Make(); //캐릭터와 상자 런처 생성

                if (itemLevel > 19) //레벨 20부터 통계
                {
                    int color = Prefs.getPrefsInt(Prefs.CHARACTER_COLOR); //환생 레벨

                    //애드브릭스 통계 저장
                    //IgaworksUnityPluginAOS.Adbrix.retention("people", "Level_" + color + "_" + Prefs.getPrefsInt(Prefs.ITEM_LEVEL + 0));
                }
            }
            else if (itemIndex == 1 || itemIndex == 2) //발업,힘업 일 떄
            {
                Game_script.Instance.Character_Speed_Up(itemIndex);
            }

            LevelUpAni(itemIndex);//레벨업 애니메이션
        }
    }

    //슈퍼파월 캐릭터 구매
    public void super_buy(int itemIndex)
    {

        int jewel = int.Parse(Prefs.getPrefsJewel());
        int SueprLevel = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + itemIndex); //슈퍼파월 레벨

        if (jewel >= Strings.SUPER_ITME_JEWEL[SueprLevel])
        {
            Prefs.setPrefsInt(Prefs.ITEM_LEVEL + itemIndex, (SueprLevel + 1));  //슈퍼파월 레벨 업
            Prefs.setPrefsMinus_Jewel(Strings.SUPER_ITME_JEWEL[SueprLevel]); //보석 마이너스
            
            Game_script.Instance.Super_Character(false); //슈퍼파워 아이템을 구매 했을 때

            itemDataSetting(itemIndex);
            LevelUpAni(itemIndex);//레벨업 애니메이션

            //애드브릭스 통계 저장
            //IgaworksUnityPluginAOS.Adbrix.retention("Super", "Level_" + Prefs.getPrefsInt(Prefs.ITEM_LEVEL + itemIndex));
        }
    }

    //상자 레벨 업 보석으로 업그레이드
    public void Box_jewel_buy()
    {
        int jewel = int.Parse(Prefs.getPrefsJewel());
        int BoxLevel = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + 4); //박스 레벨

        int jewelIndex = (BoxLevel + 1) / 5;

        int boxBuy = Strings.BOX_ITME_JEWEL[jewelIndex]; //아이템 가격 표시

        if (jewel >= Strings.BOX_ITME_JEWEL[jewelIndex]) //박스 레벨 업
        {
            Prefs.setPrefsInt(Prefs.ITEM_LEVEL + 4, (BoxLevel + 1));  //박스 레벨 업
            Prefs.setPrefsMinus_Jewel(Strings.BOX_ITME_JEWEL[jewelIndex]); //보석 마이너스
            
            itemDataSetting(4);
            LevelUpAni(4);//레벨업 애니메이션
        }
    }

    //레벨업 애니메이션
    public void LevelUpAni(int itemIndex)
    {
        //레벨업 이미지 생성
        GameObject sp_level_Up = Instantiate(Levle_Up_Prefab) as GameObject;
        sp_level_Up.transform.parent = this.transform;
        sp_level_Up.transform.localScale = Vector3.one;

        sp_level_Up.gameObject.SetActive(true);
        sp_level_Up.gameObject.transform.localPosition = new Vector3(30f, -5f, 0f);

        TweenScript.Instance.setTweenPosition(sp_level_Up.gameObject, 0, 0.5f, new Vector3(30f, 40f, 0f));
        TweenScript.Instance.setTweenAlpha(sp_level_Up.gameObject, 0.4f, 0.5f, 1, 0);

        Game_script.Instance.ItemLevle_Up_Popup(itemIndex); //레벨 업 시 능력치 설명 팝업을 띄움.

        Main_script.Instance.SoundPlay(1, "level_up"); //레벨 업 효과음
    }
}
