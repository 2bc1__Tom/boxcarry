﻿using UnityEngine;
using System.Collections;

public class Box_script : MonoBehaviour {

    public GameObject ob_Launcher; //캐릭터와 상자를 관리하는 런쳐 프리팹 오브젝트
    public GameObject ob_Box; //박스 오브젝트
    public GameObject ob_Gold; //골드 오브젝트

    private bool mBoxFloor = true; //바닥과 충돌처리를 한번만 하기 위함.

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
       
    }

    public void OnTriggerEnter2D(Collider2D other)
    {

        //오른쪽 빌등 벽에 충돌 했을 때 상자는 삭제 시키고 돈은 + 시킴
        if (other.name.Contains("sp_door_right"))
        {
            ob_Launcher.GetComponent<Launcher_script>().ObjectDelete(ob_Box); //박스 삭제
            Game_script.Instance.Gold_Plus(0); //골드가 + 시켜줌

        }else if (other.name.Contains("sp_throw")) { //상자를 던져 오른쪽 빌딩에 충돌 했을 때 상자는 삭제 시키고 돈은 + 시킴

            if (ob_Launcher.name.Contains("Launcher_prefab")) //밑에서 나르는 상자가 충돌 했을 때
            {
                  ob_Launcher.GetComponent<Launcher_script>().BoxThrowEnd(); //던진 박스가 오른쪽 건물과 충돌하고 나면 상자 날아가는 애니메이션을 종료 시킴
                  ob_Launcher.GetComponent<Launcher_script>().ObjectDelete(ob_Box); //박스 삭제
                
                  Game_script.Instance.Gold_Plus(2); //골드가 + 시켜줌
            }
            else if (ob_Launcher.name.Contains("super_Launcher"))//옥상에서 던진 박스가 충돌 했을 때
            {
                ob_Launcher.GetComponent<super_script>().BoxThrowEnd(ob_Box); //박스 삭제
                Game_script.Instance.Gold_Plus(1); //골드가 + 시켜줌
            }

        } else if (other.name.Contains("op_constructor")) //왼쪽 건물 입구쪽에 op_constructor 오브젝트와 충돌하는 순간부터 던지기 확률 계산함
        {
            ob_Launcher.GetComponent<Launcher_script>().BoxThrow_Random(); //상자 던지기 확률 계산 시작.

        }else if (other.name.Contains("sp_Floor")) //상자가 바닥에 충돌 했을 때
        {

            if (mBoxFloor) //바닥과 충돌처리를 한번만 하기 위함.
            {
                mBoxFloor = false;
                Box_Floor(); //상자가 바닥에 충돌 했을 때
            }
        }
    }

    //상자가 바닥에 충돌 했을 때
    public void Box_Floor() {

        string objectName = ob_Box.name;

        int boxIndex = int.Parse(objectName.Substring(objectName.Length - 1, 1));  //몇 번째 상자인지에 대한 값을 가져 온다.

        GameObject ob_rauncher = ob_Box.transform.parent.gameObject; //상자가 속해 있는 부모 오브젝트를 가져온다.

        ob_rauncher.transform.GetComponent<Rigidbody2D>().isKinematic = true; //움직이지 않게 고정 시킴

        if (ob_Box.transform.GetComponent<Rigidbody2D>() != null)
        {
            ob_Box.transform.GetComponent<Rigidbody2D>().isKinematic = true; //움직이지 않게 고정 시킴
        }

        ob_Box.transform.GetComponent<UISprite>().spriteName = ImgDatas.IMG_BOX_BROKEN[Prefs.getPrefsInt(Prefs.SPECIES_TYPE)][boxIndex]; //상자가 깨진 이미지로 바꿔준다.
        StartCoroutine(BoxFloor_delete()); //상자가 꺠진 뒤 0.5초 뒤에 상자를 삭제 시킴

        if (ob_Box.name.Contains("box_prefab_h")) //이벤트 상자 일 때
        {
            ob_Box.name = "delete_box_event"; //깨지는 상자가 어느 것인지 구분하기 위해 상자의 이름을 바꿔준다.
        }
        else
        {
            ob_Box.name = "delete_box"; //깨지는 상자가 어느 것인지 구분하기 위해 상자의 이름을 바꿔준다.
        }

        Game_script.Instance.BoxFloor(ob_Box); //상자 드래그 상태를 멈추기 위함

        Main_script.Instance.SoundPlay(1, "box_delete"); //상자 깨짐 효과음

        //이벤트 상자가 깨졌을 때 골드나 보석을 줌.
        if (boxIndex == 7)
        {
            int event_index = UnityEngine.Random.Range(0, 20);

            if (event_index == 7) //보석 1개 선물
            {
                Game_script.Instance.Event_Item_Ani(ob_Box, 0);
            }
            else //골드 x100 선물
            {
                Game_script.Instance.Event_Item_Ani(ob_Box , 1);
            }
        }
    }

    //상자가 꺠진 뒤 0.5초 뒤에 상자를 삭제 시킴
    IEnumerator BoxFloor_delete()
    {
         yield return new WaitForSeconds(0.5f);

         GameObject ob_rauncher = ob_Box.transform.parent.gameObject;
         ob_rauncher.GetComponent<Launcher_script>().ObjectDelete(ob_Box); //상자 삭제
    }
}
