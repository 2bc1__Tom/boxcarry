﻿using UnityEngine;
using System.Collections;


public class Launcher_script : MonoBehaviour
{

    public Camera mCamera;

    public GameObject ob_Launcher; //캐릭터와 상자를 관리하는 런쳐 프리팹 오브젝트
    public GameObject ob_Gold; //골드를 담는 오브젝트

    public GameObject character_prefab; //캐릭터 프리팹
    public GameObject box_prefab; //상자 프리팹

    private GameObject sp_character; //생성 되는 캐릭터 오브젝트
    private GameObject sp_Box; //생성 되는 상자 오브젝트

    private GameObject drag_box; //드래그 중인 상자 오브젝트

    public int mCharacterIndex = 0; //어떤 캐릭터가 나올지에 대한 인덱스 값
    public int mCharacterPosesIndex = 0; //어떤 프즈인지에 대한 인덱스 값(앞으로 나르기 ,뒤로 나르기 , 수레 끌기)
    public int mBoxIndex = 0; //어떤 상자인지에 대한 인덱스 값

    private int throwDelay = 0; //상자 던지는 전 딜레이

    private int mThrow_random = 0; //상자 던지기에 대한 확률 값
    
    private int mFramIndex = 0; //캐릭터 프레임 인덱스 값
    private int mRun_FramIndex = 0; //캐릭터 걷는 프레임 인덱스 값(드래곤,패스트푸드 종족)

    private float mCarrySpeed = 0f; //상자를 나르는 속도 값
    private float mBackRun_speed = 0f; //뒤로 되돌아가는 속도 값

    private bool mCarry = true; //캐릭터와 상자가 오른쪽 건물로 이동 상태
    private bool mBackRun = false; //뒤로 되돌아가는 상태

    private bool mBoxThrow = false; //상자 던지기 여부
    private bool mBoxThrow_fly = false; //던진 후 상자가 날아가고 있는 상태

    private bool mBoxThrow_Random = true; //상자 던지기 확률 처음 1번만 실행.
    private bool mBoxDrag = false; //상자 드래그 여부

    public bool mFeverTime = false; //피버타임인지 아닌지 구분
    public bool mEvent_Box = false; //이벤트 상자가 등장 했는지 구분

    public bool mEventBox_Delete = false; //이벤트 상자 캐릭터 삭제 여부.

    private Vector3 startPos; //상자 던지는 위치
    private Vector3 destPos;  //상자가 떨어지는 위치
    private Vector3 MousePos; //상자 드래그 위치 값

    private float timer; //상자를 던졌을 때 날아가는 속도
    private float vx;
    private float vy;
    private float vz;

	void Start () {
    }

    public void Luncher_Start()
    {
        //캐릭터와 상자 생성
        CharacterBox_Make();
        Character_Carry();
    }

	// Update is called once per frame
	void Update () {

        if (mCarry)
        {
            if (ob_Launcher != null)
            {
                if (mEvent_Box) //이벤트 상자 일 떄 스피드 줄여줌
                {
                   ob_Launcher.GetComponent<Rigidbody2D>().velocity = new Vector3(0.07f, 0f, 0f);
                }
            }
        }

        //상자 던짐
        if (mBoxThrow_fly)
        {
            throwDelay++; //상자 던지기전 잠시 딜레이를 주기 위함.

            if (throwDelay > 3)
            {
                timer += Time.deltaTime + 0.02f; //상자가 날아가는 속도

                //상자 날아가는 포물선 위치 계산
                float sx = startPos.x + vx * timer;
                float sy = startPos.y + vy * timer - 0.2f * 120.8f * timer * timer;

                if (sp_Box != null)
                {
                    sp_Box.transform.localPosition = new Vector3(sx, sy, 0);
                    sp_Box.transform.Rotate(0, 0, -1);
                }
            }
        }
	}

    //캐릭터가 오른쪽 건물로 상자를 들고 이동하는 상태
    public void Character_Carry()
    {
          if (ob_Launcher != null)
          {
              if (mEvent_Box) //이벤트 상자 일 떄 스피드 줄여줌
              {
                  ob_Launcher.GetComponent<Rigidbody2D>().velocity = new Vector3(0.07f, 0f, 0f);
              }
              else //아닐 때
              {
                  int itemPowerLevel = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + 2) + 1; //힘업 레벨
                  mCarrySpeed = Strings.ITME_LEVEL_UP[2][itemPowerLevel]; //상자를 나르는 속도

                  ob_Launcher.GetComponent<Rigidbody2D>().velocity = new Vector3(mCarrySpeed, 0f, 0f);
              }
          }
    }

    //캐릭터가 왼쪽 건물로 다시 뛰어가는 상태
    public void Character_Back()
    {
          if (sp_character != null)
          {
              if (mFeverTime) //피버 타임 일 땐 빠르게 뛰어 들어감
              {
                  if (sp_character.GetComponent<Rigidbody2D>() != null)
                  {
                    sp_character.GetComponent<Rigidbody2D>().velocity = new Vector3(-0.68f, 0f, 0f);
                  }
              }
              else
              {
                  int itemSpeedLevel = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + 1); //발업 레벨
                  mBackRun_speed = Strings.ITME_LEVEL_UP[1][itemSpeedLevel]; //상자를 나르는 속도

                  if (sp_character.GetComponent<Rigidbody2D>() != null)
                  {
                    sp_character.GetComponent<Rigidbody2D>().velocity = new Vector3(mBackRun_speed, 0f, 0f);
                  }
              }
          }
    }

    public void CharacterBox_Make()
    {
        // ====== 캐릭터 ======

        //캐릭터 생성
        sp_character = Instantiate(character_prefab) as GameObject;
        sp_character.transform.parent = ob_Launcher.transform;
        sp_character.transform.localScale = Vector3.one;

        if (!mEvent_Box) //이벤트 캐릭터 상자가 아닐 떄
        {
            float characterY = 0;

            if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 0) //인간 종족
            {
                characterY = CoordinatesData.CHARACTER_Y_COORDINATES[mCharacterIndex, mCharacterPosesIndex]; //캐릭터 y 좌표 값
            }
            else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 1) //강아지 종족
            {
                characterY = CoordinatesData_dog.CHARACTER_Y_COORDINATES[mCharacterIndex, mCharacterPosesIndex]; //캐릭터 y 좌표 값
            }
            else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 2) //고양이 종족
            {
                characterY = CoordinatesData_cat.CHARACTER_Y_COORDINATES[mCharacterIndex, mCharacterPosesIndex]; //캐릭터 y 좌표 값
            }
            else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 3) //드래곤 종족
            {
                characterY = CoordinatesData_dragon.CHARACTER_Y_COORDINATES[mCharacterIndex, mCharacterPosesIndex]; //캐릭터 y 좌표 값
            }
            else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 4) //패스트푸드 종족
            {
                characterY = CoordinatesData_food.CHARACTER_Y_COORDINATES[mCharacterIndex, mCharacterPosesIndex]; //캐릭터 y 좌표 값
            }
            
            int ReincarnationLevle = Prefs.getPrefsInt(Prefs.CHARACTER_COLOR); //환생 레벨 

            sp_character.transform.localPosition = new Vector3(-357f, characterY, 0f); //캐릭터 위치 지정

            //인간,멍멍이,냥냥이 종족 일 시 환생 레벨에 따라 색상을 변경 해 줌.
            if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) < 3)
            {
                //컬러 색상 값
                float Color_R = Strings.CHARACTER_COLOR[ReincarnationLevle, 0] / 256f;
                float Color_G = Strings.CHARACTER_COLOR[ReincarnationLevle, 1] / 256f;
                float Color_B = Strings.CHARACTER_COLOR[ReincarnationLevle, 2] / 256f;

                //인간 종족을 제외한 다른 종족이 환생 레벨 1일 떄 색상 흰색으로 해줌
                if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) != 0 && ReincarnationLevle == 0)
                {
                    sp_character.transform.GetComponent<UISprite>().color = new Color(255, 255, 255, 1f); //환생 레벨에 따라 캐릭터 색상 변경
                }
                else
                {
                    sp_character.transform.GetComponent<UISprite>().color = new Color(Color_R, Color_G, Color_B, 1f); //환생 레벨에 따라 캐릭터 색상 변경
                }
            }
        }

        Character_Script characterScript = sp_character.GetComponent<Character_Script>();

        characterScript.ob_Launcher = ob_Launcher; //상자에서 충돌되는 값들을 런처 스크립트로 리턴 받기 위해 해당 런처 오브젝트 값을 넘겨준다.
        characterScript.ob_character = sp_character; //캐릭터 오브젝트 값을 넘겨준다.

        mFramIndex = 0; //프레임 초기화
        StartCoroutine(frame_Motion()); //움직이는 모션 시작

        //======= 상자 =======

        //상자 생성
        sp_Box = Instantiate(box_prefab) as GameObject;
        sp_Box.transform.parent = ob_Launcher.transform;
        sp_Box.transform.localScale = Vector3.one;

        int boxIndex = 0;

        //여자 캐릭터가 수레를 끄는 모습 일 떄
        if (mCharacterIndex == 2 && mCharacterPosesIndex == 2)
        {
            boxIndex = mBoxIndex - 2; //좌표 값을 맞추기 위해 상자 인덱스 값을 -2 해준다.
        }
        else
        {
            boxIndex = mBoxIndex;
        }

        Box_script BoxScript = null;

        if (mEvent_Box) //이벤트 상자 일 때
        {
            sp_Box.transform.localPosition = new Vector3(-356f, -298f, -0.1f); //상자 위치 지정
            BoxScript = sp_Box.GetComponentInChildren<Box_script>();
        }
        else
        {
            float mX = 0;
            float mY = 0;
            float rotation = 0;

            if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 0) //인간 종족
            {
                mX = CoordinatesData.BOX_COORDINATES[mCharacterIndex, mCharacterPosesIndex, boxIndex, 0]; //상자 x 좌표 값
                mY = CoordinatesData.BOX_COORDINATES[mCharacterIndex, mCharacterPosesIndex, boxIndex, 1]; //상자 y 좌표 값
                rotation = CoordinatesData.BOX_COORDINATES[mCharacterIndex, mCharacterPosesIndex, boxIndex, 2]; //상자 회전 각도
            }
            else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 1) //강아지 종족
            {
                mX = CoordinatesData_dog.BOX_COORDINATES[mCharacterIndex, mCharacterPosesIndex, boxIndex, 0]; //상자 x 좌표 값
                mY = CoordinatesData_dog.BOX_COORDINATES[mCharacterIndex, mCharacterPosesIndex, boxIndex, 1]; //상자 y 좌표 값
                rotation = CoordinatesData_dog.BOX_COORDINATES[mCharacterIndex, mCharacterPosesIndex, boxIndex, 2]; //상자 회전 각도
            }
            else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 2) //고양이 종족
            {
                mX = CoordinatesData_cat.BOX_COORDINATES[mCharacterIndex, mCharacterPosesIndex, boxIndex, 0]; //상자 x 좌표 값
                mY = CoordinatesData_cat.BOX_COORDINATES[mCharacterIndex, mCharacterPosesIndex, boxIndex, 1]; //상자 y 좌표 값
                rotation = CoordinatesData_cat.BOX_COORDINATES[mCharacterIndex, mCharacterPosesIndex, boxIndex, 2]; //상자 회전 각도
            }
            else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 3) //드래곤 종족
            {
                mX = CoordinatesData_dragon.BOX_COORDINATES[mCharacterIndex, mCharacterPosesIndex, boxIndex, 0]; //상자 x 좌표 값
                mY = CoordinatesData_dragon.BOX_COORDINATES[mCharacterIndex, mCharacterPosesIndex, boxIndex, 1]; //상자 y 좌표 값
                rotation = CoordinatesData_dragon.BOX_COORDINATES[mCharacterIndex, mCharacterPosesIndex, boxIndex, 2]; //상자 회전 각도
            }
            else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 4) //패스트푸드 종족
            {
                mX = CoordinatesData_food.BOX_COORDINATES[mCharacterIndex, mCharacterPosesIndex, boxIndex, 0]; //상자 x 좌표 값
                mY = CoordinatesData_food.BOX_COORDINATES[mCharacterIndex, mCharacterPosesIndex, boxIndex, 1]; //상자 y 좌표 값
                rotation = CoordinatesData_food.BOX_COORDINATES[mCharacterIndex, mCharacterPosesIndex, boxIndex, 2]; //상자 회전 각도
            }

            sp_Box.transform.localPosition = new Vector3(mX, mY, 0);
            sp_Box.transform.Rotate(0, 0, rotation);

            BoxScript = sp_Box.GetComponent<Box_script>();
        }

        //sp_Box.name = ImgDatas.BOX_ATLAS[mBoxIndex] + mBoxIndex; //상자가 바닥에 충돌 할 때 충돌한 상자가 어떤 상자인지 구분하기 위해 이름 뒤에 숫자를 붙임
        sp_Box.name = "box_prefab_" + mBoxIndex; //상자가 바닥에 충돌 할 때 충돌한 상자가 어떤 상자인지 구분하기 위해 이름 뒤에 숫자를 붙임

        BoxScript.ob_Launcher = ob_Launcher; //상자에서 충돌되는 값들을 런처 스크립트로 리턴 받기 위해 해당 런처 오브젝트 값을 넘겨준다.
        BoxScript.ob_Box = sp_Box; //상자 오브젝트 값을 넘겨준다.
        BoxScript.ob_Gold = ob_Gold; //골드 오브젝트 값을 넘겨준다.

        //sp_Box.transform.GetComponent<UISprite>().MakePixelPerfect(); //이미지의 원래 사이즈로 맞춰 줌.
    }

    //1초에 한번 씩 상태를 체크해 랜덤 확률로 상자를 집어 던짐.
    IEnumerator boxThrowDeley()
    {
        while (true)
        {
            if (sp_Box == null) yield break;

            Random random = new Random();

            int throwLevel = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + 3); //던지기 레벨
            mThrow_random = (int)Strings.ITME_LEVEL_UP[3][throwLevel]; //던지기 랜덤 확률

            int randomThrow = UnityEngine.Random.Range(0, 10000); //랜덤 값을 구함

            //상자 드래그 상태가 아닐 떄
            if (!mBoxDrag)
            {
                //상자를 나르고 있는 상태일 때 던지기 값이 나면 상자를 던짐
                if (mCarry && (randomThrow <= mThrow_random))
                {
                    Box_Throw();
                }
            }

            yield return new WaitForSeconds(1f);
        }
    }

    //상자 던지기
    public void Box_Throw()
    {
        if (sp_Box != null)
        {
            mCarry = false; //상자를 나르던 상태 OFF

            mBoxThrow_fly = true; //던진 상자가 날아가고 있는 상태
            mBoxThrow = true; // true면 던짐

            mFramIndex = 0; //프레임 초기화

            ob_Launcher.GetComponent<Rigidbody2D>().velocity = new Vector3(0f, 0f, 0f); //캐릭터 움직임 멈춤

            startPos = sp_Box.transform.localPosition; //상자 던지는 위치
            destPos = new Vector3(312, -221, 0); //상자가 떨어지는 위치

            vx = (destPos.x - startPos.x) / 4f;
            vy = (destPos.y - startPos.y + 2 * 90.8f) / 2f;

            throwDelay = 0; //던지는 딜레이 초기화
        }
    }

    //캐릭터가 다시 뒤로 되돌아가는 액션
    public void characterBack_Run()
    {

        if (mEvent_Box) return; //이벤트 상자 일 떈 뒤로 돌아가는 액션 없음.

        //뒤로 되돌아가고 있는 상태가 아니라면
        if (!mBackRun && sp_character != null)
        {
            mFramIndex = 0; //프레임 초기화

            mBoxThrow = false; //상자 던진 상태 OFF
            mCarry = false; //상자를 나르던 상태 OFF
            mBackRun = true; //뒤로 되돌아가는 상태 ON

            sp_character.AddComponent<Rigidbody2D>(); //물리엔진 추가
            sp_character.GetComponent<Rigidbody2D>().gravityScale = 0f; //중력 값을 0으로 한다.

            sp_character.AddComponent<BoxCollider2D>(); //박스 콜라이더 추가
            sp_character.GetComponent<BoxCollider2D>().isTrigger = true;
            sp_character.GetComponent<BoxCollider2D>().size = new Vector3(33,33); //박스 콜라이더 사이즈 조절

            //몇 번째 캐릭터인지에 대한 값을 가져 온다.
            int characterIndex = mCharacterIndex + 1;

            if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 0) //인간 종족
            {
                //캐릭터 y좌표 값 설정
                sp_character.transform.localPosition = new Vector3(sp_character.transform.localPosition.x, CoordinatesData.CHARACTER_RUN_Y[mCharacterIndex], 0f);
            }
            else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 1) //강아지 종족
            {
                //캐릭터 y좌표 값 설정
                sp_character.transform.localPosition = new Vector3(sp_character.transform.localPosition.x, CoordinatesData_dog.CHARACTER_RUN_Y[mCharacterIndex], 0f);
            }
            else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 2) //고양이 종족
            {
                //캐릭터 y좌표 값 설정
                sp_character.transform.localPosition = new Vector3(sp_character.transform.localPosition.x, CoordinatesData_cat.CHARACTER_RUN_Y[mCharacterIndex], 0f);
            }
            else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 3) //드래곤 종족
            {
                //캐릭터 y좌표 값 설정
                sp_character.transform.localPosition = new Vector3(sp_character.transform.localPosition.x, CoordinatesData_dragon.CHARACTER_RUN_Y[mCharacterIndex], 0f);
            }
            else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 4) //패스트푸드 종족
            {
                //캐릭터 y좌표 값 설정
                sp_character.transform.localPosition = new Vector3(sp_character.transform.localPosition.x, CoordinatesData_food.CHARACTER_RUN_Y[mCharacterIndex], 0f);
            }

            //돌아가는 캐릭터가 상자 나르는 캐릭터 보다 뒤로 갈 수 있게 Depth 값 조정
            sp_character.GetComponent<UISprite>().depth = 4;

            //돌아가는 캐릭터에 살짝 투명도를 준다.
            TweenScript.Instance.setTweenAlpha(sp_character, 0, 0.1f, 1f, 0.5f);

            Character_Back();
        }
    }

    //클릭한 범위에 상자가 있는지 체크해서 있다면 상자에 대한 오브젝트 값을 가져온다.
    private GameObject GetClickedObject()
    {

        GameObject target = null;

        Vector2 touchPosition = mCamera.ScreenToWorldPoint(Input.mousePosition);
        Ray2D ray = new Ray2D(touchPosition, Vector2.zero);
        RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction);

        if (hit.collider != null)
        {
            target = hit.collider.gameObject;
        }

        return target;
    }

    //움직이는 모션 프레임
    IEnumerator frame_Motion()
    {

        //이벤트 상자가 아닐 떄에만 모션 작동
        if (!mEvent_Box)
        {
            mFramIndex = 0;
            int poses = 2;

            UISprite character_sp = sp_character.transform.GetComponent<UISprite>();

            int species_type = Prefs.getPrefsInt(Prefs.SPECIES_TYPE); //현재 종족이 어떤건지에 대한 값

            while (true)
            {
                //다시 시작
                if (mFramIndex == 4) mFramIndex = 0;

                if (mRun_FramIndex == 6) mRun_FramIndex = 0;

                //여자 캐릭터 일 때 수레끄는 액션이 하나 더 있어서 다른 캐릭터와 설정을 다르게 함.
                if (mCharacterIndex == 2) poses = 3;

                mFramIndex++;
                mRun_FramIndex++;

                if (mBackRun) //뒤로 되돌아 갈 떄
                {
                    if (sp_character != null)
                    {

                        //드래곤 종족일 땐 프레임이 6개
                        if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 3)
                        {
                            character_sp.spriteName = ImgDatas.CHARACTER_FRAME[species_type][mCharacterIndex][poses + 1] + mRun_FramIndex;
                        }
                        else //나머지 종족 프레임 4개
                        {
                            character_sp.spriteName = ImgDatas.CHARACTER_FRAME[species_type][mCharacterIndex][poses + 1] + mFramIndex;
                        }
                    }

                }
                else if (mBoxThrow) //상자 던질 때
                {
                    if (sp_character != null)
                    {
                        character_sp.spriteName = ImgDatas.CHARACTER_FRAME[species_type][mCharacterIndex][poses] + mFramIndex;
                    }

                    if (mFramIndex == 4) //상자를 던진 모션이 끝난 후
                    {
                        characterBack_Run(); //캐릭터가 다시 뒤로 되돌아가는 액션
                    }

                }
                else if (mCarry) //상자 나를 때
                {
                    if (sp_character != null)
                    {
                        //드래곤 종족일 땐 프레임이 6개//드래곤 종족일 땐 프레임이 6개
                        if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 3)
                        {
                            character_sp.spriteName = ImgDatas.CHARACTER_FRAME[species_type][mCharacterIndex][mCharacterPosesIndex] + mRun_FramIndex;
                        }
                        else //나머지 종족 프레임 4개
                        {
                            character_sp.spriteName = ImgDatas.CHARACTER_FRAME[species_type][mCharacterIndex][mCharacterPosesIndex] + mFramIndex;
                        }
                    }
                }

                if (sp_character != null)
                {
                    character_sp.MakePixelPerfect(); //이미지의 원래 사이즈로 맞춰 줌.
                }

                yield return new WaitForSeconds(0.1f);
            }
        }
    }

    //======= 다른 스크립트에서 호출하는 함수들 =======

    //상자 던지기 확률 계산 시작. (박스 스크립트에서 호출)
    public void BoxThrow_Random()
    {
        if (mEvent_Box) return; //이벤트 상자 일 떈 던지기 안되도록 막아둠.

        //1초에 한번 씩 상자 던지는 확률을 계산한다.
        if (mBoxThrow_Random)
        {
            mBoxThrow_Random = false;
            StartCoroutine(boxThrowDeley());
        }
    }

    //상자를 나르던 상태를 멈춤
    public void BoxCarryEnd()
    {
        characterBack_Run(); //캐릭터가 다시 뒤로 되돌아가는 액션
    }

    //던진 상자가 오른쪽 건물과 충돌하고 나면 상자 날아가는 애니메이션을 종료 시킴
    public void BoxThrowEnd()
    {
        //상자 날아가는 애니메이션 OFF
        mBoxThrow_fly = false;
    }

    //이벤트 캐릭터에 BoxCollider 걸어줌(오른쪽 건물과 충돌처리를 위함)
    public void EventCharacter_collider()
    {
        sp_character.AddComponent<BoxCollider2D>(); //박스 콜라이더 추가
        sp_character.GetComponent<BoxCollider2D>().isTrigger = true;
        sp_character.GetComponent<BoxCollider2D>().size = new Vector3(33, 33); //박스 콜라이더 사이즈 조절
    }

    //오브젝트 삭제 함수
    public void ObjectDelete(GameObject ob_game)
    {

        if (ob_game.name.Contains("box_prefab"))  //삭제하는 값이 상자면
        {
            mCarry = false; //오른쪽 건물로 이동 상태 멈춤
            ob_Launcher.GetComponent<Rigidbody2D>().velocity = new Vector3(0f, 0f, 0f); //오른쪽 건물로 이동 상태 멈춤

            if (ob_game.name.Contains("box_prefab_h")) //이벤트 상자 일 때 캐릭터도 같이 없애준다.
            {
                mEventBox_Delete = true;

                Destroy(ob_game); //오브젝트 삭제
                sp_character = null;
            }
            else //일반 상자 일 떄
            {
                characterBack_Run(); //캐릭터가 다시 뒤로 되돌아가는 액션
            }

            Destroy(ob_game); //오브젝트 삭제
            sp_Box = null;

        }else if (ob_game.name.Contains("character_prefab")){ //삭제하는 값이 캐릭터면

            if (mBackRun) //뒤로 되돌아가 왼쪽 건물과 충돌 했을 때
            {
                mCarry = false; //오른쪽 건물로 이동 상태 멈춤
                mBackRun = false; //뒤로 되돌아가는 상태 멈춤
                
                Destroy(ob_game); //오브젝트 삭제
                sp_character = null;
            }
        }
        else if (ob_game.name.Contains("delete_box")) //바닥에 충돌한 박스 일 떄
        {
            Destroy(ob_game); //오브젝트 삭제
            sp_Box = null;

            if (ob_game.name.Contains("delete_box_event")) //이벤트 상자 일 때
            {
                mEventBox_Delete = true;
            }
        }
        else if (ob_game.name.Contains("delete_event_character")) //이벤트 상자 캐릭터 일 떄
        {
            mEventBox_Delete = true;

            Destroy(ob_game); //오브젝트 삭제
            sp_character = null;
        }

        //캐릭터와 상자 오브젝트가 없다면 상자와 캐릭터를 관리하던 런처 오브젝트를 삭제 시킴
        if (sp_Box == null && sp_character == null)
        {
            if (mEventBox_Delete) //이벤트 상자 캐릭터가 없어지면
            {
                Game_script.Instance.mEvent_Box = false;
            }

            Destroy(ob_Launcher);
            Game_script.Instance.New_Launcher(); //캐릭터와 상자 런처 생성
        }
    }
}
