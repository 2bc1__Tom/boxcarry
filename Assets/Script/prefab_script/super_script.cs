﻿using UnityEngine;
using System.Collections;

public class super_script : MonoBehaviour {

    public GameObject super_Launcher;

    public GameObject[] characterPrefab; //캐릭터 프리팹
    public GameObject[] boxPrefab; //캐릭터 프리팹

    private GameObject sp_character;
    private GameObject sp_Box;
    
    private int throwDelay = 0; //상자 던지기전 잠시 딜레이를 주기 위함.
    private int mCharacterIndex = 0; //어떤 캐릭터 인지에 대한 인덱스 값

    private Vector3 startPos; //상자 던지는 위치
    private Vector3 destPos;  //상자가 떨어지는 위치
    private Vector3 MousePos; //상자 드래그 위치 값

    private float timer; //상자를 던졌을 때 날아가는 속도
    private float vx;
    private float vy;
    private float vz;

    private bool mBoxThrow = false;
    private bool mFeverTime = false; //피버타임인지 아닌지 구분

    // Use this for initialization
	void Start () {

        Character_Make(); //캐릭터 생성

        Box_Make(); //상자 생성
        Box_Throw(); //박스 던지기
    }
	
	// Update is called once per frame
	void Update () { 

            //상자 던짐
            if (mBoxThrow)
            {
                throwDelay++; //상자 던지기전 잠시 딜레이를 주기 위함.

                if (throwDelay > 3)
                {
                    timer += Time.deltaTime + 0.025f; //상자가 날아가는 속도

                    //상자 날아가는 포물선 위치 계산
                    float sx = startPos.x + vx * timer;
                    float sy = startPos.y + vy * timer - 0.2f * 120.8f * timer * timer;

                    if (sp_Box != null)
                    {
                        sp_Box.transform.localPosition = new Vector3(sx, sy, 0);
                        sp_Box.transform.Rotate(0, 0, -1);
                    }
                }
            }
	}

    //캐릭터 생성
    public void Character_Make()
    {

        mCharacterIndex = UnityEngine.Random.Range(0, 3);//어떤 캐릭터인지에 대한 인덱스 값

        float characterY = 0;
        float characterPosition = 0;

        if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 0) //인간 종족
        {
            characterY = CoordinatesData.SUPER_CHARACTER_Y[mCharacterIndex]; //캐릭터 y 좌표 값
            characterPosition = CoordinatesData.SUPER_CHARACTER_Y[mCharacterIndex];
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 1) //강아지 종족
        {
            characterY = CoordinatesData_dog.SUPER_CHARACTER_Y[mCharacterIndex]; //캐릭터 y 좌표 값
            characterPosition = CoordinatesData_dog.SUPER_CHARACTER_Y[mCharacterIndex];
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 2) //고양이 종족
        {
            characterY = CoordinatesData_cat.SUPER_CHARACTER_Y[mCharacterIndex]; //캐릭터 y 좌표 값
            characterPosition = CoordinatesData_cat.SUPER_CHARACTER_Y[mCharacterIndex];
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 3) //드래곤 종족
        {
            characterY = CoordinatesData_dragon.SUPER_CHARACTER_Y[mCharacterIndex]; //캐릭터 y 좌표 값
            characterPosition = CoordinatesData_dragon.SUPER_CHARACTER_Y[mCharacterIndex];
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 4) //패스트푸드 종족
        {
            characterY = CoordinatesData_food.SUPER_CHARACTER_Y[mCharacterIndex]; //캐릭터 y 좌표 값
            characterPosition = CoordinatesData_food.SUPER_CHARACTER_Y[mCharacterIndex];
        }

        GameObject CharacterPrefab = characterPrefab[mCharacterIndex];

        //캐릭터 생성
        sp_character = Instantiate(CharacterPrefab) as GameObject;
        sp_character.transform.parent = super_Launcher.transform;
        sp_character.transform.localScale = Vector3.one;
        sp_character.transform.localPosition = new Vector3(characterPosition, characterY, 0f);

        if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 1 || Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 2) //강아니지 고양이 종족일 때 환생 색상으로 바꿔줌.
        {
            int ReincarnationLevle = Prefs.getPrefsInt(Prefs.CHARACTER_COLOR); //환생 레벨 

            //컬러 색상 값
            float Color_R = Strings.CHARACTER_COLOR[ReincarnationLevle, 0] / 256f;
            float Color_G = Strings.CHARACTER_COLOR[ReincarnationLevle, 1] / 256f;
            float Color_B = Strings.CHARACTER_COLOR[ReincarnationLevle, 2] / 256f;

            if (Prefs.getPrefsInt(Prefs.CHARACTER_COLOR) == 0) //환생 레벨이 0이면 흰색으로
            {
                sp_character.transform.GetComponent<UISprite>().color = new Color(255, 255, 255, 1f); //환생 레벨에 따라 캐릭터 색상 변경
            }
            else
            {
                sp_character.transform.GetComponent<UISprite>().color = new Color(Color_R, Color_G, Color_B, 1f); //환생 레벨에 따라 캐릭터 색상 변경
            }
        }

        TweenScript.Instance.setTweenAlpha(sp_character, 0, 0.1f, 1f, 0.8f); //캐릭터에 살짝 투명도를 준다.
    }

    //상자 생성
    public void Box_Make()
    {

        if (mFeverTime) return; //피버타임 일때 상자를 던지지 않음.

        //박스가 날아가는 좌표 값 초기화
        timer = 0;
        vx = 0;
        vy = 0;
        vz = 0;

        int species_type = Prefs.getPrefsInt(Prefs.SPECIES_TYPE); //현재 종족 값

        if (mCharacterIndex == 2) //여자 캐릭터 일 때 수레끄는 액션이 하나 더 있어서 다른 캐릭터와 설정을 다르게 함.
        {
            sp_character.transform.GetComponent<UISprite>().spriteName = ImgDatas.CHARACTER_FRAME[species_type][mCharacterIndex][3] + 1; //던지기 전 모습 이미지
        }
        else
        {
            sp_character.transform.GetComponent<UISprite>().spriteName = ImgDatas.CHARACTER_FRAME[species_type][mCharacterIndex][2] + 1; //던지기 전 모습 이미지
        }

        int mBoxIndex = UnityEngine.Random.Range(0, 3); //어떤 상자인지에 대한 인덱스 값

        float mX = 0;
        float mY = 0;
       
        if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 0) //인간 종족
        {
            mX = CoordinatesData.SUPER_COORDINATES[mCharacterIndex, mBoxIndex, 0]; //상자 x 좌표 값
            mY = CoordinatesData.SUPER_COORDINATES[mCharacterIndex, mBoxIndex, 1]; //상자 y 좌표 값
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 1) //강아지 종족
        {
            mX = CoordinatesData_dog.SUPER_COORDINATES[mCharacterIndex, mBoxIndex, 0]; //상자 x 좌표 값
            mY = CoordinatesData_dog.SUPER_COORDINATES[mCharacterIndex, mBoxIndex, 1]; //상자 y 좌표 값
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 2) //고양이 종족
        {
            mX = CoordinatesData_cat.SUPER_COORDINATES[mCharacterIndex, mBoxIndex, 0]; //상자 x 좌표 값
            mY = CoordinatesData_cat.SUPER_COORDINATES[mCharacterIndex, mBoxIndex, 1]; //상자 y 좌표 값
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 3) //드래곤 종족
        {
            mX = CoordinatesData_dragon.SUPER_COORDINATES[mCharacterIndex, mBoxIndex, 0]; //상자 x 좌표 값
            mY = CoordinatesData_dragon.SUPER_COORDINATES[mCharacterIndex, mBoxIndex, 1]; //상자 y 좌표 값
        }
        else if (Prefs.getPrefsInt(Prefs.SPECIES_TYPE) == 4) //푸드 종족
        {
            mX = CoordinatesData_food.SUPER_COORDINATES[mCharacterIndex, mBoxIndex, 0]; //상자 x 좌표 값
            mY = CoordinatesData_food.SUPER_COORDINATES[mCharacterIndex, mBoxIndex, 1]; //상자 y 좌표 값
        }

        GameObject BoxPrefab = boxPrefab[mBoxIndex];



        //상자 생성
        sp_Box = Instantiate(BoxPrefab) as GameObject;
        sp_Box.transform.parent = super_Launcher.transform;
        sp_Box.transform.localScale = Vector3.one;
        sp_Box.transform.localPosition = new Vector3(mX, mY, 0);

        sp_Box.name = "super_box"; //슈퍼파월 캐릭터가 던지는 상자는 드래그가 되지 않게 하기 위해 이름을 바꿔 줌.
        sp_Box.transform.GetComponent<UISprite>().MakePixelPerfect(); //이미지의 원래 사이즈로 맞춰 줌.

        Box_script box_script = sp_Box.GetComponent<Box_script>();

        box_script.ob_Launcher = super_Launcher; //상자에서 충돌되는 값들을 런처 스크립트로 리턴 받기 위해 해당 런처 오브젝트 값을 넘겨준다.
        box_script.ob_Box = sp_Box; //상자 오브젝트 값을 넘겨준다.
    }

    //상자 던지기
    public void Box_Throw()
    {
        if (mFeverTime) return; //피버타임 일때 상자를 던지지 않음.

        startPos = sp_Box.transform.localPosition; //상자 던지는 위치
        destPos = new Vector3(622, -021, 0); //상자가 떨어지는 위치

        vx = (destPos.x - startPos.x) / 4f;
        vy = (destPos.y - startPos.y + 2 * 90.8f) / 2f;

        throwDelay = 0; //던지는 딜레이 초기화
        mBoxThrow = true; // true면 던짐

        StartCoroutine(Throw_Motion()); //상자를 던진는 모션 프레임
    }

    //상자가 오른쪽 건물과 충돌하는 순간 호출 됨. (Box_script 리턴되서 호출되는 함수)
    public void BoxThrowEnd(GameObject _box)
    {
        Destroy(_box); //박스를 삭제 시켜줌

        Box_Make(); //박스 생성
        Box_Throw(); //박스 던지기
    }

    //상자를 던지는 모션
    IEnumerator Throw_Motion()
    {
        int frameIndex = 0;
        int species_type = Prefs.getPrefsInt(Prefs.SPECIES_TYPE); //현재 종족 값

        for (int i = 0; i < 4; i++)
        {
            frameIndex++;

            if (mCharacterIndex == 2) //여자 캐릭터 일 때 수레끄는 액션이 하나 더 있어서 다른 캐릭터와 설정을 다르게 함.
            {
                sp_character.transform.GetComponent<UISprite>().spriteName = ImgDatas.CHARACTER_FRAME[species_type][mCharacterIndex][3] + frameIndex; //던지는 프레임 이미지
            }
            else
            {
                sp_character.transform.GetComponent<UISprite>().spriteName = ImgDatas.CHARACTER_FRAME[species_type][mCharacterIndex][2] + frameIndex; //던지는 프레임 이미지
            }

            yield return new WaitForSeconds(0.09f);
        }
    }

    //피버타임 일 때 캐릭터 Dapth를 건물 보다 뒤쪽으로 갈 수 있게 설정  함

    public void FeverTime_Dapth(bool type)
    {
        mFeverTime = type; //피버타임 상태인지 아닌지

        if (mFeverTime) //피버타임
        {
            sp_character.transform.GetComponent<UISprite>().depth = 2;
        }
        else //피버 타임이 끝나고 다시 상자를 던질 수 있게 한다.
        {
            sp_character.transform.GetComponent<UISprite>().depth = 6;

            Box_Make(); //박스 생성
            Box_Throw(); //박스 던지기
        }
    }
}
