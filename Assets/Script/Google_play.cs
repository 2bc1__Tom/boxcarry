﻿using UnityEngine;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;



public class Google_play : MonoSingleton<Google_play>
{

    private string m_strLeaderBoard = "CgkI0OyYm4ILEAIQAQ"; // 리더보드 ID..

    void Awake()
    {
        GPGSActivate();
        DontDestroyOnLoad(this.gameObject);
    }

    // Use this for initialization
    void Start()
    {
    }

    public void GPGSActivate()
    {
        // GooglePlayService
        PlayGamesPlatform.Activate();
    }

    public void LogIn()
    {
        Debug.Log("로그인 시도 ====================================");

        Social.localUser.Authenticate((bool success) =>
        {
            // handle success or failure
            if (true == success)
            {
                Debug.Log("로그인 성공 ====================================");
                //SendBoardScore();    // 로그인과 동시에 점수 보내줌. ( 사용자가 알아서 )
                //ShowLeaderBoard();  // 점수 보내주고 리더보드 보여줌 .
            }
            else
            {
               Debug.Log("로그인 실패 ====================================");
            }
        });
    }

    public void SendBoardScore()
    {
        int Reincarnation_Level = Prefs.getPrefsInt(Prefs.CHARACTER_COLOR); //현재 환생 단계
        int people_levle = Prefs.getPrefsInt(Prefs.ITEM_LEVEL + 0); //현재 사람추가 레벨

        int ranking = (30 * Reincarnation_Level) + people_levle;

        Social.ReportScore(ranking , m_strLeaderBoard, (bool success) =>
        {
            if (success == true)
            {

            }
            else
            {

            }
            // handle success or failure
        });
    }

    public void ShowLeaderBoard()
    {
        Social.ShowLeaderboardUI();
    }

    public void SpecialShowLeaderBoard()
    {
        ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(m_strLeaderBoard);
    }

    public void SignOut()
    {
        ((PlayGamesPlatform)Social.Active).SignOut();
    }
}
