﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using Day7SDK;

///인앱결제 매니저 Android Native, IOS Native를 사용한다.
public class Day7InappManager : Singleton<Day7InappManager> {
	/// 서버로 부터 오는 데이터 클레스
// 	public class Product {
// 		private string mProduct_idx;		///인덱스
// 		private string mProduct_id;			///아이디 SKU
// 		private string mProduct_type;		///타입
// 		private string mProduct_cost;		///가격
// 		private string mProduct_give_cash;	///보상 화폐
// 		private string mProduct_bonus;		///보상 화폐 보너스
		
// 		public int product_idx {
// 			get { return int.Parse(mProduct_idx); }
// 			set { mProduct_idx = value.ToString(); }
// 		}
		
// 		public string product_id {
// 			get { return mProduct_id; }
// 			set { mProduct_id = value.ToString(); }
// 		}

// 		public int product_type {
// 			get { return int.Parse(mProduct_type); }
// 			set { mProduct_type = value.ToString(); }
// 		}
		
// 		public int product_cost {
// 			get { return int.Parse(mProduct_cost); }
// 			set { mProduct_cost = value.ToString(); }
// 		}
		
// 		public int product_give_cash {
// 			get { return int.Parse(mProduct_give_cash); }
// 			set { mProduct_give_cash = value.ToString(); }
// 		}
		
// 		public int product_bonus {
// 			get { return int.Parse(mProduct_bonus); }
// 			set { mProduct_bonus = value.ToString(); }
// 		}
		
// 	}

// 	/// 서버로부터 오는 응답코드
// 	public enum MSGCODE {
// 		successed 				= 10000,    // 성공
// 		exception_api 			= 20000,    // 서버 API 호출실패
// 		disconnect_db 			= 30001,    // DB서버에 연결안됨
		
// 		overlap_nickname 		= 10001,
// 		non_user 				= 10002,
// 		overlap_udid 			= 10003,
// 		non_user_no 			= 10004,
// 		non_rank_plan 			= 10005,
// 		non_rank_data 			= 10006,
// 		FAIL_BE_ORDER_CANCEL	= 40007,
// 		FAIL_BE_ORDER_REFUND	= 40008,
// 		FAIL_BE_ORDER_ID		= 40019
// 	}
	
// 	///초기화 여부.
// 	private bool _isInited = false;
	
// 	///유저 아이디. 각앱에서 설정해준다.
// 	[HideInInspector]
// 	public string UserId = "";

// 	/// 서버로 부터 얻어온 Inapp정보
// 	public List<Product> ProductList = new List<Product>();
	
// 	/// 서버 주소
// 	const string BillingServerURL = "http://billing.becomead.com/"; // 상용
	
// 	///각 앱에서 설정해야 하는 결제 보상지급 델리게이트.
// 	//public static event Action<Product> PurchaseSucceedEvent;
// 	//public static event Action<string> RestoreSucceedEvent;

// #if UNITY_ANDROID
// 	///안드로이드 Base64 공개키 값. 각앱에서 설정해준다.
// 	public string AndroidBase64PublicKey = "";
// #elif UNITY_IOS
// 	// 2016.05.26 Kei 중복 클릭 방지 추가
// 	bool mOverlapClickFlag = false;
// #endif

// 	void Start()
//     {
// 		Init();
//     }

// 	///인앱 결제 모듈 초기화 시작. 각 앱마다 적당한 시기에 초기화를 해준다. 1회
// 	public void Init() {
// 	///델리게이트 등록.
// #if UNITY_ANDROID
// 		//listening for purchase and consume events
// 		AndroidInAppPurchaseManager.ActionProductPurchased += OnProductPurchased;
// 		AndroidInAppPurchaseManager.ActionProductConsumed  += OnProductConsumed;
		
// 		//listening for store initilaizing finish
// 		AndroidInAppPurchaseManager.ActionBillingSetupFinished += OnBillingConnected;
		
// #elif UNITY_IOS
// 		PaymentManager.OnTransactionComplete += OnTransactionComplete;
// 		PaymentManager.OnRestoreComplete += OnRestoreComplete;
// #endif
		
// 		///초기화 코르틴 시작.
// 		StartCoroutine (InitProcess ());
// 	}
	
// 	///인앱결제 시작
// 	public void InappPurchase(string sku) {
		
// #if UNITY_ANDROID
// 		if (AndroidInAppPurchaseManager.Client.Inventory.Purchases.Count == 0) {
// 			AndroidInAppPurchaseManager.Client.Purchase (sku);
// 		}
// #elif UNITY_IPHONE
// 		if(mOverlapClickFlag) return;
		
// 		mOverlapClickFlag = true;
// 		PaymentManager.Instance.BuyProduct(sku);
// #endif
// 	}

// 	public void RestorePurchasedItem () {
// #if UNITY_IPHONE
// 		PaymentManager.Instance.RestorePurchases ();
// #endif
// 	}
	
// 	///초기화 여부.
// 	public bool IsInited {
// 		get {
// 			return _isInited;
// 		}
// 	}

// 	///초기화 프로세스
// 	IEnumerator InitProcess() {
// 		///step1. 자체 서버로부터 인앱정보를 얻어온다. 
// 		yield return StartCoroutine (InitProductList ());

// 		///step2. 받아온 인앱정보를 인앱모듈에 설정하고 구글, 애플 서버에 날려서 결제정보를 받아온다.
		
// #if UNITY_ANDROID

// 		foreach (Product product in ProductList) {
// 			AndroidInAppPurchaseManager.Client.AddProduct(product.product_id);
// 		}

// 		AndroidInAppPurchaseManager.Client.Connect(AndroidBase64PublicKey);
// #elif UNITY_IOS
// 		foreach (Product product in ProductList)
// 		{
// 			SA.IOSNative.StoreKit.Product tpl = new SA.IOSNative.StoreKit.Product();
// 			tpl.Id = product.product_id;
// 			if(product.product_type == 0) tpl.Type = ProductType.Consumable;
// 			else if(product.product_type == 1) tpl.Type = ProductType.NonConsumable;

// 			PaymentManager.Instance.AddProduct(tpl);
// 		}

// 		PaymentManager.Instance.LoadStore();

// 		while(!PaymentManager.Instance.IsStoreLoaded) {
// 			yield return new WaitForEndOfFrame();
// 		}

// 		StartCoroutine(SearchLocalData());
// #endif

// 		_isInited = true;
// 	}

// 	///자체 서버로부터 인앱정보를 얻어온다. 
// 	IEnumerator InitProductList() {
// 		int retryCount = 3;
// 		while (retryCount > 0) {
// 			yield return new WaitForSeconds (1.0f);

// 			WWWForm form = new WWWForm ();
// 			form.AddField ("packageName", Application.identifier);
// 			#if UNITY_IPHONE
// 			form.AddField("os", "2");
// 			#endif
// 			WWW www = new WWW (BillingServerURL + "ProductList", form);
// 			yield return www;
			
// 			if (www.error != null) {
// 				Debug.Log ("www connection error : " + www.error);

// 				retryCount--;
// 				continue;
// 			}
			
// 			string packet = System.Text.RegularExpressions.Regex.Unescape (www.text);
// 			JSONObject json = new JSONObject (packet);
// 			if (json == null) {
// 				Debug.LogError ("Failed Result:" + packet);

// 				retryCount--;
// 				continue;
// 			}
			
// 			JSONObject jsonFirst = json ["Item"] [0];
// 			int msgCode = int.Parse (GetJsonField (json, "msgcode"));
			
// 			if (msgCode != (int)MSGCODE.successed) {
// 				Debug.LogError ("msgCode : " + msgCode);

// 				retryCount--;
// 				continue;
// 			}
			
// 			JSONObject itemJsonData = jsonFirst ["productlist"];
			
// 			ProductList.Clear ();
// 			for (int i = 0; i < itemJsonData.Count; i++) {
// 				Product items = new Product ();
// 				items.product_idx = (int)itemJsonData [i].GetField ("product_idx").n;
// 				items.product_id = itemJsonData [i].GetField ("product_id").str;
// 				items.product_type = (int)itemJsonData [i].GetField ("product_type").n;
// 				items.product_cost = (int)itemJsonData [i].GetField ("product_cost").n;
// 				items.product_give_cash = (int)itemJsonData [i].GetField ("product_give_cash").n;
// 				items.product_bonus = (int)itemJsonData [i].GetField ("product_bonus").n;
				
// 				ProductList.Add (items);
// 			}

// 			break;
// 		}
// 	}
	
// 	public System.Action<string[]> GetPurchasesListSuccess;

// 	///자체 서버로부터 인앱정보를 얻어온다. 
// 	public IEnumerator GetPurchasesList() {
// 		int retryCount = 3;
// 		while (retryCount > 0) {
// 			yield return new WaitForSeconds (1.0f);

// 			WWWForm form = new WWWForm ();
// 			form.AddField ("packageName", Application.identifier);
// 			form.AddField ("user_id", UserId);
// #if UNITY_IPHONE
// 			form.AddField("os", "2");
// #elif UNITY_ANDROID
// 			form.AddField("os", "1");
// #endif
// 			WWW www = new WWW (BillingServerURL + "UserBillLogList", form);
// 			yield return www;
			
// 			if (www.error != null) {
// 				Debug.Log ("www connection error : " + www.error);

// 				retryCount--;
// 				continue;
// 			}
			
// 			string packet = System.Text.RegularExpressions.Regex.Unescape (www.text);
// 			JSONObject json = new JSONObject (packet);
// 			if (json == null) {
// 				Debug.LogError ("Failed Result:" + packet);

// 				retryCount--;
// 				continue;
// 			}
			
// 			JSONObject jsonFirst = json ["Item"] [0];
// 			int msgCode = int.Parse (GetJsonField (json, "msgcode"));
			
// 			if (msgCode != (int)MSGCODE.successed) {
// 				Debug.LogError ("msgCode : " + msgCode);

// 				retryCount--;
// 				continue;
// 			}
			
// 			JSONObject itemJsonData = jsonFirst ["billLogList"];
			
// 			if(itemJsonData != null && itemJsonData.Count > 0) {
// 				string[] productIdList = new string[itemJsonData.Count];

// 				for (int i = 0; i < itemJsonData.Count; i++) {
// 					 productIdList[i] = itemJsonData [i].GetField ("product_id").str;
// 				}

// 				if(GetPurchasesListSuccess != null) {
// 					GetPurchasesListSuccess(productIdList);
// 				}
// 			}

// 			break;
// 		}
// 	}
	
// 	///Json 파싱
// 	string GetJsonField(JSONObject json, string field) {
// 		string result = string.Empty;
		
// 		if ((json == null) || (json["Item"] == null)) {
// 			Debug.LogError("Faild Format Json:" + field);
// 			return string.Empty;
// 		}
		
// 		JSONObject root = json["Item"][0];
// 		if (root != null) {
// 			JSONObject node = root.GetField(field);
// 			if (node != null) {
// 				result = node.str;
// 			}
// 		}
		
// 		if (string.IsNullOrEmpty(result) == true) {
// 			result = "0";
// 		}
		
// 		return result;
// 	}
	
// 	///결제 확인 시 Inventory안에 있는 비결제 정보를 모두 확인한다.
// 	IEnumerator InventoryPurchaseVerification() {
// 		foreach (GooglePurchaseTemplate purchase in AndroidInAppPurchaseManager.Client.Inventory.Purchases) {
// 			yield return StartCoroutine(PurchaseVerification(purchase.OriginalJson, purchase.SKU, null));
// 		}
// 	}
	
// 	///결제정보를 서버를 통해 검증 한후 보상한다.
// 	IEnumerator PurchaseVerification(string jsonstring, string productId, string transactionId) {
// 		int retryCount = 3;
// 		// 2017.01.18 Kei 국가코드 추가
// 		string countryCode = "EN"; // 아래 국가코드 이외의 국가는 기본 영어로 설정
// 		if(Application.systemLanguage.Equals(SystemLanguage.Korean))
//         {
//             // 한국
//             countryCode = "KOR";
//         }
//         else if(Application.systemLanguage.Equals(SystemLanguage.English))
//         {
//             // 영어
//             countryCode = "EN";
//         }
//         else if(Application.systemLanguage.Equals(SystemLanguage.Japanese))
//         {
//             // 일본
//             countryCode = "JP";
//         }
//         else if(Application.systemLanguage.Equals(SystemLanguage.ChineseTraditional))
//         {
//             // 대만
//             countryCode = "TW";
//         }
//         else if(Application.systemLanguage.Equals(SystemLanguage.ChineseSimplified) || Application.systemLanguage.Equals(SystemLanguage.Chinese))
//         {
//             // 중국
//             countryCode = "CH";
//         }
		
// 		while (retryCount > 0) {
// 			WWWForm form = new WWWForm();
			
// 			#if UNITY_ANDROID
// 			form.AddField("json", jsonstring);
// 			form.AddField("code", countryCode);
// 			form.AddField("user_id", UserId); // user_id 가 없는 게임의 경우 "" 를 넣으면 과금로그에는 ADID 값으로 들어간다.
// 			form.AddField("udid", "NULL_ADID_ANDROID_USER"); // ADID 값
// 			form.AddField("serverType", "0"); // 게임서버 존재 유무 (보상 지급을 클라이언트로 받을지 게임서버로 보낼지 결정) > 1 : 게임서버 사용, 0 : 게임서버 사용안함
// 			form.AddField("os", "1"); // OS 구분값 > 1 : 안드로이드 , 2 : 아이폰
// 			#elif UNITY_IPHONE
// 			form.AddField("receipt", jsonstring);
// 			form.AddField("productId", productId);
// 			form.AddField("transactionId", transactionId);
// 			form.AddField("code", countryCode);
// 			form.AddField("bid", Application.identifier);
// 			form.AddField("user_id", UserId);
// 			form.AddField("udid", "NULL_ADID_IOS_USER");
// 			form.AddField("serverType", "0"); // 게임서버 존재 유무 (보상 지급을 클라이언트로 받을지 게임서버로 보낼지 결정) > 1 : 게임서버 사용, 0 : 게임서버 사용안함
// 			form.AddField("os", "2");
// 			#endif

// 			WWW www = new WWW(BillingServerURL + "v3/PurchaseVerification", form);
// 			yield return www;
			
// 			if (www.error != null) {
// 				Debug.Log("PurchaseVerification Error : " + www.error);

// 				retryCount--;

// 				yield return new WaitForSeconds(1.0f);
// 				continue;
// 			}
			
// 			string packet = System.Text.RegularExpressions.Regex.Unescape(www.text);
// 			JSONObject json = new JSONObject(packet);
// 			if (json == null) {
// 				Debug.LogError("Failed Result:" + packet);

// 				retryCount--;

// 				yield return new WaitForSeconds(1.0f);
// 				continue;
// 			}
			
// 			int msgCode = int.Parse(GetJsonField(json, "msgcode"));

// 			if (msgCode != (int)MSGCODE.successed) {
				
				

// 				// 특정 상황에 대한 예외처리
// 				if(msgCode == (int)MSGCODE.FAIL_BE_ORDER_CANCEL || msgCode == (int)MSGCODE.FAIL_BE_ORDER_REFUND || msgCode == (int)MSGCODE.FAIL_BE_ORDER_ID)
// 				{
// 					#if UNITY_ANDROID
// 						AndroidInAppPurchaseManager.Client.Consume(productId);
// 					#elif UNITY_IPHONE
// 						UnregisterLocalData(jsonstring, productId, transactionId);
// 					#endif
					
// 					yield break;
// 				}

// 				retryCount--;

// 				yield return new WaitForSeconds(1.0f);
// 				continue;
// 			}

// 			///보상을 지급한다.
// 			//if(PurchaseSucceedEvent != null) {
// 				foreach (Product list in ProductList) {
// 					Debug.Log("결제 성공 : " + productId);
// 					if (list.product_id.Equals(productId)) {
// 						///reward
						
// 						int giveCash = 0;

// 						if (list.product_bonus > 0)
// 						{
// 							giveCash = list.product_give_cash + list.product_bonus;
// 						}
// 						else
// 						{
// 							giveCash = list.product_give_cash;
// 						}

// 						Debug.Log("결제 성공 : " + giveCash);

// 						Prefs.setPrefsJewel(giveCash);

// 						if (Game_script.Instance != null)
// 						{
// 							Game_script.Instance.Gold_text();
// 						}

// 						//PurchaseSucceedEvent(list);
// 					}
// 				//}
// 			}
			
// 			///보상지급 후 컨슘을 날린다.
// #if UNITY_ANDROID
// 			AndroidInAppPurchaseManager.Client.Consume(productId);
// #elif UNITY_IPHONE
// 			UnregisterLocalData(jsonstring, productId, transactionId);
// #endif

// 			break;
// 		}
// 	}

// #if UNITY_ANDROID
// 	///인앱결제 완료 델리게이트
// 	private void OnProductPurchased(BillingResult result) {
// 		if(result.IsSuccess) {
// 			StartCoroutine(InventoryPurchaseVerification());
// 		}
// 	}
	
// 	///인앱결제 완료 컨슘
// 	private void OnProductConsumed(BillingResult result) {
// 		if (result.IsSuccess) {
// 		} else {
// 			AndroidInAppPurchaseManager.Client.Consume(result.Purchase.SKU);
// 		}
// 	}
	
// 	///인앱결제 초기화 연결
// 	private void OnBillingConnected(BillingResult result) {
// 		AndroidInAppPurchaseManager.ActionBillingSetupFinished -= OnBillingConnected;
		
// 		if(result.IsSuccess) {
// 			//Store connection is Successful. Next we loading product and customer purchasing details
// 			AndroidInAppPurchaseManager.ActionRetrieveProducsFinished += OnRetrieveProductsFinised;
// 			AndroidInAppPurchaseManager.Client.RetrieveProducDetails();
// 		} 
// 	}
	
// 	///인앱결제 초기화 완료
// 	private void OnRetrieveProductsFinised(BillingResult result) {
// 		AndroidInAppPurchaseManager.ActionRetrieveProducsFinished -= OnRetrieveProductsFinised;
		
// 		if (result.IsSuccess) {
// 			///초기화 완료 후 구글서버에 인앱결제 컨슘이 완료되지 않는 아이템이 있다면...
// 			if(AndroidInAppPurchaseManager.Client.Inventory.Purchases.Count > 0) {
// 				StartCoroutine(InventoryPurchaseVerification());
// 			}
// 		}
// 	}
// #elif UNITY_IOS
// 	////////////////////////////////////////////////////////////////////////////////////
// 	/// IOS 결제 데이터 날라가는 상황 방지 로직
// 	string IOS_INAPP_LOCAL_DATA = "IOS_INAPP_LOCAL_DATA";
// 	string FIELD_RECEIPT = "receipt";
// 	string FIELD_PRODUCTID = "productId";
// 	string FIELD_TRANSACTION_ID = "transactionId";

// 	// 구매내역 확인
// 	IEnumerator CheckPackageItem(string receiptStr, string productId, string transactionId) {
// 		int retryCount = 3;

// 		while (retryCount > 0) {
// 			WWWForm form = new WWWForm();
			
// 			form.AddField("bid", Application.identifier);
// 			form.AddField("idfa", Day7AdPlugin.IOSInterface.getIDFA());
// 			form.AddField("productId", productId);
// 			form.AddField("transactionId", transactionId);
			
// 			string url = string.Format("{0}{1}", BillingServerURL, "v2/CheckPackage");
// 			WWW www = new WWW(url, form);
// 			yield return www;
			
// 			if (www.error != null) {
// 				Debug.Log("CheckPackageItem Error : " + www.error);

// 				retryCount--;

// 				yield return new WaitForSeconds(1.0f);
// 				continue;
// 			}
			
// 			string packet = System.Text.RegularExpressions.Regex.Unescape (www.text);
// 			JSONObject json = new JSONObject (packet);
// 			if (json == null) {
// 				Debug.LogError ("Failed Result:" + packet);

// 				retryCount--;
// 				continue;
// 			}
			
// 			JSONObject jsonFirst = json ["Item"] [0];
// 			int msgCode = int.Parse (GetJsonField (json, "msgcode"));
			
// 			if (msgCode != (int)MSGCODE.successed) {
// 				Debug.LogError ("msgCode : " + msgCode);

// 				retryCount--;
// 				continue;
// 			}

// 			int isPackageFlagCount = (int)jsonFirst["isFlag"].n;

// 			if(isPackageFlagCount > 0)
// 			{
// 				// 이미 한번 구매했다. restore 로직을 실행
// 				RestoredReward(productId);
// 			}
// 			else
// 			{
// 				// 최초구매
// 				RegisterLocalData (receiptStr, mProductId, mTransactionId);
// 				StartCoroutine(PurchaseVerification(receiptStr, mProductId, mTransactionId));
// 			}

// 			break;
// 		}
// 	}

// 	/// 결제 데이터 남아 있는지 확인..
// 	IEnumerator SearchLocalData() {
// 		string localData = PlayerPrefs.GetString (IOS_INAPP_LOCAL_DATA);
// 		if (string.IsNullOrEmpty (localData))
// 			yield break;

// 		JSONObject listData = new JSONObject (localData);
// 		for (int i = 0; i < listData.list.Count; i++) {
// 			string receipt = listData.list [i].GetField (FIELD_RECEIPT).str;
// 			string productId = listData.list [i].GetField (FIELD_PRODUCTID).str;
// 			string transactionId = listData.list [i].GetField (FIELD_TRANSACTION_ID).str;

// 			yield return StartCoroutine (PurchaseVerification (receipt, productId, transactionId));
// 		}
// 	}

// 	/// 결제 정보 저장
// 	void RegisterLocalData(string receipt, string productId, string transactionId) {
// 		string localData = PlayerPrefs.GetString (IOS_INAPP_LOCAL_DATA);

// 		JSONObject listData;
// 		if (!string.IsNullOrEmpty (localData)) {
// 			listData = new JSONObject (localData);
// 		} else {
// 			listData = new JSONObject (JSONObject.Type.ARRAY);
// 		}

// 		JSONObject newData = new JSONObject ();
// 		newData.AddField (FIELD_RECEIPT, receipt);
// 		newData.AddField (FIELD_PRODUCTID, productId);
// 		newData.AddField (FIELD_TRANSACTION_ID, transactionId);

// 		listData.list.Add (newData);

// 		PlayerPrefs.SetString (IOS_INAPP_LOCAL_DATA, listData.ToString ());
// 	}

// 	/// 결제 정보 삭제
// 	void UnregisterLocalData(string receipt, string productId, string transactionId) {
// 		string localData = PlayerPrefs.GetString (IOS_INAPP_LOCAL_DATA);
// 		if (string.IsNullOrEmpty (localData))
// 			return;

// 		JSONObject listData = new JSONObject (localData);
// 		for (int i = 0; i < listData.list.Count; i++) {
// 			string oldReceipt = listData.list [i].GetField (FIELD_RECEIPT).str;
// 			string oldProductId = listData.list [i].GetField (FIELD_PRODUCTID).str;
// 			string oldTransactionId = listData.list [i].GetField (FIELD_TRANSACTION_ID).str;

// 			if (productId.Equals (oldProductId) && receipt.Equals (oldReceipt) && transactionId.Equals (oldTransactionId)) {
// 				listData.list.RemoveAt (i);
// 				break;
// 			}
// 		}

// 		PlayerPrefs.SetString (IOS_INAPP_LOCAL_DATA, listData.ToString ());
// 	}
// 	////////////////////////////////////////////////////////////////////////////////////

// 	public void RestorePurchases() {
// 		PaymentManager.Instance.RestorePurchases ();
// 	}

// 	string mProductId = "";
// 	string mTransactionId = "";
// 	private void OnTransactionComplete(PurchaseResult result) {
// 		switch (result.State) {
// 		case PurchaseState.Purchased:
// 			mProductId = result.ProductIdentifier;
// 			mTransactionId = result.TransactionIdentifier;

// 			ISN_Security.OnReceiptLoaded += HandleOnReceiptLoaded;
// 			ISN_Security.Instance.RetrieveLocalReceipt();
// 			break;
// 		case PurchaseState.Restored:
// 			mProductId = result.ProductIdentifier;
// 			mTransactionId = result.TransactionIdentifier;

// 			RestoredReward(result.ProductIdentifier);
// 			break;
// 		case PurchaseState.Deferred:
// 			break;
// 		case PurchaseState.Failed:
// 			IOSNativePopUpManager.showMessage("알림", "결제에 실패하였습니다 :(");
// 			break;
// 		}
		
// 		mOverlapClickFlag = false;
// 	}

// 	private void OnRestoreComplete(RestoreResult res) {
// 		if(res.IsSucceeded) {
// 			IOSNativePopUpManager.showMessage("Success", "Restore Completed");
// 		} else {
// 			IOSNativePopUpManager.showMessage("Error: " + res.Error.Code, res.Error.Message);
// 		}
// 	}

// 	private void RestoredReward(string productId)
// 	{
// 		/// 리스토어 보상을 지급한다.
// 		//if(RestoreSucceedEvent != null) {
// 			foreach (Product list in ProductList) {
				
// 				if (list.product_id.Equals(productId)) {
// 					///reward
// 					int giveCash = 0;

// 					if (list.product_bonus > 0)
// 					{
// 						giveCash = list.product_give_cash + list.product_bonus;
// 					}
// 					else
// 					{
// 						giveCash = list.product_give_cash;
// 					}

// 					Prefs.setPrefsJewel(giveCash);

// 					if (Game_script.Instance != null)
// 					{
// 						Game_script.Instance.Gold_text();
// 					}

// 					RestoreSucceedEvent(list.product_id);
// 				}
// 			}
// 		//}
// 	}

// 	void HandleOnReceiptLoaded (ISN_LocalReceiptResult res) {
// 		ISN_Security.OnReceiptLoaded -= HandleOnReceiptLoaded;
// 		if(res.Receipt != null)
// 		{
// 			byte[] ReceiptData = res.Receipt;
// 			string base64string = System.Convert.ToBase64String(ReceiptData);
// 			bool checkFlag = false;
// 			foreach(SA.IOSNative.StoreKit.Product product in PaymentManager.Instance.Products)
// 			{
// 				if(mProductId == product.Id)
// 				{
// 					if(product.Type == ProductType.NonConsumable)
// 					{
// 						checkFlag = true;
// 						break;
// 					}
// 					else
// 					{
// 						checkFlag = false;
// 						break;
// 					}
// 				}
// 			}

// 			if(checkFlag)
// 			{
// 				StartCoroutine(CheckPackageItem(base64string, mProductId, mTransactionId));
// 			}
// 			else
// 			{
// 				RegisterLocalData (base64string, mProductId, mTransactionId);
// 				StartCoroutine(PurchaseVerification(base64string, mProductId, mTransactionId));
// 			}
			
// 		}
// 		else
// 		{
// 			ISN_Security.OnReceiptRefreshComplete += HandleOnReceiptRefreshComplete;
// 			ISN_Security.Instance.StartReceiptRefreshRequest();
// 		}
// 	}

// 	void HandleOnReceiptRefreshComplete (SA.Common.Models.Result res) {
// 		ISN_Security.OnReceiptRefreshComplete -= HandleOnReceiptRefreshComplete;
// 		if(res.IsSucceeded)
// 		{
// 			ISN_Security.OnReceiptLoaded += HandleOnReceiptLoaded;
// 			ISN_Security.Instance.RetrieveLocalReceipt();
// 		}
// 		else
// 		{
// 			//IOSNativePopUpManager.showMessage("Fail", "Receipt Refresh Failed");
// 		}
// 	}
// #endif
}
