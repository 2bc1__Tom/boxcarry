﻿// using UnityEngine;
// using System.Collections;
// using System.Collections.Generic;
// using TwitterKit.Unity;
// using TwitterKit.Internal;
// using System.IO;
// using Day7SDK;

// public class TwitterManager : Singleton<TwitterManager>
// {

//     public System.Action<bool> OnTwitterPostSuccess;

//     private bool mIsTwitterAuntifivated = false;
//     private bool mIsPosting = false;
//     private bool mIsPostingFlag = false;
//     private string mShareMessage = string.Empty;
//     private Texture2D mShareTexture = null;

//     private TwitterSession mSession;

//     void Awake()
//     {
//         Twitter.AwakeInit();
//     }

//     void Start()
//     {
// 		Twitter.Init();
//     }

//     // 트위터 공유 함수
//     public void TwitterPostSend(string message, Texture2D tex = null)
//     {
//         if (mIsPostingFlag) return; // 중복 방지
//         mIsPostingFlag = true;

// 		if (!mIsTwitterAuntifivated)
// 		{
//             ///아직 로그인을 하지 않았다면..
//             mIsPosting = true;
//             mShareMessage = message;
//             mShareTexture = tex;

//             TwitterSession _session = Twitter.Session;
//             if (_session == null) Twitter.LogIn(OnLoginSuccess, OnLoginFail);
//             else OnLoginSuccess(_session);
// 		}
// 		else
// 		{
// 			///로그인 이후라면 바로 이미지 + 글 보내기.
//             if (message == null) message = "";
//             string encodedMedia = "";
//             if (tex != null)
//             {
//                 byte[] val = tex.EncodeToPNG();
//                 encodedMedia = System.Convert.ToBase64String(val);
//             }
//             Twitter.Compose(mSession, encodedMedia, message, null, OnComposeSuccess, OnComposeFail, OnComposeCancle);
// 		}
//     }

//     /// 트위터 로그인 성공
//     private void OnLoginSuccess(TwitterSession _session)
//     {
//         ///트위터 로그인 성공
//         Debug.Log("OnLoginSuccess : " + _session.id);
//         mSession = _session;
//         mIsPostingFlag = false;
//         mIsTwitterAuntifivated = true;
//         if (mIsPosting)
//         {
//             mIsPosting = false;
//             TwitterPostSend(mShareMessage, mShareTexture);
//         }
//     }

//     /// 로그인 실패
//     private void OnLoginFail(ApiError _error)
//     {
//         mIsPostingFlag = false;
//         Debug.Log("Twitter Login Fail : " + _error);
//     }

//     /// 성공
//     private void OnComposeSuccess(string _detail)
//     {
//         Debug.Log("OnComposeSuccess : " + _detail);
//         mIsPostingFlag = false;
//         if (OnTwitterPostSuccess != null) OnTwitterPostSuccess(true);
//     }

//     /// 실패
//     private void OnComposeFail(ApiError _error)
//     {
//         Debug.Log("OnComposeFail [" + _error.code + "] message : " + _error.message);
//         mIsPostingFlag = false;
//         if (OnTwitterPostSuccess != null) OnTwitterPostSuccess(false);
//     }

//     /// 취소
//     private void OnComposeCancle()
//     {
//         Debug.Log("OnComposeCancle");
//         mIsPostingFlag = false;
//     }

// }
