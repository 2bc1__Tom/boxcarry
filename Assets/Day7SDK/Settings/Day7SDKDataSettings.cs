﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;

#if ATC_SUPPORT_ENABLED
using CodeStage.AntiCheat.ObscuredTypes;
#endif

#if UNITY_EDITOR
using UnityEditor;
[InitializeOnLoad]
#endif

///BecomeSDK에서 사용하는 설정 데이터들.
public class Day7SDKDataSettings : ScriptableObject {
	///사용자 Define 정의를 위해 사용되는 문자열들.
	public static string DEFINE_TNK 		= "Tnk";
	public static string DEFINE_ADPOPCORN 	= "Adpopcorn";
	public static string DEFINE_NAS 		= "Nas";
	public static string DEFINE_TAPJOY 		= "Tapjoy";

	public static string DEFINE_VIDEOAD		= "VideoAd";

	///설정 데이터들
	///오퍼월 사용여부.
	public bool OfferwallsAdGroup 		= true;
	public bool OfferwallsAPI 			= true;

	public bool OfferwallTnkAPI 		= false;
	public string OfferwallTnkKey 		= "Tnk App ID를 입력하세요.";

	public bool OfferwallAdpopcornAPI 	= false;
	public string OfferwallAdpopcornAppKey 	= "Igaworks App Key를 입력하세요.";
	public string OfferwallAdpopcornHashKey = "Igaworks Hash Key를 입력하세요.";
	public bool OfferwallAdpopcornLiveOps 	= false;

	public bool OfferwallNasAPI 		= false;
	public string OfferwallNasAppKey 	= "Nas App Key를 입력하세요.";
	public string OfferwallNasItemId 	= "Nas ItemId를 입력하세요.";

	public bool OfferwallTapjoyAPI 		= false;
	public string OfferwallTapjoyPlacementName 	= "offerwall_unit";

	///비디오광고 사용여부.
	public bool VideoAdGroup 			= true;
	public bool VideoAdAPI 				= false;
	public string VideoAdKey 			= "애드몹 광고 Key를 입력하세요.";

	
	public bool URLSchemesGroup 		= true;
	public string URLSchemes 			= "URLSchemes를 입력하세요.";
}
