﻿using UnityEngine;
using System.IO;
using System.Collections.Generic;

#if ATC_SUPPORT_ENABLED
using CodeStage.AntiCheat.ObscuredTypes;
#endif

#if UNITY_EDITOR
using UnityEditor;
[InitializeOnLoad]
#endif

/// 설정 데이터들.
public class Day7SDKIOSSettings : Day7SDKDataSettings {	
	public const string SettingsAssetName = "Day7SDKIOSSettings";
	public const string SettingsAssetExtension = ".asset";

	private static Day7SDKIOSSettings instance = null;

	public static Day7SDKIOSSettings Instance {

		get {
			if (instance == null) {
				instance = Resources.Load(SettingsAssetName) as Day7SDKIOSSettings;

				if (instance == null) {
					// If not found, autocreate the asset object.
					instance = CreateInstance<Day7SDKIOSSettings>();
				#if UNITY_EDITOR
					//SA.Common.Util.Files.CreateFolder("Day7SDK/Resources");
					string fullPath = Path.Combine(Path.Combine("Assets", "Day7SDK/Resources"), SettingsAssetName + SettingsAssetExtension);
					AssetDatabase.CreateAsset(instance, fullPath);
				#endif
				}
			}
			return instance;
		}
	}
}
