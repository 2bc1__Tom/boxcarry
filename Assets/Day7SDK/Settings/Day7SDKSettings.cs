﻿using UnityEngine;
using System.IO;

public class Day7SDKSettings : MonoBehaviour {
	public static Day7SDKDataSettings Instance {
		get {
#if UNITY_ANDROID
			return (Day7SDKDataSettings) Day7SDKAndroidSettings.Instance;
#elif UNITY_IOS
			return (Day7SDKDataSettings) Day7SDKIOSSettings.Instance;
#endif
		}
	}
}
