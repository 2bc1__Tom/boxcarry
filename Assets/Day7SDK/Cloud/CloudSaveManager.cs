﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using Day7SDK;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using UnityEngine.SocialPlatforms;
using GooglePlayGames.BasicApi.SavedGame;

///클라우드 저장기능.
public class CloudSaveManager : Singleton<CloudSaveManager> {
	///구글 게임저장에 들어갈 아이콘.
	public Texture2D TextureIcon;

	///로그인 결과 반환.
	public static event System.Action<bool> ActionLogin =  delegate {};
	///게임데이터 로드 성공.
	public static event System.Action<string, string, long> ActionGameLoadSuccess =  delegate {};
	///게임데이터 로드 실패.
	public static event System.Action ActionGameLoadFail =  delegate {};
	public static event System.Action ActionGameLoadEmpty =  delegate {};

	///문자열들.
	const string DATA_NAME = "Day7GamesSaveFile";
	const string DATE_TIME = "DateTime";
	const string CURRENT_TICKS = "CurrentTicks";
	const string DATA = "Data";

	///초기화 성공인지. iOS전용
	bool _isInit = false;

	const bool LOG = true;

	///현재 로그인 중인지..
	public bool IsLogin() {
		if(LOG) Debug.Log("IsLogin");

#if UNITY_ANDROID
        //if(GooglePlayConnection.State == GPConnectionState.STATE_CONNECTED) return true;
        return Social.localUser.authenticated;
#elif UNITY_IOS
		return _isInit;
#endif
        return false;
	}

	///로그인/초기화. 해당 기능을 사용한다면 최초1회는 무조건 실행.
	public void Login() {
		if(LOG) Debug.Log("Login");

#if UNITY_ANDROID
        //if(GooglePlayConnection.State != GPConnectionState.STATE_CONNECTED) {
        //	GooglePlayConnection.Instance.Connect ();
        //} 
        //else {
        //	if(LOG) Debug.Log("GooglePlayConnection.State : " + GooglePlayConnection.State.ToString());
        //}
        PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
        .EnableSavedGames()
        .Build();

        PlayGamesPlatform.InitializeInstance(config);
        PlayGamesPlatform.DebugLogEnabled = true;
        PlayGamesPlatform.Activate();
        Social.localUser.Authenticate((bool success) => {
            // handle success or failure
            if (success)
            {
                if (LOG) Debug.Log("Login success : " + Social.localUser.authenticated);
            }
            else
            {
                if (LOG) Debug.Log("Login Faild : " + Social.localUser.authenticated);
            }
            ActionLogin(success);
        });
#elif UNITY_IOS
//		iCloudManager.Instance.init ();
		OnCloundInitAction(new SA.Common.Models.Result());
#endif
    }

	///로그아웃.
	public void Logout() {
		Debug.Log("Logout");

#if UNITY_ANDROID
        //if(GooglePlayConnection.State == GPConnectionState.STATE_CONNECTED) {
        //	GooglePlayConnection.Instance.Disconnect ();
        //} 
        //else {
        //	if(LOG) Debug.Log("GooglePlayConnection.State : " + GooglePlayConnection.State.ToString());
        //}
        if(Social.localUser.authenticated)
            PlayGamesPlatform.Instance.SignOut();
#elif UNITY_IOS
		_isInit = false;
#endif
    }

	///게임을 저장한다.
	public void Save(string saveData) {
		if(LOG) Debug.Log("Save DataLength : " + saveData.Length);

#if UNITY_ANDROID
		//if(GooglePlayConnection.State == GPConnectionState.STATE_CONNECTED) {
		//	StartCoroutine(MakeScreenshotAndSaveGameData(saveData));
		//}
		//else {
		//	if(LOG) Debug.Log("GooglePlayConnection.State : " + GooglePlayConnection.State.ToString());
		//}
        if(Social.localUser.authenticated)
        {
            StartCoroutine(MakeScreenshotAndSaveGameData(saveData));
        }
#elif UNITY_IOS
		if(_isInit) {
			StartCoroutine(MakeScreenshotAndSaveGameData(saveData));
		}
		else {
			if(LOG) Debug.Log("Current State : Logout");
		}
#endif
    }

	///게임 데이터를 불러온다.
	public void Load() {
		if(LOG) Debug.Log("Loading saved games..");

#if UNITY_ANDROID
        //if(GooglePlayConnection.State == GPConnectionState.STATE_CONNECTED) {
        //	GooglePlaySavedGamesManager.ActionAvailableGameSavesLoaded += ActionAvailableGameSavesLoaded;
        //	GooglePlaySavedGamesManager.Instance.LoadAvailableSavedGames();
        //}
        //else {
        //	if(LOG) Debug.Log("GooglePlayConnection.State : " + GooglePlayConnection.State.ToString());
        //}
        if (Social.localUser.authenticated)
            OpenLoadGame();
#elif UNITY_IOS
		if(_isInit) {
			iCloudManager.Instance.requestDataForKey(DATA_NAME);
		}
		else {
			if(LOG) Debug.Log("Current State : Logout");
		}
#endif
    }

	void OnEnable() {
#if UNITY_ANDROID
		//GooglePlayConnection.ActionPlayerConnected +=  OnPlayerConnected;
		//GooglePlayConnection.ActionPlayerDisconnected += OnPlayerDisconnected;
		
		//GooglePlayConnection.ActionConnectionResultReceived += OnConnectionResult;

		//GooglePlaySavedGamesManager.ActionGameSaveLoaded += ActionGameSaveLoaded;
		//GooglePlaySavedGamesManager.ActionConflict += ActionConflict;
#elif UNITY_IOS
		//initialize icloud and listed for events
		iCloudManager.OnCloudInitAction += OnCloundInitAction;
		iCloudManager.OnCloudDataReceivedAction += OnCloudDataReceivedAction;
		iCloudManager.OnStoreDidChangeExternally += HandleOnStoreDidChangeExternally;
#endif
	}

	void OnDisable() {
#if UNITY_ANDROID
		//GooglePlayConnection.ActionPlayerConnected -=  OnPlayerConnected;
		//GooglePlayConnection.ActionPlayerDisconnected -= OnPlayerDisconnected;
		
		//GooglePlayConnection.ActionConnectionResultReceived -= OnConnectionResult;

		//GooglePlaySavedGamesManager.ActionGameSaveLoaded -= ActionGameSaveLoaded;
		//GooglePlaySavedGamesManager.ActionConflict -= ActionConflict;
#elif UNITY_IOS
		//initialize icloud and listed for events
		iCloudManager.OnCloudInitAction -= OnCloundInitAction;
		iCloudManager.OnCloudDataReceivedAction -= OnCloudDataReceivedAction;
		iCloudManager.OnStoreDidChangeExternally -= HandleOnStoreDidChangeExternally;
#endif
	}

#if UNITY_ANDROID
	//////////////////////////////////////////////////////////////////////
	///구글플레이 연결해제.
	private void OnPlayerDisconnected() {
		if(LOG) Debug.Log("OnPlayerDisconnected");
	}
	
	///구글플레이 연결.
	private void OnPlayerConnected() {
		if(LOG) Debug.Log("OnPlayerConnected");
	}
	
	///구글플레이 연결 결과.
	private void OnConnectionResult(GooglePlayConnectionResult result) {
		ActionLogin(result.IsSuccess);
		if(LOG) Debug.Log("ConnectionResul:  " + result.code.ToString());
	}

	///게임 저장 결과.
	private void ActionGameSaveResult (GP_SpanshotLoadResult result) {
		GooglePlaySavedGamesManager.ActionGameSaveResult -= ActionGameSaveResult;
		if(LOG) Debug.Log("ActionGameSaveResult: " + result.Message);
	}

	///게임 데이터 로드.
	private void ActionGameSaveLoaded (GP_SpanshotLoadResult result) {
		if(result.IsSucceeded) {
			ActionGameLoadSuccess(result.Snapshot.stringData, result.Snapshot.meta.Description, result.Snapshot.meta.LastModifiedTimestamp);
		}
		else {
			ActionGameLoadFail();
		}
		
		if(LOG) Debug.Log("ActionGameSaveLoaded: " + result.Message);
	}	

	///저장된 모든 데이터의 헤더를 로드..
	void ActionAvailableGameSavesLoaded (GooglePlayResult res) {
		GooglePlaySavedGamesManager.ActionAvailableGameSavesLoaded -= ActionAvailableGameSavesLoaded;
		if(res.IsSucceeded) {
			if(GooglePlaySavedGamesManager.Instance.AvailableGameSaves.Count > 0) {
				GP_SnapshotMeta s =  GooglePlaySavedGamesManager.Instance.AvailableGameSaves[0];
				GooglePlaySavedGamesManager.Instance.LoadSpanshotByName(s.Title);
			}
			else {
				ActionGameLoadEmpty ();
			}
		} else {
			ActionGameLoadFail();
		}
	}

	private void ActionConflict (GP_SnapshotConflict result) {

		Debug.Log("Conflict Detected: ");

		GP_Snapshot snapshot = result.Snapshot;
		GP_Snapshot conflictSnapshot = result.ConflictingSnapshot;

		// Resolve between conflicts by selecting the newest of the conflicting snapshots.
		GP_Snapshot mResolvedSnapshot = snapshot;

		if (snapshot.meta.LastModifiedTimestamp < conflictSnapshot.meta.LastModifiedTimestamp) {
			mResolvedSnapshot = conflictSnapshot;
		}

		result.Resolve(mResolvedSnapshot);
	}
    //////////////////////////////////////////////////////////////////////
#elif UNITY_IOS
	//////////////////////////////////////////////////////////////////////
	///iCloud 이벤트

	///초기화 성공/실패
	private void OnCloundInitAction (SA.Common.Models.Result result) {
		_isInit = result.IsSucceeded;
		ActionLogin(_isInit);

		if(LOG) Debug.Log("OnCloundInitAction: " + result.IsSucceeded);
	}

	///데이터 가져오기 성공/실패
	private void OnCloudDataReceivedAction (iCloudData data) {
		if(data.IsEmpty) {
			ActionGameLoadEmpty();

			if(LOG) Debug.Log("OnCloudDataReceivedAction: IsEmpty");
		} else {
			JSONObject temp = new JSONObject(data.stringValue);
			string time = temp.GetField(DATE_TIME).str;
			long ticks = temp.GetField(CURRENT_TICKS).i;
			string saveData = temp.GetField(DATA).str;

			ActionGameLoadSuccess(saveData, time, ticks);

			if(LOG) Debug.Log("OnCloudDataReceivedAction: " + time);
		}
	}	

	///데이터 변화가 있을때
	void HandleOnStoreDidChangeExternally (System.Collections.Generic.List<iCloudData> changedData) {
		if(LOG) {
			foreach(iCloudData data in changedData) {
				ISN_Logger.Log("Cloud data with key:  " + data.key + " was chnaged");
			}
		}	
	}
	
	//////////////////////////////////////////////////////////////////////
#endif
    string tempData = "";
    ///데이터 저장 코르틴.
    private IEnumerator MakeScreenshotAndSaveGameData(string saveData) {
		yield return new WaitForEndOfFrame();

		///현재 시간 및 Tick을 저장한다.
		string dateTime = System.DateTime.Now.ToString("MM/dd/yyyy H:mm:ss");
		long currentTime = System.DateTime.Now.Ticks;
        tempData = saveData;
#if UNITY_ANDROID
        OpenSavedGame();
        //GooglePlaySavedGamesManager.ActionGameSaveResult += ActionGameSaveResult;
        //GooglePlaySavedGamesManager.Instance.CreateNewSnapshot(DATA_NAME,
        //dateTime,
        //TextureIcon,
        //saveData,
        //currentTime);
#elif UNITY_IOS
		JSONObject temp = new JSONObject();
		temp.AddField(DATE_TIME, dateTime);
		temp.AddField(CURRENT_TICKS, currentTime);
		temp.AddField(DATA, saveData);

		iCloudManager.Instance.setString (DATA_NAME, temp.ToString());
#endif
    }

    void OpenSavedGame()
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.OpenWithAutomaticConflictResolution(DATA_NAME, DataSource.ReadCacheOrNetwork,
            ConflictResolutionStrategy.UseLongestPlaytime, OnSavedGameOpened);
    }

    public void OnSavedGameOpened(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            // handle reading or writing of saved game.
            SaveGame(game, tempData);
        }
        else
        {
            // handle error
        }
    }

    void SaveGame(ISavedGameMetadata game, string savedData)
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;

        string dateTime = System.DateTime.Now.ToString("MM/dd/yyyy H:mm:ss");

        SavedGameMetadataUpdate.Builder builder = new SavedGameMetadataUpdate.Builder();
        builder = builder.WithUpdatedDescription(dateTime);
        if (TextureIcon != null)
        {
            byte[] pngData = TextureIcon.EncodeToPNG();
            builder = builder.WithUpdatedPngCoverImage(pngData);
        }
        SavedGameMetadataUpdate updatedMetadata = builder.Build();
        savedGameClient.CommitUpdate(game, updatedMetadata, GetBytes(savedData), OnSavedGameWritten);
    }

    public void OnSavedGameWritten(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        Debug.LogWarning("OnSavedGameWritten : " + status);
    }

    void OpenLoadGame()
    {
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.OpenWithAutomaticConflictResolution(DATA_NAME, DataSource.ReadCacheOrNetwork,
            ConflictResolutionStrategy.UseLongestPlaytime, OnFileOpenToLoad);
    }

    private void OnFileOpenToLoad(SavedGameRequestStatus status, ISavedGameMetadata game)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            LoadGameData(game);
        }
        else
        {
            Debug.LogWarning("Error opening Saved Game" + status);
        }
    }

    string tempDescription = "";
    void LoadGameData(ISavedGameMetadata game)
    {
        tempDescription = game.Description;
        ISavedGameClient savedGameClient = PlayGamesPlatform.Instance.SavedGame;
        savedGameClient.ReadBinaryData(game, OnSavedGameDataRead);
    }

    public void OnSavedGameDataRead(SavedGameRequestStatus status, byte[] data)
    {
        if (status == SavedGameRequestStatus.Success)
        {
            // handle processing the byte array data
            if(data != null)
            {
                ActionGameLoadSuccess(GetString(data), tempDescription, 0);
            }
            else
            {
                ActionGameLoadEmpty();
            }

        }
        else
        {
            // handle error
            ActionGameLoadFail();
        }

    }

    private static byte[] GetBytes(string str)
    {
        byte[] bytes = new byte[str.Length * sizeof(char)];
        System.Buffer.BlockCopy(str.ToCharArray(), 0, bytes, 0, bytes.Length);
        return bytes;
    }


    private static string GetString(byte[] bytes)
    {
        char[] chars;
        if (bytes.Length % 2 != 0)
        {
            chars = new char[(bytes.Length / sizeof(char)) + 1];
        }
        else
        {
            chars = new char[bytes.Length / sizeof(char)];
        }

        System.Buffer.BlockCopy(bytes, 0, chars, 0, bytes.Length);
        return new string(chars);
    }
}
