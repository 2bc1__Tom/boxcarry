﻿using UnityEngine;
using System;
using System.Collections;
//using TapjoyUnity;

///오퍼월 광고 매니저
public class Day7OfferwallAds : MonoBehaviour {
	
	///싱글톤.
	private static Day7OfferwallAds _instance = null;
	public static Day7OfferwallAds Instance {
		get {
			if (_instance == null) {
				_instance = (Day7OfferwallAds)FindObjectOfType (typeof(Day7OfferwallAds));
			}
			return _instance;
		}
	}

///각 광고 핸들러 변수들.. 설정된 경우에만 정의되어 사용된다.
#if Tnk
	TnkHandler _tnkHandler;
#endif

// #if Adpopcorn
// 	AdpopcornHandler _adpopcornHandler;
// #endif

// #if Nas
// 	NasHandler _nasHandler;
// #endif

#if Tapjoy
	TapjoyHandler _tapjoyHandler;
#endif

	///오퍼월 포인트가 지급되었을때 호출. 사용자가 등록해야 한다.
	public static event Action<int> Event_OfferwallSuccess;
	
	///탭조이 객체
//	public TJPlacement offerwallPlacement;

	///사용자에게 포인트를 지급한다.
	public bool GivePointForUser(int point) {

		// if(Event_OfferwallSuccess != null) {

		// 	Prefs.setPrefsJewel(point);

		// 	if (Game_script.Instance != null)
		// 	{
		// 		Game_script.Instance.Gold_text();
		// 	}

		// 	Event_OfferwallSuccess(point);

		// 	return true;
		// }

		Prefs.setPrefsJewel(point);

		if (Game_script.Instance != null)
		{
			Game_script.Instance.Gold_text();
		}

		return true;
	}

	///하단에 빈 오브젝트를 추가한다.
	GameObject AddChildObject(string componentName) {
		GameObject newObj = new GameObject ();
		newObj.transform.parent = gameObject.transform;
		newObj.name = componentName;

		return newObj;
	}

	///오퍼월 초기화.
	IEnumerator Start () {
		///다른 광고모듈 초기화 이후에 하도록.. 탭조이가 문제..
		yield return new WaitForSeconds(1.0f);

		string userId = getUserId ();

		///각 오퍼월이 체크 중 이라면 초기화 한다.
#if Tnk
		_tnkHandler = AddChildObject("TnkHandler").AddComponent<TnkHandler>();
		_tnkHandler.InitOfferwall(userId);
#endif

// #if Adpopcorn
// 		_adpopcornHandler = AddChildObject("AdpopcornHandler").AddComponent<AdpopcornHandler>();
// 		_adpopcornHandler.InitOfferwall(userId);
// #endif

// #if Nas
// 		_nasHandler = AddChildObject("NasHandler").AddComponent<NasHandler>();
// 		_nasHandler.InitOfferwall(userId);
// #endif

#if Tapjoy
		Debug.Log("탭조이 인잇");
		_tapjoyHandler = AddChildObject("TapjoyHandler").AddComponent<TapjoyHandler>();
		_tapjoyHandler.InitOfferwall(userId);
#endif
	}

	///공지팝업
	public void ShowPopup(string popupKey) {
// #if Adpopcorn
// 		_adpopcornHandler.ShowPopup(popupKey);
// #endif
	}

	///오퍼월을 순서대로 보여준다. 1~4
	public void ShowOfferwall1 () {
		ShowOfferwallTnk();
		//ShowOfferwallTapjoy ();
	}
	
	public void ShowOfferwall2 () {
		ShowOfferwallTapjoy ();
	}

	public void ShowOfferwall3 () {
		//ShowOfferwallAdpopcorn();
		ShowOfferwallTapjoy ();
	}

	public void ShowOfferwall4 () {
		//ShowOfferwallNas();
		ShowOfferwallTapjoy ();
	}

	///각 오퍼월을 보여준다.
	public void ShowOfferwallTnk() {
#if Tnk
		_tnkHandler.ShowOfferwall();
#endif
	}
	public void ShowOfferwallAdpopcorn() {
// #if Adpopcorn
// 		_adpopcornHandler.ShowOfferwall();
// #endif
	}

// 	public void ShowOfferwallNas() {
// #if Nas
// 	_nasHandler.ShowOfferwall();
// #endif
// 	}

	public void ShowOfferwallTapjoy() {
		
#if Tapjoy
	_tapjoyHandler.ShowOfferwall();
#endif
	}

	///애드팝콘 Adbrix기능들..
	///게임을 시작하고 하루 동안의 통계 수치.
	public void SendFirstTimeExperience (string name) {
// #if Adpopcorn
// #if UNITY_ANDROID
// 		IgaworksUnityAOS.IgaworksUnityPluginAOS.Adbrix.firstTimeExperience (name);
// #elif UNITY_IOS
// 		AdBrixPluginIOS.FirstTimeExperience(name);
// #endif
// #endif
	}

	///게임을 시작하고 하루 동안의 통계 수치.
	public void SendFirstTimeExperience (string name, string param) {
// #if Adpopcorn
// #if UNITY_ANDROID
// 		IgaworksUnityAOS.IgaworksUnityPluginAOS.Adbrix.firstTimeExperience (name, param);
// #elif UNITY_IOS
// 		AdBrixPluginIOS.FirstTimeExperienceWithParam(name, param);
// #endif
// #endif
	}

	///아이템 구매 통계 수치.
	[Obsolete("9월 1일 부터 해당 함수 사용 불가!")]
	public void SendBuy (string name) {
// #if Adpopcorn
// #if UNITY_ANDROID
// 		IgaworksUnityAOS.IgaworksUnityPluginAOS.Adbrix.buy (name);
// #elif UNITY_IOS
// 		AdBrixPluginIOS.Buy(name);
// #endif
// #endif
	}

	///아이템 구매 통계 수치.
	[Obsolete("9월 1일 부터 해당 함수 사용 불가!")]
	public void SendBuy (string name, string param) {
// #if Adpopcorn
// #if UNITY_ANDROID
// 		IgaworksUnityAOS.IgaworksUnityPluginAOS.Adbrix.buy (name, param);
// #elif UNITY_IOS
// 		AdBrixPluginIOS.BuyWithParam(name, param);
// #endif
// #endif
	}

	public void SendBuy (string orderID, string productID, string productName, double price)
	{
// #if Adpopcorn
// #if UNITY_ANDROID
// 		IgaworksUnityAOS.IgaworksUnityPluginAOS.Adbrix.purchase(orderID, productID, productName, price, 1, null, null);
// #elif UNITY_IOS
// 		string currency = AdBrixPluginIOS.AdBrixCurrencyName(AdBrixPluginIOS.AdBrixCurrencyKRW);
// 		AdBrixPluginIOS.AdBrixPurchase(orderID, productID, productName, price, 1,currency, null);
// #endif
// #endif
	}

	///필요한 상황 별 통계.
	public void SendRetention (string name) {
// #if Adpopcorn
// #if UNITY_ANDROID
// 		IgaworksUnityAOS.IgaworksUnityPluginAOS.Adbrix.retention (name);
// #elif UNITY_IOS
// 		AdBrixPluginIOS.Retention(name);
// #endif
// #endif
	}

	///필요한 상황 별 통계.
	public void SendRetention (string name, string param) {
// #if Adpopcorn
// #if UNITY_ANDROID
// 		IgaworksUnityAOS.IgaworksUnityPluginAOS.Adbrix.retention (name, param);
// #elif UNITY_IOS
// 		AdBrixPluginIOS.RetentionWithParam(name, param);
// #endif
// #endif
	}

	///사용자 아이디
	public string getUserId() {
		string guid_ = PlayerPrefs.GetString("SYSTEM_GUID", "");
		
		if (guid_.Length == 0) {
			System.Guid myGUID = System.Guid.NewGuid();
			guid_ = myGUID.ToString();
			
			PlayerPrefs.SetString("SYSTEM_GUID", guid_);
			PlayerPrefs.Save();
		}
		
		return guid_;
	}
}
