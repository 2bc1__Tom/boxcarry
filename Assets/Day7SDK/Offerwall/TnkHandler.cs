﻿using UnityEngine;
using System.Collections;

///TNK 핸들러
public class TnkHandler : TnkAd.EventHandler {
	///아이폰에서 중복지급되는 경우가 있어서 방지를 위해..
	public int lastPoint = 0;

	/// <summary>
	/// 오퍼월 초기화
	/// </summary>
	/// <param name="userId">유저 ID</param>
	public void InitOfferwall(string userId) {
		handlerName = "TnkHandler";
		gameObject.name = handlerName;

#if !UNITY_EDITOR
		TnkAd.Plugin.Instance.initInstance ();
		TnkAd.Plugin.Instance.applicationStarted ();
		TnkAd.Plugin.Instance.setUserName (userId);
#endif
	}

	/// <summary>
	/// TNK 오퍼월 보여주기
	/// </summary>
	public void ShowOfferwall() {
		// 2016.08.08 Kei 수정
		// 2016.08.16 House Android에서 창으로 뜨기 때문에  rollback
		// 2016.10.28 House Android iOS 동작 분기처리.
#if UNITY_ANDROID
		TnkAd.Plugin.Instance.showAdList ();
#elif UNITY_IOS
		TnkAd.Plugin.Instance.popupAdList("무료충전소", handlerName);
#endif
	}

	/// <summary>
	/// 포인트 요청 시 이벤트.
	/// </summary>
	/// <param name="point">Point.</param>
	public override void onReturnQueryPoint(int point) {
		if (lastPoint == point) return;

		if (point > 0) {
			// {리워드 지급 처리}
			if(Day7OfferwallAds.Instance.GivePointForUser(point))
			{
				Day7OfferwallAds.Instance.SendRetention("TNK");
				TnkAd.Plugin.Instance.purchaseItem(point, "item01", handlerName); 
			}
		}
	}
	
	/// <summary>
	/// 포인트 소진.
	/// </summary>
	/// <param name="curPoint">Current point.</param>
	/// <param name="seqId">Seq identifier.</param>
	public override void onReturnPurchaseItem(long curPoint, long seqId) {
		lastPoint = 0;
	}

///안드로이드와 IOS 쿼리 동작 방식이 달라짐.
#if UNITY_ANDROID
	void OnApplicationPause(bool pauseStatus){
		if (pauseStatus == false) {
			TnkAd.Plugin.Instance.queryPoint(handlerName); 
		}
	}
#elif UNITY_IOS
	public override void onClose (int type) {
		TnkAd.Plugin.Instance.queryPoint(handlerName);
    }
#endif

}
