﻿using UnityEngine;
using System.Collections;
using TapjoyUnity;

///탭조이 핸들러.
public class TapjoyHandler : MonoBehaviour {
	///탭조이 객체
	public TJPlacement offerwallPlacement;

	/// <summary>
	/// 오퍼월 초기화
	/// </summary>
	/// <param name="userId">유저 ID</param>
	public void InitOfferwall(string userId) {
		// Create offerwall placement
		if (offerwallPlacement == null) {
			offerwallPlacement = TJPlacement.CreatePlacement(Day7SDKSettings.Instance.OfferwallTapjoyPlacementName);			
		}
	}

	/// <summary>
	/// 오퍼월 보여주기
	/// </summary>
	public void ShowOfferwall() {
		if (offerwallPlacement != null) {
			offerwallPlacement.RequestContent();
		}
	}

	void OnEnable() {
		///현재 소지중인 통화를 알려준다.
		Tapjoy.OnGetCurrencyBalanceResponse += HandleGetCurrencyBalanceResponse;
		///신규 API 요청 성공 시.
		TJPlacement.OnRequestSuccess += HandlePlacementRequestSuccess;
		///오퍼월 닫을 때.
		TJPlacement.OnContentDismiss += HandlePlacementContentDismiss;

		Tapjoy.OnConnectSuccess += HandleConnectSuccess;
	}
	
	void OnDisable() {
		Tapjoy.OnGetCurrencyBalanceResponse -= HandleGetCurrencyBalanceResponse;
		TJPlacement.OnRequestSuccess -= HandlePlacementRequestSuccess;
		TJPlacement.OnContentDismiss -= HandlePlacementContentDismiss;

		Tapjoy.OnConnectSuccess -= HandleConnectSuccess;
	}

	///통화정보를 가져온다.
	public void HandleGetCurrencyBalanceResponse(string currencyName, int balance) {
		if (balance > 0) {
			///저장된 통화가 있다면 지급한다.
			
			// {리워드 지급 처리}
			if(Day7OfferwallAds.Instance.GivePointForUser(balance))
			{
				Day7OfferwallAds.Instance.SendRetention("Tapjoy");
				Tapjoy.SpendCurrency (balance);
			}
		}
	}
	
	///오퍼월 정보를 정상적으로 가져왔다면 보여준다.
	public void HandlePlacementRequestSuccess(TJPlacement placement) {
		if (placement.IsContentAvailable()) {
			if(placement.GetName().Equals(Day7SDKSettings.Instance.OfferwallTapjoyPlacementName)) {
				// Show offerwall immediately
				placement.ShowContent();
			}
		}
	}
	
	///오퍼월을 닫는다면 저장된 통화를 가져온다.
	public void HandlePlacementContentDismiss(TJPlacement placement) {
		Tapjoy.GetCurrencyBalance ();
	}

	//Connect success
	public void HandleConnectSuccess() {
		offerwallPlacement = TJPlacement.CreatePlacement(Day7SDKSettings.Instance.OfferwallTapjoyPlacementName);
	}
}
