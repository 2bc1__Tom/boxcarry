﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class Day7SDKSettingMenu : EditorWindow {
#if UNITY_EDITOR
	[MenuItem("Window/Day7SDK/Android", false, 1)]
	public static void AndroidEdit() {
		Selection.activeObject = Day7SDKAndroidSettings.Instance; 
	}

	[MenuItem("Window/Day7SDK/IOS", false, 1)]
	public static void IOSEdit() {
		Selection.activeObject = Day7SDKIOSSettings.Instance;
	}
#endif
}
