﻿using UnityEngine;
using UnityEditor;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Xml;

///인스펙터 UI
[CustomEditor(typeof(Day7SDKIOSSettings))]
public class Day7SDKIOSSettingsEditor : Editor {
	// GUIContent PlusApiLabel   		= new GUIContent("Enable Plus API [?]:", "API used for account managment");

	private Day7SDKIOSSettings settings;

	void Awake() {
		// UpdateDefineSymbols();
	}

	///사용자 Define을 재정의 한다.
	void UpdateDefineSymbols() {
		string tempString = PlayerSettings.GetScriptingDefineSymbolsForGroup (BuildTargetGroup.iOS);
		string[] defineSymbols = tempString.Split (';');

		///기존에 사용하던 설정들을 삭제한다.
		StringBuilder builder = new StringBuilder();
		foreach (string defineSymbol in defineSymbols) {
			if(Day7SDKIOSSettings.DEFINE_TNK.Equals(defineSymbol) ||
			Day7SDKIOSSettings.DEFINE_ADPOPCORN.Equals(defineSymbol) ||
			Day7SDKIOSSettings.DEFINE_NAS.Equals(defineSymbol) ||
			Day7SDKIOSSettings.DEFINE_TAPJOY.Equals(defineSymbol) ||
			Day7SDKIOSSettings.DEFINE_VIDEOAD.Equals(defineSymbol)) {
			}
			else {
				if(builder.Length > 0) {
					builder.Append(';');
				}

				builder.Append(defineSymbol);
			}
		}

		///사용하는 오퍼월만 추가한다.
		if(Day7SDKIOSSettings.Instance.OfferwallTnkAPI) {
			if(builder.Length > 0) {
				builder.Append(';');
			}

			builder.Append(Day7SDKIOSSettings.DEFINE_TNK);
		}
		if(Day7SDKIOSSettings.Instance.OfferwallAdpopcornAPI) {
			if(builder.Length > 0) {
				builder.Append(';');
			}

			builder.Append(Day7SDKIOSSettings.DEFINE_ADPOPCORN);			
		}
		if(Day7SDKIOSSettings.Instance.OfferwallNasAPI) {
			if(builder.Length > 0) {
				builder.Append(';');
			}

			builder.Append(Day7SDKIOSSettings.DEFINE_NAS);
		}
		if(Day7SDKIOSSettings.Instance.OfferwallTapjoyAPI) {
			if(builder.Length > 0) {
				builder.Append(';');
			}

			builder.Append(Day7SDKIOSSettings.DEFINE_TAPJOY);
		}

		/// 비디오광고 추가
		if(Day7SDKIOSSettings.Instance.VideoAdAPI) {
			if(builder.Length > 0) {
				builder.Append(';');
			}

			builder.Append(Day7SDKIOSSettings.DEFINE_VIDEOAD);
		}

		PlayerSettings.SetScriptingDefineSymbolsForGroup (UnityEditor.BuildTargetGroup.iOS, builder.ToString());
	}

	///오퍼월이 설정되면 각 파일을 복사하여 추가한다.
	void FileInstall() {
		string txtPath = "Day7SDK/Offerwall/Txt/";
		string folderPath = "Day7SDK/Offerwall/";
		
		if(Day7SDKIOSSettings.Instance.OfferwallTnkAPI) {
			CopyFile(txtPath + "TnkHandler.txt", folderPath + "TnkHandler.cs");
		}
		else {
			DeleteFile(folderPath + "TnkHandler.cs");
		}

		if(Day7SDKIOSSettings.Instance.OfferwallAdpopcornAPI) {
			CopyFile(txtPath + "AdpopcornHandler.txt", folderPath + "AdpopcornHandler.cs");
		}
		else {
			DeleteFile(folderPath + "AdpopcornHandler.cs");
		}

		if(Day7SDKIOSSettings.Instance.OfferwallNasAPI) {
			CopyFile(txtPath + "NasHandler.txt", folderPath + "NasHandler.cs");
		}
		else {
			DeleteFile(folderPath + "NasHandler.cs");
		}

		if(Day7SDKIOSSettings.Instance.OfferwallTapjoyAPI) {
			CopyFile(txtPath + "TapjoyHandler.txt", folderPath + "TapjoyHandler.cs");
		}
		else {
			DeleteFile(folderPath + "TapjoyHandler.cs");
		}
	}

	private static string GetFullPath(string srcName) {
		if (srcName.Equals (string.Empty)) {
			return Application.dataPath;
		}
		
		if (srcName [0].Equals ('/')) {
			srcName.Remove(0, 1);
		}
		
		return Application.dataPath + "/" + srcName;
	}

	public static bool IsFileExists(string fileName) {
		if (fileName.Equals (string.Empty)) {
			return false;
		}
		
		return File.Exists (GetFullPath (fileName));
	}

	public static void CopyFile(string srcFileName, string destFileName) {
		if (IsFileExists (srcFileName) && !srcFileName.Equals(destFileName)) {
			int index = destFileName.LastIndexOf("/");
			string filePath = string.Empty;
			
			if (index != -1) {
				filePath = destFileName.Substring(0, index);
			}
			
			if (!Directory.Exists(GetFullPath(filePath))) {
				Directory.CreateDirectory(GetFullPath(filePath));
			}
			
			File.Copy(GetFullPath(srcFileName), GetFullPath(destFileName), true);
			
			AssetDatabase.Refresh();
		}
	}
	
	public static void DeleteFile(string fileName, bool refresh = true) {
		if (IsFileExists (fileName)) {
			File.Delete(GetFullPath(fileName));

			if (refresh) {
				AssetDatabase.Refresh();
			}
		}
	}

	public override void OnInspectorGUI() 
	{
		GUI.changed = false;

		settings = target as Day7SDKIOSSettings;

		EditorGUILayout.Space();
		EditorGUILayout.HelpBox("Day7SDK Settings [Developer by Kei]", MessageType.None);


		EditorGUI.indentLevel++;
		EditorGUI.BeginChangeCheck();

		//Offerwalls
		EditorGUILayout.BeginHorizontal();
		settings.OfferwallsAdGroup = EditorGUILayout.Foldout(settings.OfferwallsAdGroup, "Offerwall Enable");
		EditorGUILayout.EndHorizontal();
		if(settings.OfferwallsAdGroup) 
		{
			EditorGUI.indentLevel++;

			//////////////////////////////////////////
			///Tnk
			EditorGUILayout.BeginHorizontal();
			settings.OfferwallTnkAPI = EditorGUILayout.Toggle("TNK",  settings.OfferwallTnkAPI);
			if(GUILayout.Button("SDK Download",  GUILayout.Width(120))) 
			{
				Application.OpenURL("http://docs.tnkad.net/tnk-ad-sdk");
			}
			EditorGUILayout.EndHorizontal();

			EditorGUI.indentLevel++;
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("App ID:");
			settings.OfferwallTnkKey = EditorGUILayout.TextField(settings.OfferwallTnkKey);
			EditorGUILayout.EndHorizontal();
			EditorGUI.indentLevel--;
			EditorGUILayout.Space();
			//////////////////////////////////////////

			//////////////////////////////////////////
			///Adpopcorn
			EditorGUILayout.BeginHorizontal();
			settings.OfferwallAdpopcornAPI = EditorGUILayout.Toggle("Adpopcorn",  settings.OfferwallAdpopcornAPI);
			if(GUILayout.Button("SDK Download",  GUILayout.Width(120))) 
			{
				Application.OpenURL("http://help.igaworks.com/hc/ko/3_3/Content/Article/download_center");
			}
			EditorGUILayout.EndHorizontal();

			EditorGUI.indentLevel++;
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("App Key:");
			settings.OfferwallAdpopcornAppKey = EditorGUILayout.TextField(settings.OfferwallAdpopcornAppKey);
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Hash Key:");
			settings.OfferwallAdpopcornHashKey = EditorGUILayout.TextField(settings.OfferwallAdpopcornHashKey);
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			settings.OfferwallAdpopcornLiveOps = EditorGUILayout.Toggle("Live Ops",  settings.OfferwallAdpopcornLiveOps);
			EditorGUILayout.EndHorizontal();
			EditorGUI.indentLevel--;
			EditorGUILayout.Space();
			//////////////////////////////////////////

			//////////////////////////////////////////
			///Nas
			EditorGUILayout.BeginHorizontal();
			settings.OfferwallNasAPI = EditorGUILayout.Toggle("Nas",  settings.OfferwallNasAPI);
			if(GUILayout.Button("SDK Download",  GUILayout.Width(120))) 
			{
				Application.OpenURL("http://www.appang.kr/nas/doc/ow/?os=android&server=nas&ui=embed");
			}
			EditorGUILayout.EndHorizontal();

			EditorGUI.indentLevel++;
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("App Key:");
			settings.OfferwallNasAppKey = EditorGUILayout.TextField(settings.OfferwallNasAppKey);
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Item ID:");
			settings.OfferwallNasItemId = EditorGUILayout.TextField(settings.OfferwallNasItemId);
			EditorGUILayout.EndHorizontal();
			EditorGUI.indentLevel--;

			EditorGUILayout.Space();
			//////////////////////////////////////////

			//////////////////////////////////////////
			///Tapjoy
			EditorGUILayout.BeginHorizontal();
			settings.OfferwallTapjoyAPI = EditorGUILayout.Toggle("Tapjoy",  settings.OfferwallTapjoyAPI);
			if(GUILayout.Button("Document",  GUILayout.Width(120))) 
			{
				Application.OpenURL("http://dev.tapjoy.com/ko/sdk-integration/unity/getting-started-guide-publishers-unity/");
			}
			EditorGUILayout.EndHorizontal();

			EditorGUI.indentLevel++;
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Placement Name:");
			settings.OfferwallTapjoyPlacementName = EditorGUILayout.TextField(settings.OfferwallTapjoyPlacementName);
			EditorGUILayout.EndHorizontal();
			EditorGUI.indentLevel--;
			//////////////////////////////////////////

			EditorGUI.indentLevel--;
			EditorGUILayout.Space();
		}


		//Video Ads
		EditorGUILayout.BeginHorizontal();
		settings.VideoAdGroup = EditorGUILayout.Foldout(settings.VideoAdGroup, "Video Enable");
		EditorGUILayout.EndHorizontal();
		if(settings.VideoAdGroup)
		{
			EditorGUI.indentLevel++;

			EditorGUILayout.BeginHorizontal();
			settings.VideoAdAPI = EditorGUILayout.Toggle("Unity Ads",  settings.VideoAdAPI);
			if(GUILayout.Button("Download Plugin",  GUILayout.Width(120))) {
				Application.OpenURL("https://developers.google.com/admob/unity/start?hl=ko");
			}
			EditorGUILayout.EndHorizontal();

			EditorGUI.indentLevel++;
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("App Key:");
			settings.VideoAdKey = EditorGUILayout.TextField(settings.VideoAdKey);
			EditorGUILayout.EndHorizontal();
			EditorGUI.indentLevel--;
			EditorGUILayout.Space();

			EditorGUI.indentLevel--;
			EditorGUILayout.Space();
		}

		EditorGUILayout.BeginHorizontal();
		settings.URLSchemesGroup = EditorGUILayout.Foldout(settings.URLSchemesGroup, "URLSchemes");
		EditorGUILayout.EndHorizontal();
		if(settings.URLSchemesGroup)
		{
			EditorGUI.indentLevel++;

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("URLSchemes : ");
			settings.URLSchemes = EditorGUILayout.TextField(settings.URLSchemes);
			EditorGUILayout.EndHorizontal();
		}

		if(GUI.changed) {
			EditorUtility.SetDirty(settings);
			FileInstall();
			UpdateDefineSymbols();
		}
	}
}
