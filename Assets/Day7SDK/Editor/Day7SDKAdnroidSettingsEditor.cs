﻿using UnityEngine;
using UnityEditor;
using System.Text;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Xml;

///인스펙터 UI
[CustomEditor(typeof(Day7SDKAndroidSettings))]
public class Day7SDKAdnroidSettingsEditor : Editor {
	private Day7SDKAndroidSettings settings;

	///사용자 Define을 재정의 한다.
	void UpdateDefineSymbols() {
		string tempString = PlayerSettings.GetScriptingDefineSymbolsForGroup (BuildTargetGroup.Android);
		string[] defineSymbols = tempString.Split (';');

		///기존에 사용하던 설정들을 삭제한다.
		StringBuilder builder = new StringBuilder();
		foreach (string defineSymbol in defineSymbols) {
			if(Day7SDKAndroidSettings.DEFINE_TNK.Equals(defineSymbol) ||
			Day7SDKAndroidSettings.DEFINE_ADPOPCORN.Equals(defineSymbol) ||
			Day7SDKAndroidSettings.DEFINE_NAS.Equals(defineSymbol) ||
			Day7SDKAndroidSettings.DEFINE_TAPJOY.Equals(defineSymbol) ||
			Day7SDKAndroidSettings.DEFINE_VIDEOAD.Equals(defineSymbol)) {
			}
			else {
				if(builder.Length > 0) {
					builder.Append(';');
				}

				builder.Append(defineSymbol);
			}
		}

		///사용하는 오퍼월만 추가한다.
		if(Day7SDKAndroidSettings.Instance.OfferwallTnkAPI) {
			if(builder.Length > 0) {
				builder.Append(';');
			}

			builder.Append(Day7SDKAndroidSettings.DEFINE_TNK);
		}
		if(Day7SDKAndroidSettings.Instance.OfferwallAdpopcornAPI) {
			if(builder.Length > 0) {
				builder.Append(';');
			}

			builder.Append(Day7SDKAndroidSettings.DEFINE_ADPOPCORN);			
		}
		if(Day7SDKAndroidSettings.Instance.OfferwallNasAPI) {
			if(builder.Length > 0) {
				builder.Append(';');
			}

			builder.Append(Day7SDKAndroidSettings.DEFINE_NAS);
		}
		if(Day7SDKAndroidSettings.Instance.OfferwallTapjoyAPI) {
			if(builder.Length > 0) {
				builder.Append(';');
			}

			builder.Append(Day7SDKAndroidSettings.DEFINE_TAPJOY);
		}

		/// 비디오광고 추가
		if(Day7SDKAndroidSettings.Instance.VideoAdAPI) {
			if(builder.Length > 0) {
				builder.Append(';');
			}

			builder.Append(Day7SDKAndroidSettings.DEFINE_VIDEOAD);
		}

		PlayerSettings.SetScriptingDefineSymbolsForGroup (UnityEditor.BuildTargetGroup.Android, builder.ToString());
	}

	///오퍼월이 설정되면 각 파일을 복사하여 추가한다.
	void FileInstall() {
		string txtPath = "Day7SDK/Offerwall/Txt/";
		string folderPath = "Day7SDK/Offerwall/";
		
		if(Day7SDKAndroidSettings.Instance.OfferwallTnkAPI) {
			CopyFile(txtPath + "TnkHandler.txt", folderPath + "TnkHandler.cs");
		}
		else {
			DeleteFile(folderPath + "TnkHandler.cs");
		}

		if(Day7SDKAndroidSettings.Instance.OfferwallAdpopcornAPI) {
			CopyFile(txtPath + "AdpopcornHandler.txt", folderPath + "AdpopcornHandler.cs");
		}
		else {
			DeleteFile(folderPath + "AdpopcornHandler.cs");
		}

		if(Day7SDKAndroidSettings.Instance.OfferwallNasAPI) {
			CopyFile(txtPath + "NasHandler.txt", folderPath + "NasHandler.cs");
		}
		else {
			DeleteFile(folderPath + "NasHandler.cs");
		}

		if(Day7SDKAndroidSettings.Instance.OfferwallTapjoyAPI) {
			CopyFile(txtPath + "TapjoyHandler.txt", folderPath + "TapjoyHandler.cs");
		}
		else {
			DeleteFile(folderPath + "TapjoyHandler.cs");
		}
	}

	///안드로이드 매니페스트에 데이터를 설정한다.
	void UpdateManifest() {
		UpdateManifestDay7SDK();

		if(Day7SDKAndroidSettings.Instance.OfferwallTnkAPI) {
			UpdateManifestTnk();
		}

		if(Day7SDKAndroidSettings.Instance.OfferwallAdpopcornAPI) {
			UpdateManifestAdpopcorn();
		}

		if(Day7SDKAndroidSettings.Instance.OfferwallNasAPI) {
			UpdateManifestNas();
		}

		if(Day7SDKAndroidSettings.Instance.OfferwallTapjoyAPI) {
			UpdateManifestTapjoy();
		}

		if(Day7SDKAndroidSettings.Instance.VideoAdAPI) {
			UpdateManifestVideoAd();
		}
	}

	void UpdateManifestDay7SDK() {
		SA.Manifest.Template manifest =  SA.Manifest.Manager.GetManifest();
		SA.Manifest.ApplicationTemplate application =  manifest.ApplicationTemplate;

		SA.Manifest.ActivityTemplate AndroidNativeProxy = application.GetOrCreateActivityWithName("com.androidnative.AndroidNativeProxy");
		AndroidNativeProxy.SetValue("android:launchMode", "singleTask");
		AndroidNativeProxy.SetValue("android:label", "@string/app_name");
		AndroidNativeProxy.SetValue("android:configChanges", "fontScale|keyboard|keyboardHidden|locale|mnc|mcc|navigation|orientation|screenLayout|screenSize|smallestScreenSize|uiMode|touchscreen");
		AndroidNativeProxy.SetValue("android:theme", "@android:style/Theme.Translucent.NoTitleBar");
		
		SA.Manifest.PropertyTemplate receiver = application.GetOrCreatePropertyWithName("receiver", "com.day7.library.InstallConfirmReceiver");
		SA.Manifest.PropertyTemplate intentFilter = receiver.GetOrCreateIntentFilterWithName("android.intent.action.PACKAGE_ADDED");
		SA.Manifest.PropertyTemplate data = intentFilter.GetOrCreatePropertyWithTag("data");
		data.SetValue("android:scheme", "package");

		receiver = application.GetOrCreatePropertyWithName("receiver",  "com.day7.library.InstallConfirmReceiver");
		receiver.SetValue("android:exported", "true");
		intentFilter = receiver.GetOrCreateIntentFilterWithName("com.android.vending.INSTALL_REFERRER");

		if(Day7SDKAndroidSettings.Instance.OfferwallTnkAPI) {
			data = receiver.GetOrCreatePropertyWithName("meta-data", "forwardTnk");
			data.SetValue("android:value", "com.tnkfactory.ad.TnkReceiver");
		}

		if(Day7SDKAndroidSettings.Instance.OfferwallAdpopcornAPI) {
			data = receiver.GetOrCreatePropertyWithName("meta-data", "forwardAdpopcorn");
			data.SetValue("android:value", "com.igaworks.IgawReceiver");
		}

		if(Day7SDKAndroidSettings.Instance.OfferwallTapjoyAPI) {
			data = receiver.GetOrCreatePropertyWithName("meta-data", "forwardTapjoy");
			data.SetValue("android:value", "com.tapjoy.InstallReferrerReceiver");
		}
		
		SA.Manifest.PropertyTemplate property = application.GetOrCreatePropertyWithName("meta-data", "unityplayer.SkipPermissionsDialog");
		property.SetValue("android:value", "true");

		SA.Manifest.Manager.SaveManifest();
	}
	
	///안드로이드 TNK 설정.
	void UpdateManifestTnk() {
		SA.Manifest.Template manifest =  SA.Manifest.Manager.GetManifest();
		SA.Manifest.ApplicationTemplate application =  manifest.ApplicationTemplate;

		if(Day7SDKAndroidSettings.Instance.OfferwallTnkAPI) {
			SA.Manifest.ActivityTemplate activityTemp = application.GetOrCreateActivityWithName("com.tnkfactory.ad.AdWallActivity");
			activityTemp.SetValue("android:screenOrientation", "portrait");

			activityTemp = application.GetOrCreateActivityWithName("com.tnkfactory.ad.AdMediaActivity");
			activityTemp.SetValue("android:screenOrientation", "landscape");

			SA.Manifest.PropertyTemplate property = application.GetOrCreatePropertyWithName("meta-data", "tnkad_app_id");
			property.SetValue("android:value", Day7SDKAndroidSettings.Instance.OfferwallTnkKey);

			manifest.AddPermission("android.permission.INTERNET");
			manifest.AddPermission("android.permission.READ_PHONE_STATE");
			manifest.AddPermission("android.permission.ACCESS_WIFI_STATE");
			manifest.AddPermission("android.permission.WRITE_EXTERNAL_STORAGE");
		}

		SA.Manifest.Manager.SaveManifest();
	}

	///안드로이드 AdPopcorn 설정.
	void UpdateManifestAdpopcorn() {
		SA.Manifest.Template manifest =  SA.Manifest.Manager.GetManifest();
		SA.Manifest.ApplicationTemplate application =  manifest.ApplicationTemplate;

		if(Day7SDKAndroidSettings.Instance.OfferwallAdpopcornAPI) {
			///////////////////////////////////////
			///Offerwall setting
			SA.Manifest.ActivityTemplate activityTemp = application.GetOrCreateActivityWithName("com.igaworks.adpopcorn.activity.ApOfferWallActivity_NT");
			activityTemp.SetValue("android:theme", "@android:style/Theme.Translucent.NoTitleBar");

			activityTemp = application.GetOrCreateActivityWithName("com.igaworks.adpopcorn.activity.ApCSActivity_NT");
			activityTemp.SetValue("android:theme", "@android:style/Theme.NoTitleBar");

			activityTemp = application.GetOrCreateActivityWithName("com.igaworks.adpopcorn.activity.ApVideoAdActivity");
			activityTemp.SetValue("android:theme", "@android:style/Theme.NoTitleBar");
			activityTemp.SetValue("android:screenOrientation", "landscape");

			SA.Manifest.PropertyTemplate property = application.GetOrCreatePropertyWithName("meta-data", "igaworks_reward_server_type");
			property.SetValue("android:value", "client");

			manifest.AddPermission("android.permission.GET_ACCOUNTS");
			///////////////////////////////////////

			///////////////////////////////////////
			///Common
			property = application.GetOrCreatePropertyWithName("meta-data", "igaworks_app_key");
			property.SetValue("android:value", Day7SDKAndroidSettings.Instance.OfferwallAdpopcornAppKey);

			property = application.GetOrCreatePropertyWithName("meta-data", "igaworks_hash_key");
			property.SetValue("android:value", Day7SDKAndroidSettings.Instance.OfferwallAdpopcornHashKey);

			manifest.AddPermission("android.permission.INTERNET");
			manifest.AddPermission("android.permission.ACCESS_NETWORK_STATE");
			///////////////////////////////////////		
			
			///////////////////////////////////////
			///LiveOps Push
			if(Day7SDKAndroidSettings.Instance.OfferwallAdpopcornLiveOps) {
				SA.Manifest.PropertyTemplate liveOpsReceiver = application.GetOrCreatePropertyWithName("receiver",  "com.igaworks.liveops.pushservice.LiveOpsGCMBroadcastReceiver");
				liveOpsReceiver.SetValue("android:permission", "com.google.android.c2dm.permission.SEND");
				SA.Manifest.PropertyTemplate gameThriveIntentFilter = liveOpsReceiver.GetOrCreateIntentFilterWithName("com.google.android.c2dm.intent.RECEIVE");
				gameThriveIntentFilter.GetOrCreatePropertyWithName("category", PlayerSettings.applicationIdentifier);
				
				SA.Manifest.PropertyTemplate gcmService = application.GetOrCreatePropertyWithName("service",  "com.igaworks.liveops.pushservice.GCMIntentService");
				gcmService.SetValue("android:enabled", "true");

				SA.Manifest.PropertyTemplate permission_C2D_MESSAGE = manifest.GetOrCreatePropertyWithName("permission", PlayerSettings.applicationIdentifier + ".permission.C2D_MESSAGE");
				permission_C2D_MESSAGE.SetValue("android:protectionLevel", "signature");

				manifest.AddPermission("android.permission.WAKE_LOCK");
				manifest.AddPermission("android.permission.VIBRATE");

				manifest.AddPermission(PlayerSettings.applicationIdentifier + ".permission.C2D_MESSAGE");
				manifest.AddPermission("com.google.android.c2dm.permission.RECEIVE");
			}
			///////////////////////////////////////			
		}
		else {
		}

		SA.Manifest.Manager.SaveManifest();
	}

	///안드로이드 Nas 설정.
	void UpdateManifestNas() {
		SA.Manifest.Template manifest =  SA.Manifest.Manager.GetManifest();
		SA.Manifest.ApplicationTemplate application =  manifest.ApplicationTemplate;

		if(Day7SDKAndroidSettings.Instance.OfferwallNasAPI) {
			///////////////////////////////////////
			///Offerwall setting
			SA.Manifest.ActivityTemplate activityTemp = application.GetOrCreateActivityWithName("com.nextapps.naswall.NASWallBrowser");
			activityTemp.SetValue("android:configChanges", "keyboardHidden|orientation|screenSize");

			SA.Manifest.PropertyTemplate intent_filter = activityTemp.GetOrCreateIntentFilterWithName("android.intent.action.VIEW");
			intent_filter.GetOrCreatePropertyWithName("category", "android.intent.category.DEFAULT");
			intent_filter.GetOrCreatePropertyWithName("category", "android.intent.category.BROWSABLE");

			activityTemp = application.GetOrCreateActivityWithName("com.nextapps.naswall.NASWall");
			activityTemp.SetValue("android:configChanges", "keyboardHidden|orientation");

			SA.Manifest.PropertyTemplate property = application.GetOrCreatePropertyWithName("meta-data", "naswall_app_key");
			property.SetValue("android:value", Day7SDKAndroidSettings.Instance.OfferwallNasAppKey);

			manifest.AddPermission("android.permission.INTERNET");
			manifest.AddPermission("android.permission.READ_PHONE_STATE");
			manifest.AddPermission("android.permission.ACCESS_WIFI_STATE");
			manifest.AddPermission("android.permission.GET_ACCOUNTS");	
		}
		else {
		}

		SA.Manifest.Manager.SaveManifest();
	}

	///안드로이드 Tapjoy 설정.
	void UpdateManifestTapjoy() {
		SA.Manifest.Template manifest =  SA.Manifest.Manager.GetManifest();
		SA.Manifest.ApplicationTemplate application =  manifest.ApplicationTemplate;

		if(Day7SDKAndroidSettings.Instance.OfferwallTapjoyAPI) {
			SA.Manifest.ActivityTemplate activityTemp = application.GetOrCreateActivityWithName("com.tapjoy.TJAdUnitActivity");
			activityTemp.SetValue("android:configChanges", "orientation|keyboardHidden|screenSize");
			activityTemp.SetValue("android:theme", "@android:style/Theme.Translucent.NoTitleBar.Fullscreen");
			activityTemp.SetValue("android:hardwareAccelerated", "true");

			activityTemp = application.GetOrCreateActivityWithName("com.tapjoy.mraid.view.ActionHandler");
			activityTemp.SetValue("android:configChanges", "orientation|keyboardHidden|screenSize");

			activityTemp = application.GetOrCreateActivityWithName("com.tapjoy.mraid.view.Browser");
			activityTemp.SetValue("android:configChanges", "orientation|keyboardHidden|screenSize");

			activityTemp = application.GetOrCreateActivityWithName("com.tapjoy.TJContentActivity");
			activityTemp.SetValue("android:configChanges", "orientation|keyboardHidden|screenSize");
			activityTemp.SetValue("android:theme", "@android:style/Theme.Translucent.NoTitleBar");

			manifest.AddPermission("android.permission.INTERNET");
			manifest.AddPermission("android.permission.ACCESS_NETWORK_STATE");
		}
		else {
		}

		SA.Manifest.Manager.SaveManifest();
	}

	void UpdateManifestVideoAd() {
		SA.Manifest.Template manifest =  SA.Manifest.Manager.GetManifest();
		SA.Manifest.ApplicationTemplate application =  manifest.ApplicationTemplate;

		if(Day7SDKAndroidSettings.Instance.VideoAdAPI) {
			SA.Manifest.ActivityTemplate activityTemp = application.GetOrCreateActivityWithName("com.unity3d.player.UnityPlayerNativeActivity");
			activityTemp.SetValue("android:label", "@string/app_name");
			activityTemp.SetValue("android:launchMode", "singleTask");
			activityTemp.SetValue("android:configChanges", "fontScale|keyboard|keyboardHidden|locale|mnc|mcc|navigation|orientation|screenLayout|screenSize|smallestScreenSize|uiMode|touchscreen");

			SA.Manifest.PropertyTemplate data = activityTemp.GetOrCreatePropertyWithName("meta-data", "unityplayer.UnityActivity");
			data.SetValue("android:value", "true");

			data = activityTemp.GetOrCreatePropertyWithName("meta-data", "unityplayer.ForwardNativeEventsToDalvik");
			data.SetValue("android:value", "true");
		}
		else {
		}

		SA.Manifest.Manager.SaveManifest();
	}

	private static string GetFullPath(string srcName) {
		if (srcName.Equals (string.Empty)) {
			return Application.dataPath;
		}
		
		if (srcName [0].Equals ('/')) {
			srcName.Remove(0, 1);
		}
		
		return Application.dataPath + "/" + srcName;
	}

	public static bool IsFileExists(string fileName) {
		if (fileName.Equals (string.Empty)) {
			return false;
		}
		
		return File.Exists (GetFullPath (fileName));
	}

	public static void CopyFile(string srcFileName, string destFileName) {
		if (IsFileExists (srcFileName) && !srcFileName.Equals(destFileName)) {
			int index = destFileName.LastIndexOf("/");
			string filePath = string.Empty;
			
			if (index != -1) {
				filePath = destFileName.Substring(0, index);
			}
			
			if (!Directory.Exists(GetFullPath(filePath))) {
				Directory.CreateDirectory(GetFullPath(filePath));
			}
			
			File.Copy(GetFullPath(srcFileName), GetFullPath(destFileName), true);
			
			AssetDatabase.Refresh();
		}
	}
	
	public static void DeleteFile(string fileName, bool refresh = true) {
		if (IsFileExists (fileName)) {
			File.Delete(GetFullPath(fileName));

			if (refresh) {
				AssetDatabase.Refresh();
			}
		}
	}

	public override void OnInspectorGUI() {
		GUI.changed = false;

		settings = target as Day7SDKAndroidSettings;

		EditorGUILayout.Space();
		EditorGUILayout.HelpBox("Day7SDK Settings [Developer by Kei]", MessageType.None);

		EditorGUI.indentLevel++;
		EditorGUI.BeginChangeCheck();

		//Offerwalls
		EditorGUILayout.BeginHorizontal();
		settings.OfferwallsAdGroup = EditorGUILayout.Foldout(settings.OfferwallsAdGroup, "Offerwall Enable");
		EditorGUILayout.EndHorizontal();
		if(settings.OfferwallsAdGroup) {
			EditorGUI.indentLevel++;

			//////////////////////////////////////////
			///Tnk
			EditorGUILayout.BeginHorizontal();
			settings.OfferwallTnkAPI = EditorGUILayout.Toggle("TNK",  settings.OfferwallTnkAPI);
			if(GUILayout.Button("SDK Download",  GUILayout.Width(120))) {
				Application.OpenURL("http://docs.tnkad.net/tnk-ad-sdk");
			}
			EditorGUILayout.EndHorizontal();

			EditorGUI.indentLevel++;
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("App ID:");
			settings.OfferwallTnkKey = EditorGUILayout.TextField(settings.OfferwallTnkKey);
			EditorGUILayout.EndHorizontal();
			EditorGUI.indentLevel--;
			EditorGUILayout.Space();
			//////////////////////////////////////////

			//////////////////////////////////////////
			///Adpopcorn
			EditorGUILayout.BeginHorizontal();
			settings.OfferwallAdpopcornAPI = EditorGUILayout.Toggle("Adpopcorn",  settings.OfferwallAdpopcornAPI);
			if(GUILayout.Button("SDK Download",  GUILayout.Width(120))) {
				Application.OpenURL("http://help.igaworks.com/hc/ko/3_3/Content/Article/download_center");
			}
			EditorGUILayout.EndHorizontal();

			EditorGUI.indentLevel++;
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("App Key:");
			settings.OfferwallAdpopcornAppKey = EditorGUILayout.TextField(settings.OfferwallAdpopcornAppKey);
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Hash Key:");
			settings.OfferwallAdpopcornHashKey = EditorGUILayout.TextField(settings.OfferwallAdpopcornHashKey);
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			settings.OfferwallAdpopcornLiveOps = EditorGUILayout.Toggle("Live Ops",  settings.OfferwallAdpopcornLiveOps);
			EditorGUILayout.EndHorizontal();
			EditorGUI.indentLevel--;
			EditorGUILayout.Space();
			//////////////////////////////////////////

			//////////////////////////////////////////
			///Nas
			EditorGUILayout.BeginHorizontal();
			settings.OfferwallNasAPI = EditorGUILayout.Toggle("Nas",  settings.OfferwallNasAPI);
			if(GUILayout.Button("SDK Download",  GUILayout.Width(120))) {
				Application.OpenURL("http://www.appang.kr/nas/doc/ow/?os=android&server=nas&ui=embed");
			}
			EditorGUILayout.EndHorizontal();

			EditorGUI.indentLevel++;
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("App Key:");
			settings.OfferwallNasAppKey = EditorGUILayout.TextField(settings.OfferwallNasAppKey);
			EditorGUILayout.EndHorizontal();

			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Item ID:");
			settings.OfferwallNasItemId = EditorGUILayout.TextField(settings.OfferwallNasItemId);
			EditorGUILayout.EndHorizontal();
			EditorGUI.indentLevel--;

			EditorGUILayout.Space();
			//////////////////////////////////////////

			//////////////////////////////////////////
			///Tapjoy
			EditorGUILayout.BeginHorizontal();
			settings.OfferwallTapjoyAPI = EditorGUILayout.Toggle("Tapjoy",  settings.OfferwallTapjoyAPI);
			if(GUILayout.Button("Document",  GUILayout.Width(120))) {
				Application.OpenURL("http://dev.tapjoy.com/ko/sdk-integration/unity/getting-started-guide-publishers-unity/");
			}
			EditorGUILayout.EndHorizontal();

			EditorGUI.indentLevel++;
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Placement Name:");
			settings.OfferwallTapjoyPlacementName = EditorGUILayout.TextField(settings.OfferwallTapjoyPlacementName);
			EditorGUILayout.EndHorizontal();
			EditorGUI.indentLevel--;
			//////////////////////////////////////////

			EditorGUI.indentLevel--;
			EditorGUILayout.Space();
		}


		//Video Ads
		EditorGUILayout.BeginHorizontal();
		settings.VideoAdGroup = EditorGUILayout.Foldout(settings.VideoAdGroup, "VideoAd Enable");
		EditorGUILayout.EndHorizontal();
		if(settings.VideoAdGroup) {
			EditorGUI.indentLevel++;

			EditorGUILayout.BeginHorizontal();
			settings.VideoAdAPI = EditorGUILayout.Toggle("Video Ad",  settings.VideoAdAPI);
			if(GUILayout.Button("Download Plugin",  GUILayout.Width(120))) {
				Application.OpenURL("https://developers.google.com/admob/unity/start?hl=ko");
			}
			EditorGUILayout.EndHorizontal();

			EditorGUI.indentLevel++;
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("App Key:");
			settings.VideoAdKey = EditorGUILayout.TextField(settings.VideoAdKey);
			EditorGUILayout.EndHorizontal();
			EditorGUI.indentLevel--;
			EditorGUILayout.Space();
		}

		EditorGUILayout.BeginHorizontal();
		if(GUILayout.Button("Update Manifest",  GUILayout.Width(150))) {
			UpdateManifest();
		}
		EditorGUILayout.EndHorizontal();

		if(GUI.changed) {
			EditorUtility.SetDirty(settings);
			FileInstall();
			UpdateDefineSymbols();
		}
	}
}
