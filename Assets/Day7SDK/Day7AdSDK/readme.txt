﻿/********************************************************************
	Created by Kei on 2018.04
	Copyright © 2018년 Kei. All rights reserved.
*********************************************************************/

# Day7 유니티 광고라이브러리

1. 'Day7ADLib' base 씬에 배치한다.

2. Day7ADLib.Instance.Init() 함수로 광고 초기화. ( Day7ADSample 참고 )

3. 아래의 광고 리스트를 참고하여 각 광고를 호출한다.

 - 전면			: Day7ADLib.Instance.ShowADFront(), Day7ADLib.Instance.HideADFront()
 - 중간			: Day7ADLib.Instance.ShowADMid(), Day7ADLib.Instance.HideADMid()
 - 메인아이콘 	: Day7ADLib.Instance.ShowADMainIcon(), Day7ADLib.Instance.HideADMainIcon()
 - 종료광고		: Day7ADLib.Instance.ShowADEnd()
