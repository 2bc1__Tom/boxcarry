﻿/********************************************************************
	Created by Kei on 2018.04
	Copyright © 2018년 Kei. All rights reserved.
*********************************************************************/
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using System.IO;
using System.Linq;
using UnityEngine;
using Day7SDK;
#if UNITY_EDITOR
using UnityEditor;
#endif

public class Day7ADLib : MonoBehaviour
{
	private static Day7ADLib _instance = null;
	public static Day7ADLib Instance {
		get {
			if (_instance == null) {
				_instance = (Day7ADLib)FindObjectOfType (typeof(Day7ADLib));
			}
			return _instance;
		}
	}

	// #if UNITY_EDITOR
    // [MenuItem("Tool/DeleteAllPlayerPrefs")]
    // static void DeleteAllPlayerPrefs()
    // {
    //     PlayerPrefs.DeleteAll();
    // }
    // #endif

#region event
	///광고 초기화 성공 유무 이벤트.
	public static event Action <bool> InitAdComplete;
#endregion

#region enums
	/// <summary>
	/// 앱 브랜드
	/// Day7, 21g, 0back
	/// </summary>
	public enum Brand
	{
		_DAY7,
		_21G
	}

	public enum Orientation
	{
		Portrait,
		Landscape,
	}

	public enum ADType
	{
		front,	// 전면
		mid,	// 중간
		main,	// 메인아이콘
		end,	// 종료
	}

	public enum AniType
	{
		none,
		scale
	}
#endregion

#region const
	//const string mApiUrl = "http://192.168.0.15:8080/day7Ad/excuteGetADList"; //로컬
	//const string mStatisticsUrl = "http://192.168.0.15:8080/day7Ad/statistics"; //로컬
	const string mApiUrl = "http://day7ad.day7games.com/day7Ad/excuteGetADList"; //상용
	const string mStatisticsUrl = "http://day7ad.day7games.com/day7Ad/statistics"; //상용
	const int mPortraitAddListCnt = 9;
	const int mLandscapeAddListCnt = 15;
#endregion

#region path
	string cachePath { get { return Application.temporaryCachePath + "/"; } }
	string domainResPath { get { return "Day7AD/" + mBrand + "/"; } }
#endregion

#region member var
	public string mLanguage { get; private set; }
	int addListCnt = 9;
	Brand mBrand;
	Orientation mOrientation;
	///광고 초기화 성공 유무.
	[HideInInspector]
	public bool isInitSuccess = false;
	[HideInInspector]
	public bool isClosedADFront = false;
	// 앱정보
	AppInfo mAppInfo;

	// 광고정보
	List<ADInfo> mFrontAdInfoArr; // 전면
	List<ADInfo> mMainAdInfoArr; // 메인아이콘
	List<ADInfo> mMidAdInfoArr; // 중간
	//List<ADInfo> mEndAdInfoDay7Arr; // 종료아이콘 Day7
	//List<ADInfo> mEndAdInfo21gArr; // 종료아이콘 21g
	List<ADInfo> mEndAdInfoArr; // 종료아이콘

	bool isLodingList;
	bool mNoticeOpenFlag = false;
#endregion

#region inspector
	public bool mEditorView;
	public bool mDebugLog;
	public CanvasScaler cs;

	public GameObject mFront;
	public GameObject mNotice;
	public GameObject mMid;
	public GameObject mMainIcon;
	public GameObject[] mEnd;

	// Brand
	public Image[] mFrontImage;
	public Image[] mMidImage;
	public Image[] mMainIconImage;
	public Image[] mEndPortImage;
	public Image[] mEndLandImage;

	// Toggle
	// public Toggle[] mEndPortTabs;
	// public GameObject[] mEndPortTabScroll;
	// public Toggle[] mEndLandTabs;
	// public GameObject[] mEndLandTabScroll;

	// graphic image
	public RawImage[] mEndGraphicImage;

	// app list
	public GameObject mIconADPrefab;
	public Transform mMainIconParent;
	public Transform mEndPortParent;
	public Transform mEndLandParent;
	
#endregion

	void Awake()
	{
		DontDestroyOnLoad(transform.parent);
	}

#region func
	/// <summary>
	/// 라이브러리 초기화
	/// </summary>
	/// <param name="brand">Day7, 21g, 0back</param>
	/// <param name="orientation">Portrait, Landscape</param>
	/// <param name="screenResolution">해상도 new Vector2(720, 1280)</param>
	/// <param name="lang">한국, 영어, 일본, 대만 - ChineseTraditional, 중국 - ChineseSimplifie</param>
	public void Init(Day7ADLib.Brand brand, Orientation orientation, Vector2 screenResolution, SystemLanguage lang)
	{
		if (!Instance) { return; }
		if(isInitSuccess) { return; }

		this.mBrand = brand;
		this.mOrientation = orientation;
		if(orientation == Orientation.Portrait) addListCnt = mPortraitAddListCnt;
		else addListCnt = mLandscapeAddListCnt;
		this.cs.referenceResolution = screenResolution;
#if UNITY_IOS
		if (lang == SystemLanguage.Chinese) {
			lang = SystemLanguage.ChineseTraditional;
		}
#endif
		this.mLanguage = Instance.GetLanguageCode(lang);

		if(mDebugLog) Debug.LogError("language:" + this.mLanguage);

		StartCoroutine(InitAdForCorutine(orientation));

	}

	public string GetLanguageCode(SystemLanguage language)
	{
		string languageCode = "KO";
		switch (language)
		{
			case SystemLanguage.Korean:
				///한국어
				languageCode = "KO";
				break;
			case SystemLanguage.Japanese:
				///일본어
				languageCode = "JA";
				break;
			case SystemLanguage.English:
				///영어
				languageCode = "EN";
				break;
#if UNITY_IOS
			case SystemLanguage.Chinese:
#endif
			case SystemLanguage.ChineseTraditional:
				///번체(대만, 홍콩, 중국 등)
				languageCode = "ZH-CHT";
				break;
			case SystemLanguage.ChineseSimplified:
                ///간체(중국)
                languageCode = "ZH-CHS";
                break;
		}
		return languageCode;
	}

	void CachingImg(List<ADInfo> arrData, int dataNo, bool graphicImgFlag = false)
	{
		if(arrData.Count > 0)
		{
			if(mDebugLog) Debug.LogError(dataNo + " / " + arrData[dataNo]);
			string tempUrl = arrData[dataNo].appImageUrl;
			if(graphicImgFlag) tempUrl = arrData[dataNo].appGraphicImageUrl;
			// imageUrl 값이 없을수도 있다
			string[] s = tempUrl.Split('/');
			if(s.Length > 4) // 예외처리
			{
				string fileName = s[s.Length - 1];
				StartCoroutine(_CachingImg(tempUrl, fileName));
			}
		}
	}

	IEnumerator _CachingImg(string imageUrl, string fileName)
	{
		if(!File.Exists(string.Format("{0}{1}", cachePath, fileName)))
		{
			// 없으면 저장
			WWW www = new WWW(imageUrl);
			yield return www;
			if (!www.responseHeaders["STATUS"].Contains("200")) { print("STATUS=" + www.responseHeaders["STATUS"]); }

			if (www.error == null && www.texture != null && www.responseHeaders.Count > 0 && www.responseHeaders["STATUS"].Contains("200"))
			{
				File.WriteAllBytes(string.Format("{0}{1}", cachePath, fileName), www.bytes);
			}
			www.Dispose();
		}
	}

	Texture2D GetTexture(string fileName)
	{
		Texture2D tex = new Texture2D(2, 2, TextureFormat.RGBA32, false);
		tex.LoadImage(File.ReadAllBytes(string.Format("{0}/{1}", Application.temporaryCachePath, fileName)));
		return tex;
	}
		
#endregion

#region www
	int[] mCachingImgNos = new int[4];
	int retry = 3;
	/// <summary>
	/// 광고 전체 목록
	/// </summary>
	/// <param name="orientation"></param>
	/// <returns></returns>
	IEnumerator InitAdForCorutine(Orientation orientation)
	{
        while (retry > 0) {
			yield return StartCoroutine(GetADInfoList(orientation));
            retry--;

            if (isInitSuccess) {
                break;
            }

            yield return new WaitForSeconds(1.0f);
        }

        if (InitAdComplete != null) {
            InitAdComplete(isInitSuccess);
        }

		// 기본 UI셋팅
		SetBrandUI();
	}
	IEnumerator GetADInfoList(Orientation orientation)
	{
		WWWForm form = new WWWForm();
		form.AddField("appPackage", Application.identifier);
	#if UNITY_EDITOR || UNITY_ANDROID
		form.AddField("appOs", "Android");
	#elif UNITY_IPHONE
		form.AddField("appOs", "IOS");
	#endif
		form.AddField("appOrientation", orientation.ToString());
		form.AddField("appLanguag", mLanguage);

		var time = Time.time;

		WWW www = new WWW(mApiUrl, form);

		if(mDebugLog) print(www.url + " " + WWW.UnEscapeURL(System.Text.Encoding.UTF8.GetString(form.data)));

		float sendTemer = Time.time;
        do  
        {  
            if (Time.time > (sendTemer + 5.0f))
            {
                Debug.Log ("[SERVER] Failed Send OverTimer");
                yield break;
            }
            yield return null;
        }  
        while(!www.isDone);

		if (www.error != null) { Debug.LogError(www.error); yield break; }

		JSONObject json = new JSONObject(www.text);

		if(mDebugLog) print(json.Print(true));

		// json 데이터를 정리한다.
		JSONObject dataJson = json["data"];
		if(mDebugLog)  print(dataJson.Print(true));
		if(dataJson["appId"].str == null) yield break;
		// 앱 정보 저장
		mAppInfo = new AppInfo(dataJson["appId"].str, dataJson["appVersion"].str, dataJson["appNotice"].str, dataJson["appNoticeStartDate"].str, dataJson["appNoticeEndDate"].str, dataJson["appNoticeImageUrl"].str, dataJson["serverTime"].str);

		// appNotice 값이 true 이면 이미지를 캐싱한다.
		if(mAppInfo.appNotice)
		{
			string[] s = mAppInfo.appNoticeImageUrl.Split('/');
			string fileName = s[s.Length - 1];
			StartCoroutine(_CachingImg(mAppInfo.appNoticeImageUrl, fileName));
		}

		// 광고별 리스트 정리
		// 리스트 정리와 함께 설치되어있는 앱인지 판단
		// 전면광고
		JSONObject frontAds = dataJson["frontAd"];
		mFrontAdInfoArr = CheckAdInfoPackage(frontAds, ADType.front);
		// 정렬
		mFrontAdInfoArr.Sort(adCompare);
		// 이미지 캐싱
		// 우선 하나만 캐싱 하고 광고가 오픈될때 다음 이미지를 캐싱한다.
		CachingImg(mFrontAdInfoArr, mCachingImgNos[0]);

		// 메인아이콘광고
		// Day7, 21g 를 구분
		JSONObject mainAds = dataJson["mainAd"];
		mMainAdInfoArr = CheckAdInfoPackage(mainAds, ADType.main);
		// 정렬
		mMainAdInfoArr.Sort(adCompare);
		// 이미지 캐싱
		// 한 화면에 보여질 이미지만 우선 캐싱한다.
		// addListCnt 개수 만큼
		int mainAdListCount = (mMainAdInfoArr.Count < addListCnt) ? mMainAdInfoArr.Count : addListCnt;
		for (int i = 0; i < mainAdListCount; i++)
		{
			CachingImg(mMainAdInfoArr, mCachingImgNos[1]);
			if(i < mainAdListCount - 1) mCachingImgNos[1]++;
		}

		// 중간광고
		JSONObject midAds = dataJson["midAd"];
		mMidAdInfoArr = CheckAdInfoPackage(midAds, ADType.mid);
		// 정렬
		mMidAdInfoArr.Sort(adCompare);
		// 이미지 캐싱
		// 우선 하나만 캐싱 하고 광고가 오픈될때 다음 이미지를 캐싱한다.
		CachingImg(mMidAdInfoArr, mCachingImgNos[2]);

		// 종료광고
		// Day7, 21g 를 구분
		JSONObject endAds = dataJson["endAd"];
		mEndAdInfoArr = CheckAdInfoPackage(endAds, ADType.end);
		// 정렬
		mEndAdInfoArr.Sort(adCompare);
		// 한 화면에 보여질 이미지만 우선 캐싱한다.
		// addListCnt 개수 만큼
		int endAdListCount = (mEndAdInfoArr.Count < addListCnt) ? mEndAdInfoArr.Count : addListCnt;
		for (int i = 0; i < endAdListCount; i++)
		{
			CachingImg(mEndAdInfoArr, mCachingImgNos[3]);
			if(i < endAdListCount - 1) mCachingImgNos[3]++;
		}

		// 상단 그래필 이미지는 하나만 캐싱하면 된다.
		CachingImg(mEndAdInfoArr, 0, true);

		isInitSuccess = true;

		print("response time : " + (Time.time - time));
	}

	/// <summary>
	/// 통계저장
	/// </summary>
	/// <param name="appIds"></param>
	/// <param name="adType"></param>
	/// <param name="actionType"></param>
	/// <returns></returns>
	IEnumerator SetADStatistics(string appIds, string adType, string actionType)
	{
		WWWForm form = new WWWForm();
		form.AddField("appId", appIds);
		form.AddField("adType", adType);
		form.AddField("actionType", actionType);
	#if UNITY_EDITOR || UNITY_ANDROID
		form.AddField("os", "Android");
	#elif UNITY_IPHONE
		form.AddField("os", "IOS");
	#endif

		WWW www = new WWW(mStatisticsUrl, form);

		yield return www;
		if(mDebugLog) print(www.url + " " + www.responseHeaders["STATUS"]);
		www.Dispose();
	}

	/// <summary>
	/// 기본 베이스 UI를 설정한다.
	/// </summary>
	void SetBrandUI()
	{
		// 전면
		mFrontImage[0].sprite = Resources.Load<Sprite>(string.Format("{0}{1}", domainResPath, "btn_ad_popup_x"));
		mFrontImage[1].sprite = Resources.Load<Sprite>(string.Format("{0}{1}{2}", domainResPath, mLanguage, "/ad_popup_reservation"));

		// 중간
		mMidImage[0].sprite = Resources.Load<Sprite>(string.Format("{0}{1}", domainResPath, "btn_ad_popup_x"));
		mMidImage[1].sprite = Resources.Load<Sprite>(string.Format("{0}{1}{2}", domainResPath, mLanguage, "/ad_popup_reservation"));

		// 메인아이콘
		mMainIconImage[0].sprite = Resources.Load<Sprite>(string.Format("{0}{1}", domainResPath, "main_box"));
		mMainIconImage[1].sprite = Resources.Load<Sprite>(string.Format("{0}{1}{2}", domainResPath, mLanguage, "/main_box2"));
		mMainIconImage[1].SetNativeSize();
		mMainIconImage[2].sprite = Resources.Load<Sprite>(string.Format("{0}{1}{2}", domainResPath, mLanguage, "/main_btn"));
		mMainIconImage[2].SetNativeSize();

		// 종료
		if (mOrientation == Orientation.Portrait)
		{
			// 전면, 중간 이미지 사이즈 픽스
			mFront.transform.GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(620, 1102);
			mMid.transform.GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(620, 1102);

			mEndPortImage[0].sprite = Resources.Load<Sprite>(string.Format("{0}{1}{2}", domainResPath, mLanguage, "/endpopup_box"));
			mEndPortImage[1].sprite = Resources.Load<Sprite>(string.Format("{0}{1}", domainResPath, "endpopup_new"));
			mEndPortImage[2].sprite = Resources.Load<Sprite>(string.Format("{0}{1}{2}", domainResPath, mLanguage, "/btn_endpopup_cancel"));
			mEndPortImage[3].sprite = Resources.Load<Sprite>(string.Format("{0}{1}{2}", domainResPath, mLanguage, "/btn_endpopup_end"));
			mEndPortImage[4].sprite = Resources.Load<Sprite>(string.Format("{0}{1}{2}", domainResPath, mLanguage, "/btn_popup_close"));
			mEndPortImage[5].sprite = Resources.Load<Sprite>(string.Format("{0}{1}", domainResPath, "endpopup_box_list"));
		}
		else
		{
			// 전면, 중간 이미지 사이즈 픽스
			mFront.transform.GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(1102, 620);
			mMid.transform.GetChild(1).GetComponent<RectTransform>().sizeDelta = new Vector2(1102, 620);

			mEndLandImage[0].sprite = Resources.Load<Sprite>(string.Format("{0}{1}{2}", domainResPath, mLanguage, "/endpopup_box_width"));
			mEndLandImage[1].sprite = Resources.Load<Sprite>(string.Format("{0}{1}", domainResPath, "endpopup_new"));
			mEndLandImage[2].sprite = Resources.Load<Sprite>(string.Format("{0}{1}{2}", domainResPath, mLanguage, "/btn_endpopup_cancel"));
			mEndLandImage[3].sprite = Resources.Load<Sprite>(string.Format("{0}{1}{2}", domainResPath, mLanguage, "/btn_endpopup_end"));
			mEndLandImage[4].sprite = Resources.Load<Sprite>(string.Format("{0}{1}{2}", domainResPath, mLanguage, "/btn_popup_close"));
			mEndLandImage[5].sprite = Resources.Load<Sprite>(string.Format("{0}{1}", domainResPath, "endpopup_box_list"));
		}
	}

#region Notice
	public void ShowNotice(AniType aniType = AniType.none)
	{
		mNoticeOpenFlag = true;

		// 이미지 셋팅
		// gif 이미지인지 판단
		string fileName = GetImageFileName(mAppInfo.appNoticeImageUrl);
		string extensionName = fileName.Split('.')[1].ToLower();
		mNotice.transform.GetChild(1).GetComponent<RawImage>().texture = null;
		if("gif".Equals(extensionName))
		{
			// gif이미지
			mNotice.transform.GetChild(1).GetComponent<AdGifPlayer>().Init();
			mNotice.transform.GetChild(1).GetComponent<AdGifPlayer>().TargetComponent = mNotice.transform.GetChild(1).GetComponent<RawImage>();
			mNotice.transform.GetChild(1).GetComponent<AdGifPlayer>().Load(File.ReadAllBytes(string.Format("{0}/{1}", Application.temporaryCachePath, fileName)));
			mNotice.transform.GetChild(1).GetComponent<AdGifPlayer>().Play();
		}
		else
		{
			mNotice.transform.GetChild(1).GetComponent<RawImage>().texture = GetTexture(fileName);
		}

		NGUIUICameraInput(false);
		mNotice.SetActive(true);

		if(aniType != AniType.none) mNotice.GetComponent<Animation>().Play(aniType.ToString());
	}

	public void HideNotice() { StartCoroutine(_CloseNotice()); }
	IEnumerator _CloseNotice()
	{
		yield return new WaitForSeconds(0.25f);

		if (!mNotice.activeSelf) { yield break; }
		mNotice.SetActive(false);
		NGUIUICameraInput(true);
	}

	public bool IsNoticeActive()
	{
		if(mNotice == null) return false;
		return mNotice.activeSelf;
	}
#endregion

#region Front AD, Mid AD
	private static string Ad_FrontShowCount = "Ad_FrontShowCount";
	private static string Ad_MidShowCount = "Ad_MidShowCount";
	public void ShowADFront(AniType aniType = AniType.none)
	{
		#if UNITY_EDITOR
		if (!mEditorView) { return; }
		#endif
		if (Day7ADLib.Instance.GetAdFree()) { return; }
		if (!Day7ADLib.Instance.isUseDay7Ad()) { return; }
		if(isClosedADFront) { return; }

		// 공지팝업이 있는지 확인
		if(mAppInfo.appNotice && !mNoticeOpenFlag)
		{
			bool tempFlag = false;
			// 오픈 시간 체크
			DateTime t1 = DateTime.Parse(mAppInfo.serverTime); // 서버시간
			DateTime t2 = DateTime.Parse(mAppInfo.appNoticeStartDate); // 오픈 시작시간
			DateTime t3 = DateTime.Parse(mAppInfo.appNoticeEndDate); // 오픈 종료시간

			// t1.CompareTo(t2) // - : 서버시간이 시작시간 이전, + : 서버시간이 시작시간이 이후
			// t1.CompareTo(t3) // - : 서버시간이 종료시간 이전, + : 서버시간이 종료시간이 이후
			if(t1.CompareTo(t2) > 0 && t1.CompareTo(t3) < 0) tempFlag = true;

			if(tempFlag)
			{
				ShowNotice(aniType); 
				return;
			}
		}

		StartCoroutine(_ShowADFront(aniType));
	}

	IEnumerator _ShowADFront(AniType aniType = AniType.none)
	{
		// 보여줄 광고를 선별
		ADInfo frontAd = GetAdInfo(ADType.front, mFrontAdInfoArr, Ad_FrontShowCount);
		
		// 이미지가 누락되는 경우 서버에서 다시 받아야한다.
		string tempUrl = frontAd.appImageUrl;
		string[] s = tempUrl.Split('/'); // imageUrl 값이 없을수도 있다
		if(s.Length > 4) // 예외처리
		{
			string fileName = s[s.Length - 1];
			yield return StartCoroutine(_CachingImg(tempUrl, fileName));
		}

		SetADFrontMidImage(mFront, frontAd, aniType);
		AddListener(mFront.transform.GetChild(1).GetComponent<Button>(), frontAd, ADType.front);

		StartCoroutine(SetADStatistics(frontAd.appId, "front", "View"));

		mCachingImgNos[0]++;
		if(mCachingImgNos[0] < mFrontAdInfoArr.Count)
		{
			CachingImg(mFrontAdInfoArr, mCachingImgNos[0]);
		}
	}

	public void HideADFront() { StartCoroutine(_CloseADFront()); }
	IEnumerator _CloseADFront()
	{
		isClosedADFront = true;
		yield return new WaitForSeconds(0.25f);

		if (!mFront.activeSelf) { yield break; }
		mFront.SetActive(false);
		NGUIUICameraInput(true);
	}

	public bool IsAdFrontActive()
	{
		if(mFront == null) return false;
		return mFront.activeSelf;
	}

	public void ShowADMid(AniType aniType = AniType.none)
	{
		#if UNITY_EDITOR
		if (!mEditorView) { return; }
		#endif
		if (Day7ADLib.Instance.GetAdFree()) { return; }
		if (!Day7ADLib.Instance.isUseDay7Ad()) { return; }

		StartCoroutine(_ShowADMid(aniType));
	}

	IEnumerator _ShowADMid(AniType aniType = AniType.none)
	{
		// 보여줄 광고를 선별
		ADInfo midAd = GetAdInfo(ADType.mid, mMidAdInfoArr, Ad_MidShowCount);
		
		// 이미지가 누락되는 경우 서버에서 다시 받아야한다.
		string tempUrl = midAd.appImageUrl;
		string[] s = tempUrl.Split('/'); // imageUrl 값이 없을수도 있다
		if(s.Length > 4) // 예외처리
		{
			string fileName = s[s.Length - 1];
			yield return StartCoroutine(_CachingImg(tempUrl, fileName));
		}

		SetADFrontMidImage(mMid, midAd, aniType);
		AddListener(mMid.transform.GetChild(1).GetComponent<Button>(), midAd, ADType.mid);

		StartCoroutine(SetADStatistics(midAd.appId, "mid", "View"));

		mCachingImgNos[2]++;
		if(mCachingImgNos[2] < mMidAdInfoArr.Count)
		{
			CachingImg(mMidAdInfoArr, mCachingImgNos[2]);
		}
	}

	public void HideADMid() { StartCoroutine(_CloseADMid()); }
	IEnumerator _CloseADMid()
	{
		yield return new WaitForSeconds(0.25f);

		if (!mMid.activeSelf) { yield break; }
		mMid.SetActive(false);
		NGUIUICameraInput(true);
	}

	public bool IsAdMidActive()
	{
		if(mMid == null) return false;
		return mMid.activeSelf;
	}

	void SetADFrontMidImage(GameObject obj, ADInfo adInfo, AniType aniType)
	{
		// 사전예약 여부
		obj.transform.GetChild(1).GetChild(1).gameObject.SetActive(adInfo.appReservation);
		
		// 이미지 셋팅
		// gif 이미지인지 판단
		string fileName = GetImageFileName(adInfo.appImageUrl);
		string extensionName = fileName.Split('.')[1].ToLower();
		obj.transform.GetChild(1).GetComponent<RawImage>().texture = null;
		if("gif".Equals(extensionName))
		{
			// gif이미지
			obj.transform.GetChild(1).GetComponent<AdGifPlayer>().Init();
			obj.transform.GetChild(1).GetComponent<AdGifPlayer>().TargetComponent = obj.transform.GetChild(1).GetComponent<RawImage>();
			obj.transform.GetChild(1).GetComponent<AdGifPlayer>().Load(File.ReadAllBytes(string.Format("{0}/{1}", Application.temporaryCachePath, fileName)));
			obj.transform.GetChild(1).GetComponent<AdGifPlayer>().Play();
		}
		else
		{
			obj.transform.GetChild(1).GetComponent<RawImage>().texture = GetTexture(fileName);
		}

		NGUIUICameraInput(false);
		obj.SetActive(true);

		if(aniType != AniType.none) obj.GetComponent<Animation>().Play(aniType.ToString());
	}

#endregion

#region MainIcon AD
	ADType iconItemsType;
	public void ShowADMainIcon()
	{
		#if UNITY_EDITOR
		if (!mEditorView) { return; }
		#endif
		if (Day7ADLib.Instance.GetAdFree()) { return; }
		if (!Day7ADLib.Instance.isUseDay7Ad()) { return; }

		mMainIconParent.GetComponent<RectTransform> ().anchoredPosition = new Vector2 (0, 0);
		
		if (mMainIconParent.childCount != 0)
		{
			mMainIcon.SetActive(true);
			mMainIconImage[0].rectTransform.anchoredPosition = new Vector2(mMainIconImage[0].rectTransform.anchoredPosition.x, 0);
			return;
		}

		iconItemsType = ADType.main;
		StartCoroutine(UpdateIconADList(mMainAdInfoArr, mMainIconParent));
		
		mMainIcon.SetActive(true);
		mMainIconImage[0].rectTransform.anchoredPosition = new Vector2(mMainIconImage[0].rectTransform.anchoredPosition.x, 0);
	}

	public void HideADMainIcon()
	{
		mMainIcon.SetActive(false);
	}

	IEnumerator UpdateIconADList(List<ADInfo> adInfo, Transform parent)
	{
		string tempStr = "";
		int startIdx = parent.childCount;
		if (adInfo == null || startIdx >= adInfo.Count) { yield break; }
		if(mDebugLog) print("UpdateIconADList");
		isLodingList = true;
		for (int i = startIdx; (i == startIdx || i % addListCnt != 0) && i < adInfo.Count; i++)
		{
			ADInfo adItem = adInfo[i];

			GameObject itemObj = Instantiate(mIconADPrefab);
			itemObj.name = adItem.appId;
			string fileName = GetImageFileName(adItem.appImageUrl);
			itemObj.transform.GetChild(0).GetComponent<RawImage>().texture = GetTexture(fileName);
			itemObj.transform.GetChild(1).GetComponent<Text>().text = adItem.appName;
			itemObj.transform.GetChild(0).GetChild(0).gameObject.SetActive(adItem.appReservation);
			AddListener(itemObj.GetComponent<Button>(), adItem, iconItemsType);

			itemObj.transform.SetParent(parent);
			itemObj.GetComponent<RectTransform> ().anchoredPosition3D = Vector3.zero;
			itemObj.GetComponent<RectTransform> ().localScale = Vector3.one;

			if(iconItemsType == ADType.main)
			{
				if(mCachingImgNos[1] < adInfo.Count - 1) mCachingImgNos[1]++;
				CachingImg(adInfo, mCachingImgNos[1]);
			}
			else if(iconItemsType == ADType.end)
			{
				if(mCachingImgNos[3] < adInfo.Count - 1) mCachingImgNos[3]++;
				CachingImg(mEndAdInfoArr, mCachingImgNos[3]);
			}

			tempStr += string.Format("{0}{1}", adItem.appId, ",");

			yield return null;
		}
		isLodingList = false;

        // appId 를 , 를 붙여 한번에 전송
        if(!"".Equals(tempStr)) StartCoroutine(SetADStatistics(tempStr, iconItemsType.ToString(), "View"));
	}

	public void UpdateIconADList(Vector2 v2)
	{
		if (isLodingList) { return; }

		switch (iconItemsType)
		{
			case ADType.main:
				if (v2.x >= 1)
				{
					StartCoroutine(UpdateIconADList(mMainAdInfoArr, mMainIconParent));
				}
				break;
			case ADType.end:
				if(mOrientation == Orientation.Portrait)
				{
					if (v2.y <= 0) StartCoroutine(UpdateIconADList(mEndAdInfoArr, mEndPortParent));
				}
				else
				{
					if (v2.x >= 1) StartCoroutine(UpdateIconADList(mEndAdInfoArr, mEndLandParent));
				}
				break;
			default:
				break;
		}
	}

	public void ShowADOtherApps()
	{
		#if UNITY_EDITOR
		if (!mEditorView) { return; }
		#endif

		EndAdBtnView(mEnd[(int)mOrientation], true);
		StartCoroutine(_ShowADEnd());
	}

	void EndAdBtnView(GameObject adObj, bool otherAppFlag)
	{
		adObj.transform.GetChild(1).GetChild(2).gameObject.SetActive(!otherAppFlag);
		adObj.transform.GetChild(1).GetChild(3).gameObject.SetActive(otherAppFlag);
	}
#endregion

#region End AD
	public void ShowADEnd()
	{
		#if UNITY_EDITOR
		if (!mEditorView) { return; }
		#endif

		EndAdBtnView(mEnd[(int)mOrientation], false);
		StartCoroutine(_ShowADEnd());
	}
	IEnumerator _ShowADEnd()
	{
		if (Day7ADLib.Instance.GetAdFree()) { Quit(); }
		if (!Day7ADLib.Instance.isUseDay7Ad()) { yield break; }
		if (mEnd[(int)mOrientation].activeSelf) { CloseADEnd(); yield break; }
		
		// 상단광고
		mEndGraphicImage[(int)mOrientation].texture = null;
		mEndGraphicImage[(int)mOrientation].texture = GetTexture(GetImageFileName(mEndAdInfoArr[0].appGraphicImageUrl));
		AddListener(mEndGraphicImage[(int)mOrientation].GetComponent<Button>(), mEndAdInfoArr[0], ADType.end);
		
		iconItemsType = ADType.end;
		if(mOrientation == Orientation.Portrait)
		{
			if(mEndPortParent.childCount != 0)
			{
				NGUIUICameraInput(false);
				mEnd[(int)mOrientation].SetActive(true);
				yield break;
			}
			yield return StartCoroutine(UpdateIconADList(mEndAdInfoArr, mEndPortParent));
		}
		else
		{
			if(mEndLandParent.childCount != 0)
			{
				NGUIUICameraInput(false);
				mEnd[(int)mOrientation].SetActive(true);
				yield break;
			}
			yield return StartCoroutine(UpdateIconADList(mEndAdInfoArr, mEndLandParent));
		}
		
		NGUIUICameraInput(false);
		mEnd[(int)mOrientation].SetActive(true);
	}

	void CloseADEnd()
	{
		NGUIUICameraInput(true);
		mEnd[(int)mOrientation].SetActive(false);
	}
#endregion

	string GetImageFileName(string imageUrl)
	{
		string[] s = imageUrl.Split('/');
		string fileName = s[s.Length - 1];
		return fileName;
	}

	void AddListener(Button b, ADInfo adInfo, ADType adType)
	{
		b.onClick.RemoveAllListeners();
		b.onClick.AddListener(() => OpenURL(adInfo, adType));
	}

	public void OpenURL(ADInfo adInfo, ADType adType)
	{
		if(mDebugLog) print("openURL : " + adInfo.urlLink);
		Application.OpenURL(adInfo.urlLink);

		if (adType == ADType.front) HideADFront();
        else if (adType == ADType.mid) HideADMid();

		// 통계
		if(!adInfo.appReservation) Day7AdPlugin.Instance.appInstallReady(adInfo.appId, adType.ToString(), adInfo.appPackage);
	}

	public void Quit()
	{
		Application.Quit();
	}

	ADInfo GetAdInfo(ADType type, List<ADInfo> adInfoArr, string repeatCountKey)
	{
		string s = PlayerPrefs.GetString(type.ToString(), "");
		string[] sArr = s.Split('_');

		List<ADInfo> copyAdInfoArr = new List<ADInfo>(adInfoArr.ToArray()); // List 복제

		ADInfo adInfo = copyAdInfoArr[0]; // 초기화
		foreach (var item in sArr) 
		{
			copyAdInfoArr.RemoveAll(t => t.appId == item);
		}

		// 리스트에 있는 광고가 모두 노출 되었을 때
		if (copyAdInfoArr.Count <= 0) 
		{ 
			s = adInfo.appId; 
		} 
		else 
		{ 
			adInfo = copyAdInfoArr[0];
			bool tempFlag = false;
			if(adInfo.appRepeatCount > 1)
			{
				// 저장되어있는 RepeatCount 를 확인
				int tempCount = PlayerPrefs.GetInt(repeatCountKey, 0);
				if(tempCount > 0)
				{
					tempCount--;
					PlayerPrefs.SetInt(repeatCountKey, tempCount);
					if(tempCount <= 0) tempFlag = true;
				}
				else
				{
					tempCount = adInfo.appRepeatCount;
					tempCount--;
					PlayerPrefs.SetInt(repeatCountKey, tempCount);
				}
			}
			else
			{
				tempFlag = true;
			}

			if(tempFlag) s += "_" + adInfo.appId;
			
		}

		// 노출 광고코드 저장
		PlayerPrefs.SetString(type.ToString(), s);

		return adInfo;
	}

	// 배열값중 이미 설치되어있는 앱의 정보를 정리
	List<ADInfo> CheckAdInfoPackage(JSONObject json, ADType adType)
	{
		List<ADInfo> rtnArr = new List<ADInfo>();
		for (int i = 0; i < json.Count; i++)
		{
			string appPackage = json[i]["appPackage"].str;
		#if UNITY_IPHONE
			appPackage = (json[i]["urlSchemes"].str != null) ? json[i]["urlSchemes"].str : "";
		#endif
			bool checkFlag = Day7AdPlugin.Instance.isInstalledApplication(appPackage);
			if(mDebugLog) Debug.LogError(adType.ToString() + " / " + json[i]["appPackage"].str + " / " + checkFlag);
			if(!checkFlag)
			{
				rtnArr.Add(new ADInfo(ADType.front, (int)json[i]["orderNo"].n, json[i]["appId"].str, json[i]["appName"].str, json[i]["appBrand"].str, json[i]["appPackage"].str, json[i]["urlSchemes"].str, json[i]["appReservation"].str, (int)json[i]["appFirstOrder"].n, (int)json[i]["appRepeatCount"].n, json[i]["appGraphicImageUrl"].str, json[i]["appImageUrl"].str, json[i]["urlLink"].str));
			}
		}
		return rtnArr;
	}
	
	public static int adCompare(ADInfo o1, ADInfo o2)
    {
        if (o1 != null && o2 != null)
        {
            if (o1.appFirstOrder > o2.appFirstOrder)
            {
                return -1;
            }
            else if (o1.appFirstOrder < o2.appFirstOrder)
            {
                return 1;
            }
            else
            {
				if (o1.orderNo > o2.orderNo)
				{
					return -1;
				}
				else if (o1.orderNo < o2.orderNo)
				{
					return 1;
				}
				else
				{
					return 0;
				}
            }
        }
        else
        {
            return 0;
        }

    }

	/// <summary>
	/// NGUI 사용시 UICamera 의 터치, 클릭 리스너를 on/off 시킨다.
	/// </summary>
	/// <param name="on"></param>
	public void NGUIUICameraInput(bool on) { StartCoroutine(_NGUIUICameraInput(on)); }
	IEnumerator _NGUIUICameraInput(bool on)
	{
		yield return null;
#if !NONGUI && !UNITY_EDITOR
		foreach (var item in FindObjectsOfType<UICamera>())
		{
			item.useMouse = on;
			item.useTouch = on;
		}
#endif
	}

#endregion

#region SDK Method
	///오퍼월 버전을 받아온다.
	public string getAppVersion() {
		if (!isInitSuccess)
			return "";
			
		return mAppInfo.appVersion;
	}

	public bool isUseDay7Ad() {
		if (!isInitSuccess)
			return false;

#if UNITY_EDITOR || UNITY_ANDROID
		return true;
#elif UNITY_IPHONE
		bool isUseDay7Ad = false;
		if (Day7AdPlugin.IOSInterface.getBundleVersion().CompareTo(getAppVersion()) > 0) {
			isUseDay7Ad = false;
		}
		else {
			isUseDay7Ad = true;
		}
		return isUseDay7Ad;
#endif
	}

	/// <summary>
	/// AdFree
	/// </summary>
	private static string BecomeAd_Free = "BecomeAd_Free"; // 구 키값
    private static string Ad_Free = "Ad_Free";
    public void SetAdFree () {
        PlayerPrefs.SetInt (Ad_Free, 1);
    }
    public bool GetAdFree () {
        string tempKeyStr = Ad_Free;
        if(PlayerPrefs.HasKey(BecomeAd_Free)) tempKeyStr = BecomeAd_Free;
        int adFree = PlayerPrefs.GetInt (tempKeyStr, 0);
		Debug.Log("저장키 : " + tempKeyStr + " 값 : " + adFree);
        if (adFree == 1) {
            return true;
        }else {
            return false;   
        }
    }
#endregion

#region Info class
	[System.Serializable]
	public class ADInfo
	{
		public int orderNo;
		public string appId;
		public string appName;
		public string appBrand;
		public string appPackage;
		public string urlSchemes;
		public bool appReservation;
		public int appFirstOrder;
		public int appRepeatCount;
		public string appGraphicImageUrl;
		public string appImageUrl;
		public string urlLink;

		public ADType type;

		public ADInfo() { }
		public ADInfo(ADType type, int orderNo, string appId, string appName, string appBrand, string appPackage, string urlSchemes, string appReservation, int appFirstOrder, int appRepeatCount, string appGraphicImageUrl, string appImageUrl, string urlLink)
		{
			this.type = type;
			this.orderNo = orderNo;
			this.appId = appId;
			this.appName = appName;
			this.appBrand = appBrand;
			this.appPackage = appPackage;
			this.urlSchemes = urlSchemes;
			this.appReservation = "Y".Equals(appReservation) ? true : false;
			this.appFirstOrder = appFirstOrder;
			this.appRepeatCount = appRepeatCount;
			this.appGraphicImageUrl = appGraphicImageUrl;
			this.appImageUrl = appImageUrl;
			this.urlLink = urlLink;
		}
	}

	[System.Serializable]
	public class AppInfo
	{
		public string appId;
		public string appVersion;
		public bool appNotice;
		public string appNoticeStartDate;
		public string appNoticeEndDate;
		public string appNoticeImageUrl;
		public string serverTime;

		public AppInfo() { }
		public AppInfo(string appId, string appVersion, string appNotice, string appNoticeStartDate = null, string appNoticeEndDate = null, string appNoticeImageUrl = null, string serverTime = null)
		{
			this.appId = appId;
			this.appVersion = appVersion;
			this.appNotice = "Y".Equals(appNotice) ? true : false;
			this.appNoticeStartDate = appNoticeStartDate;
			this.appNoticeEndDate = appNoticeEndDate;
			this.appNoticeImageUrl = appNoticeImageUrl;
			this.serverTime = serverTime;
		}
	}
#endregion

}