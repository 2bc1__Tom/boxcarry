﻿/********************************************************************
	Created by Kei on 2018.04
	Copyright © 2018년 Kei. All rights reserved.
*********************************************************************/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;
using Day7SDK;

public class Day7AdPlugin : Singleton<Day7AdPlugin>
{
	public enum PERMISSION {
		GET_ACCOUNTS,
		READ_PHONE_STATE,
		WRITE_EXTERNAL_STORAGE
	};

	void OnApplicationPause(bool pause) {
		if (pause) {
		}
		else {
	#if UNITY_EDITOR
	#elif UNITY_IPHONE
		IOSInterface.onForeground();
	#endif
		}
	}
	
	/// <summary>
	/// 앱설치 유무를 체크
	/// </summary>
	/// <param name="packageName">안드로이드 : packageName, 아이폰 : urlSchemes</param>
	/// <returns></returns>
	public bool isInstalledApplication(string packageName)
	{
	#if UNITY_EDITOR
		return false;
	#elif UNITY_ANDROID
		return AndroidInterface.isInstalledApplication(packageName);
	#elif UNITY_IPHONE
		return IOSInterface.isInstalledApplication(packageName);
	#endif
	}

	public void appInstallReady(string appId, string adType, string packageName)
	{
	#if UNITY_ANDROID
		AndroidInterface.appInstallReady(appId, adType, packageName);
	#elif UNITY_IPHONE
		IOSInterface.appInstallReady(appId, adType, packageName);
	#endif
	}

	string GetPermissionString(PERMISSION permission)
	{
		return string.Format("android.permission.{0}", permission.ToString());
	}
	
#region Interface Class
	public class AndroidInterface
	{
		#if UNITY_ANDROID
			///안드로이드 브릿지
			private static AndroidJavaObject _android = null;
			public static AndroidJavaObject Android {
				get {
					if (Application.platform == RuntimePlatform.Android) {
						if (_android == null) {
							_android = new AndroidJavaObject ("com.day7.library.Day7AdLibrary");
						}
					}			
					return _android;
				}
			}

			public static bool isInstalledApplication(string packageName) {
				return Android.Call<bool> ("isInstalledApplication", packageName);
			}

			public static void appInstallReady(string appId, string adType, string packageName) {
				Android.Call("appInstallReady", appId, adType, packageName);
			}

			///퍼미션이 허락되어있는지 체크.
			public static bool checkPermission() {
				return Android.Call<bool> ("checkPermission");
			}

			///퍼미션 허락을 요청.
			public static void requestPermission() {
				Android.Call ("requestPermission");
			}

			public static bool checkPermission(params PERMISSION[] permissions) {
				if(permissions.Length > 0)
				{
					string[] arrStr = new string[permissions.Length];
					for (int i = 0; i < permissions.Length; i++)
					{
						arrStr[i] = Day7AdPlugin.Instance.GetPermissionString(permissions[i]);
					}
					#if UNITY_EDITOR
					return false;
					#elif UNITY_ANDROID
					return Android.Call<bool> ("checkPermissionArr", string.Join(",", arrStr));
					#endif
				}
				return true;
			}

			public static void requestPermission(params PERMISSION[] permissions) {
				if(permissions.Length > 0)
				{
					string[] arrStr = new string[permissions.Length];
					for (int i = 0; i < permissions.Length; i++)
					{
						arrStr[i] = Day7AdPlugin.Instance.GetPermissionString(permissions[i]);
					}
					#if !UNITY_EDITOR
					Android.Call ("requestPermissionArr", string.Join(",", arrStr));
					#endif
				}
			}
		#endif
	}
	public class IOSInterface
	{
		#if UNITY_IPHONE
		[DllImport("__Internal")]
		public static extern bool isInstalledApplication(string urlSchemes);

		[DllImport("__Internal")]
		public static extern void appInstallReady(string appId, string adType, string urlSchemes);		

		[DllImport("__Internal")]
		public static extern string getBundleId();

		[DllImport("__Internal")]
		public static extern string getBundleVersion();

		[DllImport("__Internal")]
		public static extern string getIDFA();

		[DllImport("__Internal")]
		public static extern void onForeground();
		#endif
	}
#endregion
}
