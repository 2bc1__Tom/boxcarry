//
//  Day7AdLibrary.m
//
//  Created by Kei on 2018.04
//  Copyright © 2018년 Kei. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
#import <MessageUI/MessageUI.h>
#import "Day7Ad.h"

static Day7Ad* day7Ad = NULL;

extern "C"
{
    ///문자열 복사
    char* cStringCopy(const char* string) {
        if (string == NULL)
            return NULL;
        
        char* res = (char*)malloc(strlen(string) + 1);
        strcpy(res, string);
        
        return res;
    }

    /// 앱 설치 유무 확인
    bool isInstalledApplication(const char* urlSchemes)
    {
        NSString *urlSchemesStr = [NSString stringWithUTF8String:urlSchemes];
        
        Boolean isInstalled = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@://", urlSchemesStr]]];

        if(isInstalled)
        {
            return 1;
        }
        else
        {
            return 0;
        }
    }
    
    ///앱을 다운받기 위해 마켓으로 이동..
    void appInstallReady(const char* appId, const char* adType, const char* urlSchemes) {
        NSString *appIdString = [NSString stringWithUTF8String:appId];
        NSString *adTypeString = [NSString stringWithUTF8String:adType];
        NSString *urlSchemesString = [NSString stringWithUTF8String:urlSchemes];
        
        if(day7Ad == nil) day7Ad = [[Day7Ad alloc] init];
        
        [day7Ad SetAppId:appIdString];
        [day7Ad SetAdType:adTypeString];
        [day7Ad SetUrlSchemes:urlSchemesString];
        
        [day7Ad statistics:appIdString withAdType:adTypeString withActionType:@"Click"];
    }
    
    ///앱이 활성화 되었을때 날려준다.
    void onForeground() {
        if(day7Ad != nil) {
            NSString *appId = [day7Ad mAppId];
            NSString *adType = [day7Ad mAdType];
            NSString *urlSchemes = [day7Ad mUrlSchemes];
            
            if([appId length] > 0) {
                Boolean isInstalled = [[UIApplication sharedApplication] canOpenURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@://", urlSchemes]]];
                
                if(!isInstalled) {
                    [day7Ad statistics:appId withAdType:adType withActionType:@"Down"];
                }
                
                [day7Ad SetAppId:@""];
                [day7Ad SetAdType:@""];
                [day7Ad SetUrlSchemes:@""];
            }
        }
    }

    char* getBundleId () {
        return cStringCopy([[[NSBundle mainBundle] bundleIdentifier] UTF8String]);
    }
    
    char* getBundleVersion () {
        NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
        return cStringCopy([version UTF8String]);
    }
    
    char* getIDFA () {
        if(day7Ad == nil) day7Ad = [[Day7Ad alloc] init];
        return cStringCopy([[day7Ad idfaString] UTF8String]);
    }
}
