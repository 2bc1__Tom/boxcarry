//
//  Day7Ad.h
//
//  Created by Kei on 2018.04
//  Copyright © 2018년 Kei. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Day7Ad : NSObject
{
    NSString *_appId;
    NSString *_adType;
    NSString *_urlSchemes;
}

-(NSString *) mAppId;
-(void)SetAppId:(NSString *)mAppId;

- (NSString*) mAdType;
-(void)SetAdType:(NSString *)mAdType;

- (NSString*) mUrlSchemes;
-(void)SetUrlSchemes:(NSString *)mUrlSchemes;

///통계 날리기.
- (void) statistics : (NSString*)code withAdType: (NSString*)adType withActionType : (NSString*)actionType;

- (NSString*) idfaString;
@end
