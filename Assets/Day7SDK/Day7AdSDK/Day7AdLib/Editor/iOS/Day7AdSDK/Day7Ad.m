//
//  Day7Ad.m
//
//  Created by Kei on 2018.04
//  Copyright © 2018년 Kei. All rights reserved.
//

#import "Day7Ad.h"
#import <AdSupport/AdSupport.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

static NSString* _Nonnull URL_STATISTICS = @"http://day7ad.day7games.com/day7Ad/statistics";

@interface Day7Ad ()

@end

@implementation Day7Ad

- (NSString *)mAppId {
    return _appId;
}
- (void)SetAppId: (NSString *)mAppId {
    _appId = [mAppId copy];
}

- (NSString *)mAdType {
    return _adType;
}
- (void)SetAdType: (NSString *)mAdType {
    _adType = [mAdType copy];
}

- (NSString *)mUrlSchemes {
    return _urlSchemes;
}
- (void)SetUrlSchemes: (NSString *)mUrlSchemes {
    _urlSchemes = [mUrlSchemes copy];
}

- (NSString*) idfaString {
    return [[[ASIdentifierManager sharedManager] advertisingIdentifier] UUIDString];;
}

///통계 날리기..
- (void) statistics : (NSString*)appId withAdType : (NSString*)adType withActionType: (NSString*)actionType {
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL_STATISTICS]];
    [urlRequest setHTTPMethod:@"POST"];
    [urlRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    ///param설정
    NSMutableArray *pairs = [[NSMutableArray alloc] init];
    [pairs addObject:[NSString stringWithFormat:@"%@=%@", @"appId", appId]];
    [pairs addObject:[NSString stringWithFormat:@"%@=%@", @"actionType", actionType]];
    [pairs addObject:[NSString stringWithFormat:@"%@=%@", @"adType", adType]];
    [pairs addObject:[NSString stringWithFormat:@"%@=%@", @"OS", @"IOS"]];
    
    ///중간에 넣을 특수문자..
    NSString *requestParameters = [pairs componentsJoinedByString:@"&"];
    [urlRequest setHTTPBody:[requestParameters dataUsingEncoding:NSUTF8StringEncoding]];
    
    ///통신 시작.
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:urlRequest completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
//        @try {
//        }
//        @catch (NSException *exception) {
//            NSLog(@"%@", exception.description);
//        }
//        @finally {
//        }
    }];
    
    [dataTask resume];
}

@end
