/********************************************************************
	Created by Kei on 2018.04
	Copyright © 2018년 Kei. All rights reserved.
*********************************************************************/
using UnityEngine;
using System.IO;
using System.Xml;
using System.Collections.Generic;
#if UNITY_IPHONE
using UnityEditor.iOS.Xcode;
#endif

namespace UnityEditor.Day7AdEditor
{
    public class PlistMod
    {        
        public static void UpdatePlist(string path)
        {
        #if UNITY_IPHONE
            // Get plist
            const string fileName = "Info.plist";
			string plistPath = Path.Combine(path, fileName);
			PlistDocument plist = new PlistDocument();
			plist.ReadFromString(File.ReadAllText(plistPath));

            // Get root
			PlistElementDict rootDict = plist.root;

            PlistElementDict appTransportSecurityDict = rootDict["NSAppTransportSecurity"] as PlistElementDict;
			if (appTransportSecurityDict == null)
            {
                appTransportSecurityDict = rootDict.CreateDict ("NSAppTransportSecurity");
                appTransportSecurityDict.SetBoolean("NSAllowsArbitraryLoads", true);
			}
            
            PlistElementArray applicationQueriesSchemesArray = rootDict["LSApplicationQueriesSchemes"] as PlistElementArray;
            if(applicationQueriesSchemesArray == null)
            {
                applicationQueriesSchemesArray = rootDict.CreateArray("LSApplicationQueriesSchemes");
            }
            for(int i = 0 ; i < 80 ; i++)
            {
                applicationQueriesSchemesArray.AddString(string.Format("become{0:D3}", i+1));
            }
            
            if(Day7SDKSettings.Instance.OfferwallTnkAPI)
            {
                if(!rootDict.values.ContainsKey("tnkad_app_id")) rootDict["tnkad_app_id"] = new PlistElementString(Day7SDKSettings.Instance.OfferwallTnkKey);
            }
            
            if(Day7SDKSettings.Instance.OfferwallAdpopcornLiveOps)
            {
                PlistElementArray requiredBackgroundArray = rootDict["Required background mode"] as PlistElementArray;
                if(requiredBackgroundArray == null)
                {
                    requiredBackgroundArray = rootDict.CreateArray("Required background modes");
                    requiredBackgroundArray.AddString("App downloads content in response to push notifications");
                }
            }

            // Privacy 설정
            if(!rootDict.values.ContainsKey("NSCalendarsUsageDescription")) rootDict["NSCalendarsUsageDescription"] = new PlistElementString("Calendars Usage Description");
            if(!rootDict.values.ContainsKey("NSCameraUsageDescription")) rootDict["NSCameraUsageDescription"] = new PlistElementString("For Social Sharing");
            if(!rootDict.values.ContainsKey("NSLocationWhenInUseUsageDescription")) rootDict["NSLocationWhenInUseUsageDescription"] = new PlistElementString("Location When In Use Usage Description");
            if(!rootDict.values.ContainsKey("NSBluetoothPeripheralUsageDescription")) rootDict["NSBluetoothPeripheralUsageDescription"] = new PlistElementString("Bluetooth Peripheral Usage Description");
            if(!rootDict.values.ContainsKey("NSPhotoLibraryUsageDescription")) rootDict["NSPhotoLibraryUsageDescription"] = new PlistElementString("For Social Sharing");
            if(!rootDict.values.ContainsKey("NSAppleMusicUsageDescription")) rootDict["NSAppleMusicUsageDescription"] = new PlistElementString("Media Library Usage Description");

            PlistElementArray bundleURLTypesArray = rootDict["CFBundleURLTypes"] as PlistElementArray;
			if (bundleURLTypesArray == null) {
			    bundleURLTypesArray = rootDict.CreateArray ("CFBundleURLTypes");
			}
			PlistElementDict dict = bundleURLTypesArray.AddDict ();
            dict.SetString("CFBundleTypeRole", "Editor");
			PlistElementArray bundleURLSchemesArray = dict.CreateArray ("CFBundleURLSchemes");
            bundleURLSchemesArray.AddString (Day7SDKSettings.Instance.URLSchemes);

            File.WriteAllText(plistPath, plist.WriteToString());
        #endif        
        }

    }
}
