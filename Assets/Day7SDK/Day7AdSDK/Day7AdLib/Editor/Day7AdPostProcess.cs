﻿/********************************************************************
	Created by Kei on 2018.04
	Copyright © 2018년 Kei. All rights reserved.
*********************************************************************/
using System;
using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.Day7AdEditor;
using UnityEngine;

namespace UnityEditor.Day7AdEditor
{	
	public static class XCodePostProcess
	{
		[PostProcessBuild(400)]
		public static void OnPostProcessBuild(BuildTarget target, string path) {
#if UNITY_IOS
			UnityEditor.XCodeEditor.XCProject project = new UnityEditor.XCodeEditor.XCProject(path);
			
			string projModPath = System.IO.Path.Combine(Application.dataPath, "Day7SDK/Day7AdSDK/Day7AdLib/Editor/iOS");
			var files = System.IO.Directory.GetFiles(projModPath, "*.projmods", System.IO.SearchOption.AllDirectories);
			foreach (var file in files)
			{
				project.ApplyMod(Application.dataPath, file);
			}
			project.Save();
			
			PlistMod.UpdatePlist(path);
#endif
		}
	}
}