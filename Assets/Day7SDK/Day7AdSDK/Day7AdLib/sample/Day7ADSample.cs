﻿/********************************************************************
	Created by Kei on 2018.04
	Copyright © 2018년 Kei. All rights reserved.
*********************************************************************/
using UnityEngine;
using System.Collections;
using Day7SDK;

public class Day7ADSample : MonoBehaviour {

	// Use this for initialization
	void Start () {

		// Day7 유니티 광고UI 초기화
		Debug.LogError(Application.temporaryCachePath);
		Day7ADLib.Instance.Init(Day7ADLib.Brand._DAY7, Day7ADLib.Orientation.Portrait, new Vector2(720, 1280), SystemLanguage.Korean);
		//Day7ADLib.Instance.Init(Day7ADLib.Brand._DAY7, Day7ADLib.Orientation.Landscape, new Vector2(720, 1280), SystemLanguage.Korean);
		// Day7 인앱 초기화
		//Day7InappManager.Instance.Init();
	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape))
		{
			// 종료팝업광고
			Day7ADLib.Instance.ShowADEnd();
		}
	}

	void OnEnable()
	{
		Day7ADLib.InitAdComplete += initAdComplete;
		// 구매 성공 시 호출 이벤트 등록
		//Day7InappManager.PurchaseSucceedEvent += OnPurchaseSucceed;
		
	}

	void OnDisable()
	{
		Day7ADLib.InitAdComplete -= initAdComplete;
		//Day7InappManager.PurchaseSucceedEvent -= OnPurchaseSucceed;
	}

	void initAdComplete(bool success)
	{
		Debug.LogError("init 성공!!");
		Debug.LogError(Day7ADLib.Instance.getAppVersion());
	}

	void OnPurchaseSucceed(IAPManager.ProductInfo product)
	{
		// 구매 성공 시 처리 ( comsume 등 )
	}

	#region Sample UI
	/************************************************************************/
	/* Sample UI                                                            */
	/************************************************************************/

	public void Front()
	{
		//BC21ADLib.ReqADFront(true);
		Day7ADLib.Instance.ShowADFront(Day7ADLib.AniType.scale);
	}
	public void Mid()
	{
		//BC21ADLib.ReqADMid(BC21ADLib.AniType.position);
		Day7ADLib.Instance.ShowADMid();
	}
	public void Icon()
	{
		//BC21ADLib.ReqADIcon();
		Day7ADLib.Instance.ShowADMainIcon();
	}
	public void IconHide()
	{
		Day7ADLib.Instance.HideADMainIcon();
	}
	public void EndPort()
	{
		Day7ADLib.Instance.ShowADEnd();
	}
	public void OpenTnk()
	{
		Day7OfferwallAds.Instance.ShowOfferwall1();
	}
	public void OpenTapjoy()
	{
		Day7OfferwallAds.Instance.ShowOfferwall2();
	}
	public void OpenAdpopcorn()
	{
		Day7OfferwallAds.Instance.ShowOfferwall3();
	}
	public void OpenNas()
	{
		Day7OfferwallAds.Instance.ShowOfferwall4();
	}
	public void ShowVideoAd()
	{
		//Day7VideoAdManager.Instance.ShowVideoAd();
	}
	#endregion


}
